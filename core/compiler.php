<?php
/**
 * Created by PhpStorm.
 * User: tdc
 * Date: 1/19/16
 * Time: 11:59 PM
 */

namespace core;

class Compiler
{
    private $compile_cmd_1;
    private $compile_cmd_2;
    private $run_cmd;
    private $name;
    private $version;
    private $compile_file_name;
    private $run_file_name;
    private $time_limit_rule;
    private $memory_limit_rule;
    private $ROOT = "/var/www/html";

    public function load($compiler)
    {
        $compiler_file = "../compilers/" . $compiler . '.xml';
        if (file_exists($compiler_file)) {
            $data = simplexml_load_file($compiler_file);

            $this->name = ((string)$data->name[0]);
            $this->version = ((string)$data->version[0]);
            $this->compile_cmd_1 = ((string)$data->compile_cmd_1);
            $this->compile_cmd_2 = ((string)$data->compile_cmd_2);
            $this->run_cmd = ((string)$data->run_cmd);
            $this->compile_file_name = $data->compile_file_name;
            $this->run_file_name = $data->run_file_name;
            $this->time_limit_rule = $data->time_limit_rule;
            $this->memory_limit_rule = $data->memory_limit_rule;
            return true;
        } else {
            return false;
        }
    }

    public function compile($code, $outputFolder)
    {
        $this->ROOT = "/var/www/html";
        $sourcePath = "/".$this->ROOT."/uploads/userCode/" . $outputFolder . "/" . $this->compile_file_name;
        $handle = fopen($sourcePath, 'w+');
        fwrite($handle, $code);
        fclose($handle);
        chmod($sourcePath, 0770);

        $return = Array(
            "code" => 0,
            "output" => ""
        );

        if ($this->compile_cmd_1 != "") {
            $compileCmd = str_replace("{output_folder}", $outputFolder, $this->compile_cmd_1);
            exec($compileCmd, $output, $outcode);
            $return["output"] .= implode($output);
            $return["code"] = $outcode;

            if ($outcode == 0 && $this->compile_cmd_2 != "") {
                $compileCmd = str_replace("{output_folder}", $outputFolder, $this->compile_cmd_2);
                exec($compileCmd, $output, $outcode);
                $return["output"] .= implode($output);
                $return["code"] = $outcode;
            }
            chmod("/".$this->ROOT."/uploads/userCode/" . $outputFolder . "/" . $this->run_file_name, 0770);
        }
        return $return;
    }

    public function run($outputFolder, $timelimit, $is_judge)
    {
        $this->ROOT = "/var/www/html";
        $descriptorspec = array(
            0 => array("file", "/".$this->ROOT."/uploads/userCode/" . $outputFolder . "/input.txt", "r"),  // stdin is a pipe that the child will read from
            1 => array("file", "/".$this->ROOT."/uploads/userCode/" . $outputFolder . ($is_judge===true ? "/judge_output.txt" : "/output.txt"), "w+"),  // stdout is a pipe that the child will write to
            2 => array("file", "/".$this->ROOT."/uploads/userCode/" . $outputFolder . ($is_judge===true ? "/judge_error.txt" : "/error.txt"), "w+") // stderr is a file to write to
        );

        $cwd = "/".$this->ROOT."/uploads/userCode/" . $outputFolder . "/";

        $env = array('some_option' => 'aeiou');
        $result["success"] = "none";

        //For linux:
        $process = proc_open('/usr/bin/time -f "%M" -o memory.txt '.$this->run_cmd, $descriptorspec, $pipes, $cwd, $env);

        //For Mac
        //$process = proc_open('gtime -f "%M" -o memory.txt '.$this->run_cmd, $descriptorspec, $pipes, $cwd, $env);

        if (is_resource($process)) {
            $currTime = 0;
            while($currTime < $timelimit) {
                $process_status = proc_get_status($process);
                if(!$process_status["running"]) {
                    break;
                }
                sleep(1);
                $currTime++;
            }
            $process_status = proc_get_status($process);

            if($process_status["running"]) {
                proc_terminate($process, 9);
                $result["success"] = "terminated";
            } else {
                $result["success"] = "ok";
            }
            proc_close($process);
            return $result;
        } else {
            return $result;
        }
    }

    public function cleanUp($outputFolder)
    {
        exec("rm -rf "."/".$this->ROOT."/uploads/userCode/" . $outputFolder);
    }

    /**
     * @return mixed
     */
    public function getCompileCmd1()
    {
        return $this->compile_cmd_1;
    }

    /**
     * @param mixed $compile_cmd_1
     */
    public function setCompileCmd1($compile_cmd_1)
    {
        $this->compile_cmd_1 = $compile_cmd_1;
    }

    /**
     * @return mixed
     */
    public function getCompileCmd2()
    {
        return $this->compile_cmd_2;
    }

    /**
     * @param mixed $compile_cmd_2
     */
    public function setCompileCmd2($compile_cmd_2)
    {
        $this->compile_cmd_2 = $compile_cmd_2;
    }

    /**
     * @return mixed
     */
    public function getRunCmd()
    {
        return $this->run_cmd;
    }

    /**
     * @param mixed $run_cmd
     */
    public function setRunCmd($run_cmd)
    {
        $this->run_cmd = $run_cmd;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param mixed $version
     */
    public function setVersion($version)
    {
        $this->version = $version;
    }

    /**
     * @return mixed
     */
    public function getCompileFileName()
    {
        return $this->compile_file_name;
    }

    /**
     * @param mixed $compile_file_name
     */
    public function setCompileFileName($compile_file_name)
    {
        $this->compile_file_name = $compile_file_name;
    }

    /**
     * @return mixed
     */
    public function getRunFileName()
    {
        return $this->run_file_name;
    }

    /**
     * @param mixed $run_file_name
     */
    public function setRunFileName($run_file_name)
    {
        $this->run_file_name = $run_file_name;
    }

    /**
     * @return mixed
     */
    public function getTimeLimitRule()
    {
        return $this->time_limit_rule;
    }

    /**
     * @param mixed $time_limit_rule
     */
    public function setTimeLimitRule($time_limit_rule)
    {
        $this->time_limit_rule = $time_limit_rule;
    }

    /**
     * @return mixed
     */
    public function getMemoryLimitRule()
    {
        return $this->memory_limit_rule;
    }

    /**
     * @param mixed $memory_limit_rule
     */
    public function setMemoryLimitRule($memory_limit_rule)
    {
        $this->memory_limit_rule = $memory_limit_rule;
    }

}