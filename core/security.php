<?php
/**
 * Created by PhpStorm.
 * User: luba
 * Date: 12/24/15
 * Time: 10:56 PM
 */


namespace core;

abstract class Security
{

    public function filter_html($string)
    {
        return htmlspecialchars($string);
    }

    public function escape($string)
    {
        return mysqli_real_escape_string($string);
    }
}