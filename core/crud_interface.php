<?php
/**
 * Created by PhpStorm.
 * User: luba
 * Date: 12/24/15
 * Time: 10:58 PM
 */

namespace core;

interface Crud
{
    public function create($object);
    public function read($id=-1);
    public function update($object, $id);
    public function delete($id);
}