<?php
/**
 * Created by PhpStorm.
 * User: luba
 * Date: 12/24/15
 * Time: 10:54 PM
 */

namespace core;

use \PDO;

class Database
{

    private $DB_USER = "homestead";
    private $DB_PASS = "secret";
    private $DB_NAME = "sat";
    private $DB_HOST = "192.168.1.222";

    private $db;

    public function __construct()
    {
        $this->db = new PDO('mysql:host='.$this->DB_HOST.';dbname='.$this->DB_NAME.';charset=utf8',
            $this->DB_USER, $this->DB_PASS);
        $this->db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );

    }

    public function get_db()
    {
        return $this->db;
    }

    public function __destruct()
    {
        $this->db = null;
    }

}
