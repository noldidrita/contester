<?php
/**
 * Created by PhpStorm.
 * User: TDC
 * Date: 25.1.2016
 * Time: 14:39
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Add Problem</title>
    <meta class="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/view/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/s/bs/dt-1.10.10/datatables.min.css"/>
    <link rel="stylesheet" href="/view/css/styles.css">
    <link rel="stylesheet" href="/view/css/codemirror.css">
    <link rel="stylesheet" href="/view/css/material.css">

    <script src="/view/js/jquery-1.11.3.min.js"></script>
    <script src="/view/js/bootstrap.min.js"></script>
    <script src="/view/js/sidebar_menu.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/s/bs/dt-1.10.10/datatables.min.js"></script>

    <script src="/view/js/codemirror.js"></script>
    <script src="/view/js/javascript.js"></script>
    <script src="/view/js/foldcode.js"></script>
    <script src="/view/js/matchbrackets.js"></script>
    <script src="/view/js/active-line.js"></script>

    <script type="text/javascript" charset="utf-8">

        var casesNr = 1;
        <?php
        if(isset($problemCases))
            echo "casesNr = ".(count($problemCases)+1).";";
        ?>
        $(document).ready(function () {
            var textArea = document.getElementById("judge_code");
            window.editor = CodeMirror.fromTextArea(textArea, {
                mode: "javascript",
                lineNumbers: true,
                lineWrapping: true,
                matchBrackets: true,
                styleActiveLine: true,
                theme: "material",
                foldGutter: {
                    rangeFinder: new CodeMirror.fold.combine(CodeMirror.fold.brace, CodeMirror.fold.comment)
                },
                gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"]
            });

            $('.table').DataTable({
                "order": [[0, "asc"]]
            });

            $('#add-case').click(function () {
                var input = $('#input').val();
                var pattern = $('#pattern').val();
                var points = $('#points').val();
                if (points == "")
                    points = 1;
                if (input == "")
                    alert("Input cannot be empty");
                else {
                    var row = "<tr><td class='col-sm-2'><textarea class='form-control' name='caseNr[]' style='max-width: 40px;'>" + casesNr + "</textarea></td><td class='col-sm-4'><textarea class='form-control' name='input[]'>" + input + "</textarea></td><td  class='col-sm-4'><textarea class='form-control' name='pattern[]'>" + pattern + "</textarea></td><td  class='col-sm-2'><textarea style='max-width: 40px;' class='form-control' name='points[]'>" + points + "</textarea></td></tr>";
                    casesNr++;
                    document.getElementById("cases-body").innerHTML += row;
                    $('#input').val("");
                    $('#pattern').val("");
                    $('#points').val("");
                }
            });
        });
    </script>
</head>
<!--<xs->phone,sm->tablets,md->normal desktop,lg->large desktop >-->

<body>
<!--navbar and logo-->
<?php include("header.php") ?>
<!-- /navbar-->
<div id="wrapper">
    <!-- Sidebar -->
    <?php include("sidebar.php");
    ?>
    <!-- Page Content -->
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <?php
                if ($error == true) {
                    echo '<div class="col-md-12 alert alert-danger" role="alert"><strong>Error: </strong><br/>' . $message . '</div>';
                }
                ?>
                <div class="row" style="margin-bottom: 16px">
                    <div class="text-center">
                        <?php if(isset($problem)) {?>
                            <h1 style="margin-top:0px;">Edit Problem</h1>
                        <?php } else { ?>
                        <h1 style="margin-top:0px;">Create a Problem</h1>
                        <?php } ?>
                        <p class="title">Please fill in the following information.</p>
                    </div>
                </div>
                <?php
                    if(isset($problem)) {
                        $action = "/edit-problem/".$_GET['id'];
                    } else {
                        $action = "/add-problem";
                    }
                ?>

                <form class="form-horizontal" method="post" action="<?= $action ?>">

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="titleinput" class="col-sm-3 control-label">Title</label>

                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="titleinput" name="title"
                                       value="<?= (isset($problem) ? $problem['title'] : "") ?>"
                                       placeholder="Enter the problem title" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="subtitleInput" class="col-sm-3 control-label">Subtitle</label>

                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="subtitleInput" name="subtitle"
                                       value="<?= (isset($problem) ? $problem['subtitle'] : "") ?>"
                                       placeholder="Enter a subtitle (author, date, source, etc)" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="shortdescription" class="col-sm-3 control-label">Short Description</label>

                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="shortdescription" name="short_description"
                                       value="<?= (isset($problem) ? $problem['shortdescription'] : "") ?>"
                                       placeholder="Enter a short description of the problem" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="timelimit" class="col-sm-3 control-label">Time Limits* (seconds)</label>

                            <div class="col-sm-3">
                                <input type="number" class="form-control" id="timelimit" name="time_limit_1"
                                       value="<?= (isset($problem) ? $problem['time_limit_1'] : "") ?>"
                                       placeholder="Limit 1" required>
                            </div>
                            <div class="col-sm-3">
                                <input type="number" class="form-control" id="timelimit" name="time_limit_2"
                                       value="<?= (isset($problem) ? $problem['time_limit_2'] : "") ?>"
                                       placeholder="Limit 2" required>
                            </div>
                            <div class="col-sm-3">
                                <input type="number" class="form-control" id="timelimit" name="time_limit_3"
                                       value="<?= (isset($problem) ? $problem['time_limit_3'] : "") ?>"
                                       placeholder="Limit 3" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="memorylimit" class="col-sm-3 control-label">Memory Limits* (KBytes)</label>

                            <div class="col-sm-3">
                                <input type="number" class="form-control" id="memorylimit" name="memory_limit_1"
                                       value="<?= (isset($problem) ? $problem['memory_limit_1'] : "") ?>"
                                       placeholder="Limit 1" required>
                            </div>
                            <div class="col-sm-3">
                                <input type="number" class="form-control" id="memorylimit" name="memory_limit_2"
                                       value="<?= (isset($problem) ? $problem['memory_limit_2'] : "") ?>"
                                       placeholder="Limit 2" required>
                            </div>
                            <div class="col-sm-3">
                                <input type="number" class="form-control" id="memorylimit" name="memory_limit_3"
                                       value="<?= (isset($problem) ? $problem['memory_limit_3'] : "") ?>"
                                       placeholder="Limit 3" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-3 control-label">Difficulty</label>

                            <div class="col-sm-3">
                                <input type="range" min="1" max="5" id="difficulty" name="difficulty"
                                       value="<?= (isset($problem) ? $problem['difficulty'] : "") ?>"
                                       placeholder="Enter the problem title" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="ready" class="col-sm-3 control-label">Ready</label>

                            <div class="col-sm-6">

                                <?php
                                if (isset($problem) && $problem['ready'] == 1)
                                    echo "<input type='checkbox' id='ready' name='ready' checked/>";
                                else echo "<input type='checkbox' id='ready' name='ready'/>";

                                ?>
                                Make this problem visible to everyone
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="volume" class="col-sm-3 control-label">Categories</label>

                            <div class="col-sm-4 col-sm-offset-1">
                                <select class="form-control" id="volume" name="volume_id" required>
                                    <?php
                                    foreach ($volumes as $volume) {
                                        if (isset($problem) && $problem['volume_id'] == $volume['id'])
                                            echo "<option value='" . $volume['id'] . "' selected>" . $volume['name'] . "</option>";
                                        else echo "<option value='" . $volume['id'] . "'>" . $volume['name'] . "</option>";
                                    }
                                    ?>                                </select>
                            </div>
                            <div class="col-sm-4">
                                <select class="form-control" id="section" name="section_id" required>
                                    <?php
                                    foreach ($sections as $section) {
                                        if (isset($problem) && $problem['section_id'] == $section['id'])
                                            echo "<option value='" . $section['id'] . "' selected>" . $section['name'] . "</option>";
                                        else echo "<option value='" . $section['id'] . "'>" . $section['name'] . "</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="contest" class="col-sm-3 control-label">Contests</label>

                            <div class="col-sm-4 col-sm-offset-1">
                                <select class="form-control" name="contest_id" id="contest" required>
                                    <?php
                                    foreach ($contests as $contest) {
                                        if (isset($problem) && $problem['contest_id'] == $contest['id'])
                                            echo "<option value='" . $contest['id'] . "' selected>" . $contest['title'] . "</option>";
                                        else echo "<option value='" . $contest['id'] . "'>" . $contest['title'] . "</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <select class="form-control" name="homework_id" id="homework" required>
                                    <?php
                                    foreach ($homeworks as $homework) {
                                        if (isset($problem) && $problem['homework_id'] == $homework['id'])
                                            echo "<option value='" . $homework['id'] . "' selected>" . $homework['title'] . "</option>";
                                        else echo "<option value='" . $homework['id'] . "'>" . $homework['title'] . "</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12 well well-sm" style="margin-top:8px">*The values will be used
                            along with the compiler configurations to judge the performance of the solutions.<br/>Limits
                            1 are usually used for lower level languages such as C++, Limits 2 for Java and Limits 3
                            for interpreted languages such as Python.<br/>Usual Time Limits include 2s for c++, 4s
                            for java, 4s for python.<br/>Usual Memory limits are 64000 KBytes
                        </div>
                        <div class="form-group">
                            <label for="statement" class="col-sm-3 col-sm-offset-5 control-label">Statement</label>

                            <div class="col-sm-12">
                                <textarea class="form-control" rows="15" name="statement" id="statement"
                                          placeholder="Please enter the statement for the problem. HTML code is allowed. Please include only the elements themselves and no html/head/body tags."><?= (isset($problem) ? $problem['statement'] : "") ?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="form-group">
                                <label for="judge_code" class="col-sm-3 col-sm-offset-5 control-label">Judge
                                    Code</label>

                                <div class="col-sm-12">
                                <textarea class="form-control" rows="15" name="judge_code" id="judge_code"
                                          placeholder="Please enter the checker for this problem. The files input.txt, output.txt, pattern.txt will be provided to this program to help evaluate the solution. If you do not submit a checker, the default one will be used. It simply compares the output and the pattern for every input file."><?= (isset($problem) ? $problem['judge_code'] : "") ?></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="judge_language" class="col-sm-4 control-label">Judge Language</label>

                                <div class="col-sm-4 col-sm-offset-4">
                                    <select class="form-control" name="judge_language" id="judge_language">
                                        <?php
                                        foreach ($compilers as $compiler) {
                                            if (isset($problem) && $problem['judge_language'] == $compiler['value'])
                                                echo "<option value='" . $compiler['value'] . "' selected>" . $compiler['name'] . "</option>";
                                            else echo "<option value='" . $compiler['value'] . "'>" . $compiler['name'] . "</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="test_cases" class="col-sm-5 col-sm-offset-4 control-label">Add more Test
                                    Cases</label>

                                <div class="col-sm-12">
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>Case Nr</th>
                                            <th>Input</th>
                                            <th>Pattern</th>
                                            <th>Points</th>
                                        </tr>
                                        </thead>
                                        <tbody id="cases-body">
                                        <?php if(isset($problemCases)) {
                                            foreach($problemCases as $case){?>
                                            <tr>
                                                <td><?=$case['test_nr']?></td>
                                                <td><?=$case['input']?></td>
                                                <td><?=$case['pattern']?></td>
                                                <td><?=$case['points']?></td>
                                            </tr>
                                        <?php }
                                        } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-12">
                                    <textarea rows="12" class="form-control" id="input"
                                              placeholder="Input"></textarea>
                                </div>
                                <div class="col-sm-12">
                                    <textarea rows="12" class="form-control" id="pattern"
                                              placeholder="Pattern"></textarea>
                                </div>
                                <div class="col-sm-3">
                                    <input type="number" class="form-control" id="points"
                                           placeholder="Points"/>
                                </div>
                                <div class="col-sm-6 col-sm-offset-3">
                                    <button type="button" value="Add" id="add-case" class="btn btn-block btn-primary">
                                        Add
                                    </button>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="row" style="margin-top:22px;">
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-4">
                                <?php
                                if (isset($problem)) { ?>
                                    <button type="submit" name="submit" value="Save" class="btn btn-block btn-primary">
                                        Save
                                    </button>

                                <?php } else {
                                    ?>
                                    <button type="submit" name="submit" value="submit"
                                            class="btn btn-block btn-primary">
                                        Create Problem
                                    </button>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /#page-content-wrapper -->
    </div>
</body>
</html>