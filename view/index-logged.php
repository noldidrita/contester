<?php
/**
 * Created by PhpStorm.
 * User: TDC
 * Date: 25.1.2016
 * Time: 14:39
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>SAT</title>
    <meta class="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/view/css/bootstrap.min.css">
    <link rel="stylesheet" href="/view/css/styles.css">

    <script src="/view/js/jquery-1.11.3.min.js"></script>
    <script src="/view/js/bootstrap.min.js"></script>
    <script src="/view/js/sidebar_menu.js"></script>

</head>
<!--<xs->phone,sm->tablets,md->normal desktop,lg->large desktop >-->

<body>
<!--navbar and logo-->
<?php include("header.php") ?>
<!-- /navbar-->
<div id="wrapper">
    <!-- Sidebar -->
    <?php include("sidebar.php");
    ?>
    <!-- /#sidebar-wrapper -->
    <!-- Page Content -->
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                  <h2>Welcome back <?=$_SESSION['user']->getName()?>!</h2>
                  <h3>We are glad to see you willing to learn.</h3>
                    <h4>May the tests be ever in your favor...</h4>
                    <img src="/view/img/Codewars.png" width="100%"/>
                </div>
            </div>
        </div>
    </div>
    <!-- /#page-content-wrapper -->
</div>
</body>
</html>