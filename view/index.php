<?php
/**
 * Created by PhpStorm.
 * User: luba
 * Date: 25.1.2016
 * Time: 14:39
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>SAT</title>
    <meta class="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/view/css/bootstrap.min.css">
    <link rel="stylesheet" href="/view/css/styles.css">

    <script src="/view/js/jquery-1.11.3.min.js"></script>
    <script src="/view/js/bootstrap.min.js"></script>
    <script src="/view/js/sidebar_menu.js"></script>

    <script>
        $(document).ready(function(){
            var birthdayDay = "<option value='-1' selected>Day</option>";
            for(var i=1; i<=31; i++) {
                birthdayDay += "<option value='"+(i<10 ? "0" : "")+i+"'>"+i+"</option>";
            }
            $('#birthday_day').html(birthdayDay);

            var birthdayYear = "<option value='-1' selected>Year</option>";
            var thisYear = parseInt(new Date().getFullYear());
            for(var i=thisYear; i>=1950; i--) {
                birthdayYear += "<option value='"+i+"'>"+i+"</option>";
            }
            $('#birthday_year').html(birthdayYear);
        })
    </script>

</head>
<!--<xs->phone,sm->tablets,md->normal desktop,lg->large desktop >-->

<body>
<!--navbar and logo-->
<?php include("header.php") ?>
<!-- /navbar-->
<div id="wrapper">
    <!-- Sidebar -->
  <?php include("sidebar.php");
  ?>
    <!-- Page Content -->
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <?php
                    if($error == true) {
                      echo '<div class="col-md-12 alert alert-danger" role="alert"><strong>Error: </strong><br/>'.$message.'</div>';
                    } else if($registered==true) {
                        echo '<div class="col-md-12 alert alert-success" role="alert"><strong>Success!</strong><br/> Your account has been created. Please wait for approval.</div>';
                    }
                ?>
                <div class="col-md-6 col-md-offset-6">
                    <h1 style="margin-top:0px;">Sign Up</h1>

                    <p class="title">It's free and always will be.</p>

                    <form class="form-horizontal" enctype="multipart/form-data" method="post" action="/login">
                        <div class="form-group">
                            <label for="nameinput" class="col-sm-3 control-label">Name</label>

                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="nameinput" name="name"
                                       placeholder="Enter your name" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputSurname" class="col-sm-3 control-label">Surname</label>

                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="inputSurname" name="surname"
                                       placeholder=" Enter your surname" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputusername" class="col-sm-3 control-label">Username</label>

                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="inputusername" name="username"
                                       placeholder="Enter your username" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputemail" class="col-sm-3 control-label">Email</label>

                            <div class="col-sm-9">
                                <input type="email" class="form-control" id="inputemail" name="email"
                                       placeholder="Enter your email" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputpassword" class="col-sm-3 control-label">Password</label>

                            <div class="col-sm-9">
                                <input type="password" class="form-control" id="inputpassword" name="password"
                                       placeholder="Enter your password" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputconfirm" class="col-sm-3 control-label">Confirm Password</label>

                            <div class="col-sm-9">
                                <input type="password" class="form-control" id="inputconfirm" name="confirm_password"
                                       placeholder="Re-enter your password" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputfile" class="col-sm-3 control-label">Profile Picture</label>

                            <div class="col-sm-9">
                                <input type="file" class="form-control" id="inputfile" accept="image/gif, image/jpeg, image/png" name="picture">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="birthday_year" class="col-sm-3 control-label">Birthday</label>
                            <div class="col-sm-3">
                                <select class="form-control" id="birthday_day" name="birthday_day" required>
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <select class="form-control" id="birthday_month" name="birthday_month" required>
                                    <option selected>Month</option>
                                    <option value="01">Jan</option>
                                    <option value="02">Feb</option>
                                    <option value="03">Mar</option>
                                    <option value="04">Apr</option>
                                    <option value="05">May</option>
                                    <option value="06">Jun</option>
                                    <option value="07">Jul</option>
                                    <option value="08">Aug</option>
                                    <option value="09">Sep</option>
                                    <option value="10">Oct</option>
                                    <option value="11">Nov</option>
                                    <option value="12">Dec</option>
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <select class="form-control" name="birthday_year" id="birthday_year" required>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-9">
                                <label class="radio-inline">
                                    <input type="radio" name="gender" value="Female" checked> Female
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="gender" value="Male"> Male
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" name="submit" value="signup" class="btn btn-block btn-primary">
                                    Sign Up
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- /#page-content-wrapper -->
</div>
</body>
</html>