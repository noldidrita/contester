<?php
/**
 * Created by PhpStorm.
 * User: luba
 * Date: 1/24/16
 * Time: 8:46 PM
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Edit Profile</title>
    <meta class="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/view/css/bootstrap.min.css">
    <link rel="stylesheet" href="/view/css/styles.css">


    <script src="/view/js/jquery-1.11.3.min.js"></script>
    <script src="/view/js/bootstrap.min.js"></script>
    <script src="/view/js/sidebar_menu.js"></script>

</head>
<!--<xs->phone,sm->tablets,md->normal desktop,lg->large desktop >-->

<body>
<!--navbar and logo-->
<?php include("header.php") ?>
<!-- /navbar-->
<div id="wrapper">
    <!-- Sidebar -->
    <?php include("sidebar.php");
    ?>
    <!-- /#sidebar-wrapper -->
    <!-- Page Content -->
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="h2-style">Profile</h2>
                    <hr/>
                    <?php
                    if($error == true) {
                        echo '<div class="col-md-12 alert alert-danger" role="alert"><strong>Error: </strong><br/>'.$message.'</div>';
                    } else if($updated==true) {
                        echo '<div class="col-md-12 alert alert-success" role="alert"><strong>Success!</strong><br/> Your settings have been updated.</div>';
                    }
                    ?>
                    <div class="profile-container">

                        <div class = "table-cointainer col-sm-6 col-sm-offset-2">
                            <form class="form-horizontal" enctype="multipart/form-data" method="post" action="/profile/edit/<?=$user->getId()?>">
                                <div class="form-group">
                                    <label for="nameinput" class="col-sm-4 control-label">Name</label>

                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="nameinput" name="name"
                                               placeholder="Enter your name" value="<?=$user->getName()?>" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputSurname" class="col-sm-4 control-label">Surname</label>

                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="inputSurname" name="surname"
                                               placeholder=" Enter your surname" value="<?=$user->getSurname()?>" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputusername" class="col-sm-4 control-label">Username</label>

                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="inputusername" name="username"
                                               placeholder="Enter your username" value="<?=$user->getUsername()?>" >
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputpassword" class="col-sm-4 control-label">New password</label>

                                    <div class="col-sm-8">
                                        <input type="password" class="form-control" id="inputpassword" name="password"
                                               placeholder="Enter your new password (optional)" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputconfirm" class="col-sm-4 control-label">Confirm Password </label>

                                    <div class="col-sm-8">
                                        <input type="password" class="form-control" id="inputconfirm" name="confirm_password"
                                               placeholder="Re-enter your new password (optional)" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputfile" class="col-sm-4 control-label"> Change Profile Picture</label>

                                    <div class="col-sm-8">
                                        <input type="file" class="form-control" id="inputfile" accept="image/gif, image/jpeg, image/png" name="picture">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-offset-5 col-sm-6">
                                        <button type="submit" name="Submit" value="Save" class="btn btn-block btn-primary">
                                            Save
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /#page-content-wrapper -->
</div>
</body>
</html>
