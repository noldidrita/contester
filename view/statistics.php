<?php
/**
 * Created by PhpStorm.
 * User: TDC
 * Date: 1/21/16
 * Time: 12:15 AM
 */

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Statistics</title>
    <meta class="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/view/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css"
          href="https://cdn.datatables.net/s/bs/dt-1.10.10,se-1.1.0/datatables.min.css"/>
    <link rel="stylesheet" href="/view/css/styles.css">

    <script src="/view/js/jquery-1.11.3.min.js"></script>
    <script src="/view/js/bootstrap.min.js"></script>
    <script src="/view/js/sidebar_menu.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/s/bs/dt-1.10.10,se-1.1.0/datatables.min.js"></script>

    <script type="text/javascript" charset="utf-8">
        $(document).ready(function () {
            var table = $('.table').DataTable({
                "order": [[5, "desc"], [6, "asc"]]
            });
        });
    </script>

</head>
<!--<xs->phone,sm->tablets,md->normal desktop,lg->large desktop >-->

<body>
<?php
include("header.php");
?>
<!-- /navbar-->
<div id="wrapper">
    <?php include("sidebar.php"); ?>
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <h2 class="col-md-3">Top Ranked Users</h2>
            </div>
            <hr/>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Group</th>
                    <th>Type</th>
                    <th>Accepted</th>
                    <th>Submissions</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($users as $row) {
                    echo "<tr>";
                    echo "<td>" . $row['id'] . "</td>";
                    echo "<td><a href='/profile/" . $row['id'] . "'>" . $row['username'] . "</a></td>";
                    echo "<td>" . $row['email'] . "</td>";
                    $group = $groupCrud->read($row['group_id']);
                    echo "<td><a href='/groups/" . $row['group_id'] . "'>" . $group[0]["name"] . "</a></td>";
                    echo "<td>" . getTypeString($row['type']) . "</td>";
                    echo "<td>" . $row['accept_cnt'] . "</td>";
                    echo "<td>" . $row['attempts_cnt'] . "</td>";
                    echo "</tr>";
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
    <!-- /#page-content-wrapper -->
</div>
</body>
</html>