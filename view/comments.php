<?php
/**
 * Created by PhpStorm.
 * User: luba
 * Date: 1/22/16
 * Time: 1:01 AM
 */

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Comments</title>
    <meta class="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/view/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/s/bs/dt-1.10.10/datatables.min.css"/>
    <link rel="stylesheet" href="/view/css/styles.css">

    <script src="/view/js/jquery-1.11.3.min.js"></script>
    <script src="/view/js/bootstrap.min.js"></script>
    <script src="/view/js/sidebar_menu.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/s/bs/dt-1.10.10/datatables.min.js"></script>
    <script src="/view/js/moment.min.js"></script>

    <script type="text/javascript" charset="utf-8">
    </script>

</head>
<!--<xs->phone,sm->tablets,md->normal desktop,lg->large desktop >-->

<body>
<?php
include("header.php");
?>
<!-- /navbar-->
<div id="wrapper">
    <?php include("sidebar.php"); ?>
    <div id="page-content-wrapper">
        <?php
        if($error == true) {
            echo '<div class="col-md-12 alert alert-danger" role="alert"><strong>Error: </strong><br/>'.$message.'</div>';
        } else if($success==true) {
            echo '<div class="col-md-12 alert alert-success" role="alert"><strong>Success!</strong><br/> Your comment was successfully submitted.</div>';
        }
        ?>
        <div class="container-fluid">
            <h2><a href="/problems/<?= $problem['id'] ?>"><?= $problem['title'] ?></a></h2>
            <h4>Comments</h4>
            <hr/>
            <?php
            $isDuringEvent = $problemCrud->isDuringEvent($problem['contest_id'], $problem['homework_id']);
            $commentNr = 0;
            foreach ($comments as $row) {
                $profile = "<div class='col-md-3' style='background-color:#e7e7e7; padding:16px; margin-bottom: 16px; text-align: center;'>
                    <a href='/profile/" . $row['user_id'] . "'><img src='/uploads/profileImg/" . $row['pictureUrl'] . "' class='img-circle profile-img' width='80' height='80'/>
                    " . $row['username'] . "</a>
                    <script>var time = moment('" . $row['timestamp'] . "'); document.write('<h5>'+time.fromNow()+'</h5>');</script></div>";

                $commentText = "<div class='col-md-9' style='margin-bottom: 16px;'><h6>Comment: ";

                if (isset($_SESSION['user']) && ($_SESSION['user']->getType() > 2 || $_SESSION['user']->getId() == $row['user_id'])) {
                    $commentText .= "<a href='/comments/".$problem['id']."/remove/" . $row['comment_id'] . "'>Remove</a>";
                }
                if ((!$isDuringEvent) || $row['user_type'] > 2 || ($isDuringEvent && isset($_SESSION['user']) && ($_SESSION['user']->getType() > 2 || $_SESSION['user']->getId() == $row['user_id'])))
                    $commentText .= "</h6><pre style='min-height:115px;'>" . $row['text'] . "</pre></div>";
                else $commentText .= "</h6><pre style='min-height:115px;'>Comments from other users are not visible while there is an active contest.</pre></div>";

                echo "<div class='row'>";

                if ($commentNr % 2 == 0) {
                    echo $profile . $commentText;
                } else {
                    echo $commentText . $profile;
                }
                echo "</div>";
                echo "</hr>";

                $commentNr++;
            }
            ?>
            <div class="row"
                 style="background-color:#ececec; border-top-left-radius: 10px; border-top-right-radius: 10px;">
                <div class="col-md-12"
                     style="background-color:#c3c3c3; border-top-left-radius: 10px; border-top-right-radius: 10px; text-align: center; margin-bottom: 6px">
                    <h5 style="color: #505050;">Enter your comment</h5>
                </div>
                <form action="/comments/add/<?=$problem['id']?>" method="post">
                    <div class="col-md-6 col-md-offset-3">
                    <textarea class="form-control" rows="10" name="text"
                              placeholder="Enter your comment here. Be sure not to submit codes, but instead describe you ideas in pseudo-code or plain english so that other students do not get influenced by your code."></textarea>
                    </div>
                    <div class="col-md-2 col-md-offset-5">
                        <input class="form-control" type="submit" name="Submit" value="Submit"/>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /#page-content-wrapper -->
</div>
</body>
</html>