<?php
/**
 * Created by PhpStorm.
 * User: luba
 * Date: 1/20/16
 * Time: 11:55 PM
 */

?>
<div class="container-fluid" style="margin:0; padding:0;">
    <nav class="navbar navbar-default no-margin row">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#sidebar-wrapper">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>

        <div>
            <?php
            if (isset($_SESSION["user"])) { ?>
                <div class="nav navbar-nav navbar-right col-lg-2">
                    <li class="dropdown ">
                        <a href="#" class="dropdown-toggle username-dropdown-toggle" data-toggle="dropdown">
                            <img src="<?="/uploads/profileImg/". $_SESSION['user']->getPictureUrl() ?>" class="img-circle profile-img"
                                 width="40"
                                 height="40"><?php echo $_SESSION["user"]->getName() . " " . $_SESSION["user"]->getSurname(); ?>
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu dropdown-style">
                            <li>
                                <img src="<?="/uploads/profileImg/". $_SESSION['user']->getPictureUrl() ?>" class="img-rounded profile-img"
                                     width="80" height="80">

                                <div class="username-container">
                                    <div class="username">
                <?php echo $_SESSION["user"]->getName()." ".$_SESSION["user"]->getSurname(); ?>
            </div>
                                    <div class="title">
                <?php
                $type = $_SESSION["user"]->getType();
                echo getTypeString($type);
                ?>
            </div>
                                </div>
                            </li>
                            <li><span class="divider"></span><a href="/profile"><span
                                        class="glyphicon glyphicon-user margin-icon-side" aria-hidden="true"></span>Profile</a>
                            </li>
                            <form action="/logout" method="get">
                                <li><span class="divider"></span>
                                    <button type="submit" class="btn  btn-lg btn-block btn-info"><span
                                            class="glyphicon glyphicon-off margin-icon-side" aria-hidden="true"></span>Logout
                                    </button>
                                </li>
                            </form>
                        </ul>
                    </li>
                </div>
                <?php } else { ?>

                <div class="nav navbar-nav navbar-right navbar-right-user-field row">
                    <form method="post" action="/login">
                        <div class="col-xs-5">
                            <label for="ex1"></label>
                            <input class="form-control" id="ex1" type="text" name="username" placeholder="Username">
                        </div>
                        <div class="col-xs-5">
                            <label for="ex2"></label>
                            <input class="form-control" id="ex2" type="password" name="password" placeholder="Password">
                        </div>
                        <div class="col-xs-2">
                            <button type="submit" name="submit" value="login" class="btn btn-default">Login</button>
                        </div>
                    </form>
                </div>
            <?php } ?>
        </div>
    </nav>
</div>