<?php
/**
 * Created by PhpStorm.
 * User: luba
 * Date: 1/21/16
 * Time: 12:15 AM
 */
?>
<div id="sidebar-wrapper">
    <ul class="sidebar-nav" id="menu">
        <li >
            <a href="/home" class="navbar-brand navbar-logo"><img src="/view/logo.png" height="75"/></a>
        </li>
        <li>
            <a href="/home"><span class="divider"></span><span class="glyphicon glyphicon-home margin-icon-side"
                                                           aria-hidden="true"></span>Home</a>
        </li>
        <li>
            <a href="/volumes"><span class="divider"></span><span class="glyphicon glyphicon-th margin-icon-side"
                                                           aria-hidden="true"></span>Volumes</a>
        </li>
        <li>
            <a href="/sections"><span class="divider"></span><span class="glyphicon glyphicon-th-list margin-icon-side"
                                                           aria-hidden="true"></span>Sections</a>
        </li>
        <li>
            <a href="/contests"><span class="divider"></span><span class="glyphicon glyphicon-time margin-icon-side"
                                                           aria-hidden="true"></span>Contests</a>
        </li>
        <li>
            <a href="/homeworks"><span class="divider"></span><span class="glyphicon glyphicon-book margin-icon-side"
                                                           aria-hidden="true"></span>HomeWorks</a>
        </li>
        <li>
            <a href="/statistics"><span class="divider"></span><span class="glyphicon glyphicon-stats margin-icon-side"
                                                           aria-hidden="true"></span>Statistics</a>
        </li>
        <li>
            <a href="/help"><span class="divider"></span><span
                    class="glyphicon glyphicon-question-sign margin-icon-side"
                    aria-hidden="true"></span>Help</a>
        </li>
        <li>
            <a href="/about"><span class="divider"></span><span class="glyphicon glyphicon-king margin-icon-side"
                                                           aria-hidden="true"></span>About</a>
        </li>
        <?php
        //Only assistants and admins can manage users.
        if(isset($_SESSION['user']) && $_SESSION['user']->getType()>2) { ?>
            <li>
                <a href="/add-problem"><span class="divider"></span><span class="glyphicon glyphicon-file margin-icon-side"
                                                                              aria-hidden="true"></span>New Problem</a>
            </li>

            <li>
            <a href="/users"><span class="divider"></span><span class="glyphicon glyphicon-user margin-icon-side"
                                                           aria-hidden="true"></span>Users</a>
        </li>
            <li>
                <a href="/groups"><span class="divider"></span><span class="glyphicon glyphicon-copy margin-icon-side"
                                                                   aria-hidden="true"></span>Groups</a>
            </li>

        <?php } ?>
    </ul>
</div><!-- /#sidebar-wrapper -->

