<?php
/**
 * Created by PhpStorm.
 * User: TDC
 * Date: 1/21/16
 * Time: 12:15 AM
 */
if (!isset($_SESSION['user']) && $_SESSION['user']->getType() < 3) {
    $host  = $_SERVER['HTTP_HOST'];
    $page = "home";
    header("Location: http://$host/$page");
    return;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Groups</title>
    <meta class="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/view/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css"
          href="https://cdn.datatables.net/s/bs/dt-1.10.10,se-1.1.0/datatables.min.css"/>
    <link rel="stylesheet" href="/view/css/styles.css">

    <script src="/view/js/jquery-1.11.3.min.js"></script>
    <script src="/view/js/bootstrap.min.js"></script>
    <script src="/view/js/sidebar_menu.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/s/bs/dt-1.10.10,se-1.1.0/datatables.min.js"></script>

    <script type="text/javascript" charset="utf-8">
        $(document).ready(function () {
            var table = $('.table').DataTable({
                select: true
            });

            $('#delete').click(function () {
                var ids = $.map(table.rows('.selected').data(), function (item) {
                    return item[0]
                });
                if (ids.length == 0) {
                    alert("You haven't selected any groups.");
                    return;
                }
                if(ids[0]=='1') {
                    alert("You cannot delete the group 'No Groups'.");
                    return;
                }
                var r = confirm("Are you sure you want to delete " + ids.length + " group(s)? This action cannot be reversed.");
                if (r == true) {
                    $.post('/groups', {'action': 'delete', 'ids[]': ids}, function (data) {
                        location.reload();
                    });
                }
            });
        });
    </script>
</head>
<!--<xs->phone,sm->tablets,md->normal desktop,lg->large desktop >-->

<body>
<?php
include("header.php");
?>
<!-- /navbar-->
<div id="wrapper">
    <?php include("sidebar.php"); ?>
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <?php
                if($error == true) {
                    echo '<div class="col-md-12 alert alert-danger" role="alert"><strong>Error: </strong><br/>'.$insert_result['message'].'</div>';
                }
                ?>

                <div class="row">
                <h2 class="col-md-3 h2-style">Manage Groups</h2>
                <div class="col-md-12">
                    <div class="row" style="text-align: right;margin-right: 16px">
                            <form action="/groups" method="post">
                                <input type="text" name="group_name" placeholder="Group Name" required>
                                <input type="text" name="group_description" placeholder="Group Description" required>
                                <input type="submit" name="submit" value="Add">
                            </form>
                        <br/>
                        <input type="button" id="delete" value="Delete Selected">
                    </div>
                </div>
            </div>
            <hr/>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Nr. Users</th>
                    <th>Created</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($groups as $row) {
                    echo "<tr>";
                    echo "<td>" . $row['id'] . "</td>";
                    echo "<td><a href='/groups/" . $row['id'] . "'>" . $row['name'] . "</a></td>";
                    echo "<td>" . $row['description'] . "</td>";
                    echo "<td>" . $row['nr_users'] . "</td>";
                    echo "<td>" . date_format(date_create($row['timestamp']), 'Y-m-d') . "</td>";
                    echo "</tr>";
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
    <!-- /#page-content-wrapper -->
</div>
</body>
</html>