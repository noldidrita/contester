<?php
/**
 * Created by PhpStorm.
 * User: TDC
 * Date: 22.1.2016
 * Time: 22:18
 */

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Scoreboard</title>
    <meta class="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/view/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/s/bs/dt-1.10.10/datatables.min.css"/>
    <link rel="stylesheet" href="/view/css/styles.css">

    <script src="/view/js/jquery-1.11.3.min.js"></script>
    <script src="/view/js/bootstrap.min.js"></script>
    <script src="/view/js/sidebar_menu.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/s/bs/dt-1.10.10/datatables.min.js"></script>
    <script type="text/javascript" src="/view/js/moment.min.js"></script>

    <script type="text/javascript" charset="utf-8">
        $(document).ready(function () {
            var nrProblems = <?=count($problems)?>;
            $('.table').DataTable({
                "order": [[nrProblems+2, "desc"], [nrProblems+3, "asc"]],
                "bPaginate" : false
            });
            $rows = document.getElementById("table").getElementsByTagName("tbody")[0].getElementsByTagName("tr");
            console.log($rows);
            for(var i=0; i<$rows.length; i++) {
                console.log($rows[i]);
                $rows[i].firstChild.innerHTML = (i+1);
            }
        });
    </script>

</head>
<!--<xs->phone,sm->tablets,md->normal desktop,lg->large desktop >-->

<body>
<?php
include("header.php");
?>
<!-- /navbar-->
<div id="wrapper">
    <?php include("sidebar.php"); ?>
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <h2 class="h2-style"><?= $title ?></h2>
            <?php
            if ($crtime >= $event['start_time'] && $crtime < $event['end_time']) {
                ?>
                <h6 id="time_elapsed">Elapsed: 0 days 00:00:00 </h6>
                <h6 id="time_left">Time: 0 days 00:00:00 </h6>
                <script>
                    var start = moment('<?=$event['start_time']?>');
                    var end = moment('<?=$event['end_time']?>');

                    var updateFunction = function () {
                        var now = moment();
                        var elapsed = moment.duration(now.diff(start));
                        var left = moment.duration(end.diff(now));

                        $('#time_elapsed').html("Elapsed: " + ((elapsed.days() > 0) ? (elapsed.days() + " days ") : "") + elapsed.hours() + ":" + elapsed.minutes() + ":" + elapsed.seconds());
                        $('#time_left').html("Left: " + ((left.days() > 0) ? (left.days() + " days ") : "") + left.hours() + ":" + left.minutes() + ":" + left.seconds());
                    };
                    updateFunction();
                    setInterval(updateFunction, 1000);
                </script>
                <?php
            } else if ($crtime < $event['start_time']) {
                echo "<h6>Starts at: " . $event['start_time'] . "</h6>";
            } else {
                echo "<h6>Finished at: " . $event['end_time'] . "</h6>";
            }
            ?>
            <hr/>
            <table id="table" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Rank</th>
                    <th>User</th>
                    <?php
                    foreach ($problems as $problem) {
                        echo "<th><a href='/problems/" . $problem['id'] . "'>" . $problem['title'] . "</a></th>";
                    }
                    ?>
                    <th>Points</th>
                    <th>Time</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $lastUser = -1;
                $solvedCount = 0;
                $timeCount = 0;

                $problem = 0;

                foreach ($scoreboard as $row) {
                    if ($lastUser != $row['user_id']) {
                        if ($lastUser != -1) {
                            while ($problem < count($problems) && $row['problem_id'] != $problems[$problem]['id']) {
                                echo "<td></td>";
                                $problem++;
                            }
                            echo "<td>" . $solvedCount . "</td>";
                            echo "<td>" . $timeCount . "</td>";
                            echo "</tr>";
                        }
                        echo "<tr><td>0</td><td><a href='/profile/".$row['user_id']."'>". $row['name'] . "</a></td>";
                        $lastUser = $row['user_id'];
                        $problem = 0;
                        $solvedCount = 0;
                        $timeCount = 0;
                    }
                    while ($problem < count($problems) && $row['problem_id'] != $problems[$problem]['id']) {
                        echo "<td></td>";
                        $problem++;
                    }

                    $solvedCount+=intval($row["points"]);
                    $timeCount+=intval($row["time"]);

                    if ($row["points"] == $row["total"]) {
                        echo "<td class='success'>" . $row["points"] ."/". $row["total"] . "</td>";
                    } else {
                        echo "<td class='danger'>" . $row["points"] ."/". $row["total"] . "</td>";
                    }
                    $problem++;
                }
                while ($problem < count($problems)) {
                    echo "<td></td>";
                    $problem++;
                }
                echo "<td>" . $solvedCount . "</td>";
                echo "<td>" . $timeCount . "</td>";

                echo "</tr>";
                ?>
                </tbody>
            </table>
        </div>
    </div>
    <!-- /#page-content-wrapper -->
</div>
</body>
</html>
