<?php
/**
 * Created by PhpStorm.
 * User: TDC
 * Date: 1/21/16
 * Time: 12:15 AM
 */
if (!isset($_SESSION['user']) && $_SESSION['user']->getType() < 3) {
    $host  = $_SERVER['HTTP_HOST'];
    $page = "home";
    header("Location: http://$host/$page");
    return;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Users</title>
    <meta class="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/view/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css"
          href="https://cdn.datatables.net/s/bs/dt-1.10.10,se-1.1.0/datatables.min.css"/>
    <link rel="stylesheet" href="/view/css/styles.css">

    <script src="/view/js/jquery-1.11.3.min.js"></script>
    <script src="/view/js/bootstrap.min.js"></script>
    <script src="/view/js/sidebar_menu.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/s/bs/dt-1.10.10,se-1.1.0/datatables.min.js"></script>

    <script type="text/javascript" charset="utf-8">
        $(document).ready(function () {
            var table = $('.table').DataTable({
                select: true
            });

            $('#approve').click(function () {
                var ids = $.map(table.rows('.selected').data(), function (item) {
                    return item[0]
                });
                if(ids.length==0) {
                    alert("You haven't selected any user.");
                    return;
                }
                $.post('/users', {'action': 'approve', 'ids[]': ids}, function(data){
                    location.reload();
                });
            });
            $('#delete').click(function () {
                var ids = $.map(table.rows('.selected').data(), function (item) {
                    return item[0]
                });
                if(ids.length==0) {
                    alert("You haven't selected any user.");
                    return;
                }
                var r = confirm("Are you sure you want to delete "+ids.length+" user(s)? This action cannot be reversed.");
                if (r == true) {
                    $.post('/users', {'action': 'delete', 'ids[]': ids}, function(data){
                        location.reload();
                    });
                }
            });
            $('#add-group').click(function () {
                var ids = $.map(table.rows('.selected').data(), function (item) {
                    return item[0]
                });
                if(ids.length==0) {
                    alert("You haven't selected any user.");
                    return;
                }
                var groupId = $('#group-select').val();
                var group = $('#group-select :selected').text();
                var r = confirm("Are you sure you want to assign "+ids.length+" user(s) to the group '"+group+"'");
                if (r == true) {
                    $.post('/users', {'action': 'set_group', 'ids[]': ids, 'group_id':groupId}, function(data){
                        location.reload();
                    });
                }
            });
            $('#change-type').click(function () {
                var ids = $.map(table.rows('.selected').data(), function (item) {
                    return item[0]
                });
                if(ids.length==0) {
                    alert("You haven't selected any user.");
                    return;
                }
                var type = $('#type-select').val();
                var typeText = $('#type-select :selected').text();
                var r = confirm("Are you sure you want to change the type of "+ids.length+" user(s) to '"+typeText+"'");
                if (r == true) {
                    $.post('/users', {'action': 'set_type', 'ids[]': ids, 'type':type}, function(data){
                        location.reload();
                    });
                }
            });
        });
    </script>

</head>
<!--<xs->phone,sm->tablets,md->normal desktop,lg->large desktop >-->

<body>
<?php
include("header.php");
?>
<!-- /navbar-->
<div id="wrapper">
    <?php include("sidebar.php"); ?>
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <h2 class="col-md-3">Manage Users</h2>
                <div class="col-md-6 col-md-offset-3">
                    <div class="row">
                        <div class="row" style="text-align: right;margin-right: 16px;">
                            <select id="type-select">
                                <option value="1">Student</option>
                                <option value="2">Mentor</option>
                                <?php
                                if ($_SESSION['user']->getType() == 4) {
                                    ?>
                                    <option value="3">Assistant</option>
                                    <option value="4">Administrator</option>
                                <?php } ?>
                            </select>
                            <input type="button" id="change-type" value="Change Type">
                        </div>
                        <div class="row" style="text-align: right;margin-right: 16px;">
                            <select id="group-select">
                                <?php
                                $groups = $groupCrud->read();
                                foreach ($groups as $group) {
                                    echo "<option value='" . $group['id'] . "'>" . $group['name'] . "</option>";
                                }
                                ?>
                            </select>
                            <input type="button" id="add-group" value="Assign to Group">
                        </div>
                        <div class="row" style="text-align: right;margin-right: 16px;">
                        <input type="button" id="approve" value="Approve">
                        <input type="button" id="delete" value="Delete">
                            </div>
                    </div>
                </div>
            </div>
            <hr/>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Username</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Group</th>
                    <th>Type</th>
                    <th>Registered</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($users as $row) {
                    echo "<tr>";
                    echo "<td>" . $row['id'] . "</td>";
                    echo "<td><a href='/profile/" . $row['id'] . "'>" . $row['username'] . "</a></td>";
                    echo "<td>" . $row['name'] . " " . $row['surname'] . "</td>";
                    echo "<td>" . $row['email'] . "</td>";
                    $group = $groupCrud->read($row['group_id']);
                    echo "<td><a href='/groups/" . $row['group_id'] . "'>" . $group[0]["name"] . "</a></td>";
                    echo "<td>" . getTypeString($row['type']) . "</td>";
                    echo "<td>" . date_format(date_create($row['timestamp']), 'Y-m-d H:i:s') . "</td>";
                    echo "</tr>";
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
    <!-- /#page-content-wrapper -->
</div>
</body>
</html>