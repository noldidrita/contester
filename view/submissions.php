<?php
/**
 * Created by PhpStorm.
 * User: TDC
 * Date: 1/22/16
 * Time: 1:01 AM
 */

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
    <title>Submissions</title>
    <meta class="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/view/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/s/bs/dt-1.10.10/datatables.min.css"/>
    <link rel="stylesheet" href="/view/css/styles.css">
    <link rel="stylesheet" href="/view/css/codemirror.css">
    <link rel="stylesheet" href="/view/css/material.css">

    <script src="/view/js/jquery-1.11.3.min.js"></script>
    <script src="/view/js/bootstrap.min.js"></script>
    <script src="/view/js/sidebar_menu.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/s/bs/dt-1.10.10/datatables.min.js"></script>
    <script src="/view/js/codemirror.js"></script>
    <script src="/view/js/javascript.js"></script>
    <script src="/view/js/foldcode.js"></script>
    <script src="/view/js/matchbrackets.js"></script>
    <script src="/view/js/active-line.js"></script>

    <script type="text/javascript" charset="utf-8">
        /*
        $(document).ready(function () {
            var textArea = document.getElementById("text");
            window.editor = CodeMirror.fromTextArea(textArea, {
                mode: "javascript",
                lineNumbers: true,
                lineWrapping: true,
                matchBrackets: true,
                styleActiveLine: true,
                theme: "material",
                foldGutter: {
                    rangeFinder: new CodeMirror.fold.combine(CodeMirror.fold.brace, CodeMirror.fold.comment)
                },
                gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"]
            });
        });
        */
    </script>

</head>
<!--<xs->phone,sm->tablets,md->normal desktop,lg->large desktop >-->

<body>
<?php
include("header.php");
?>
<!-- /navbar-->
<div id="wrapper">
    <?php include("sidebar.php"); ?>
    <div id="page-content-wrapper">
        <?php
        if ($error == true) {
            echo '<div class="col-md-12 alert alert-danger" role="alert"><strong>Error: </strong><br/>' . $message . '</div>';
        } else if ($success == true) {
            echo '<div class="col-md-12 alert alert-success" role="alert"><strong>Success!</strong><br/> The report was successfully sent.</div>';
        }
        ?>
        <div class="container-fluid">
            <h2><a href="/problems/<?= $submission['problem_id'] ?>"><?= $submission['problem_name'] ?></a>
            </h2>
            <h4><?= $submission['timestamp'] ?></h4>
            <hr/>
            <div class="col-md-6">
                    <pre><?= htmlentities($submission['code'])?></pre>
            </div>
            <div class="col-md-6">
                <pre><?= htmlentities($submission['verdict']) ?></pre>
            </div>
            <?php
            if ($_SESSION['user']->getType() > 2 || ($_SESSION['user']->getType() == 2 && $_SESSION['user']->getGroupId() == $user['group_id'])) {
                ?>
                <div class="col-md-4 col-md-offset-4">
                    <form action='/submissions/<?= $submission['submission_id'] ?>' method="post">
                        <input class="form-control" type="submit" name="Submit" value="View Report"/>
                    </form>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
    <!-- /#page-content-wrapper -->
</div>
</body>
</html>