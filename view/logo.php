<?php
// Set the content-type
header('Content-Type: image/png');

// Create the image
$im = imagecreatetruecolor(600, 200);

// Create some colors
$white = imagecolorallocate($im, 255, 255, 255);
$grey = imagecolorallocate($im, 128, 128, 128);
$black = imagecolorallocate($im, 0, 0, 0);

imagecolortransparent($im, $black);

// The text to draw
$text = 'SAT';

$font = 'fonts/MrSunshine.ttf';
$roboto = 'fonts/Roboto-Regular.ttf';

$logo = imagecreatefrompng("img/epoka-logo.png");
imagecopy($im, $logo, 10,10, 0, 0, 150, 160);

// Add the text
imagettftext($im, 100, 0, 180, 130, $white, $font, $text);
imagettftext($im, 18, 0, 00, 190, $white, $roboto, "Student Assesment and Training System");


// Using imagepng() results in clearer text compared with imagejpeg()
imagepng($im);
imagedestroy($im);