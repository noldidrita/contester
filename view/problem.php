<?php
/**
 * Created by PhpStorm.
 * User: TDC
 * Date: 24.1.2016
 * Time: 19:02
 */
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>SAT</title>
    <meta class="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/view/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/s/bs/dt-1.10.10/datatables.min.css"/>
    <link rel="stylesheet" href="/view/css/styles.css">
    <link rel="stylesheet" href="/view/css/codemirror.css">
    <link rel="stylesheet" href="/view/css/material.css">

    <script src="/view/js/jquery-1.11.3.min.js"></script>
    <script src="/view/js/bootstrap.min.js"></script>
    <script src="/view/js/sidebar_menu.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/s/bs/dt-1.10.10/datatables.min.js"></script>

    <script src="/view/js/codemirror.js"></script>
    <script src="/view/js/javascript.js"></script>
    <script src="/view/js/foldcode.js"></script>
    <script src="/view/js/matchbrackets.js"></script>
    <script src="/view/js/active-line.js"></script>

    <script type="text/javascript" charset="utf-8">
        $(document).ready(function () {
            var textArea = document.getElementById("text");
            window.editor = CodeMirror.fromTextArea(textArea, {
                mode: "javascript",
                lineNumbers: true,
                lineWrapping: true,
                matchBrackets: true,
                styleActiveLine: true,
                theme: "material",
                foldGutter: {
                    rangeFinder: new CodeMirror.fold.combine(CodeMirror.fold.brace, CodeMirror.fold.comment)
                },
                gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"]
            });

            $('.table').DataTable({
                "order": [[0, "desc"]]
            });
        });

    </script>

</head>
<!--<xs->phone,sm->tablets,md->normal desktop,lg->large desktop >-->

<body>
<?php
include("header.php");
?>
<!-- /navbar-->
<div id="wrapper">
    <?php include("sidebar.php"); ?>
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <?php
                if($error == true) {
                    echo '<div class="col-md-12 alert alert-danger" role="alert"><strong>Error: </strong><br/>'.$message.'</div>';
                }
                ?>
                <div class="col-md-6">
                    <h2><?= $problem['title'] ?></h2>
                    <h4><?= $problem['subtitle'] ?></h4>
                </div>
                <div class="col-md-6" style="text-align: right">
                    <h4>Time
                        Limits: <?= $problem['time_limit_1'] . "s/" . $problem['time_limit_2'] . "s/" . $problem['time_limit_3'] . "s" ?></h4>
                    <h4>Memory
                        Limits: <?= $problem['memory_limit_1'] . "KB/" . $problem['memory_limit_2'] . "KB/" . $problem['memory_limit_3'] . "KB" ?></h4>
                    <h4>
                        <a href="/comments/<?= $problem['id'] ?>">View coments from other users</a>
                    </h4>

                    <?php if(isset($_SESSION['user']) && $_SESSION['user']->getType()>2) {
                        echo "<h4><a href='/edit-problem/" . $problem['id'] . "'>Edit this problem</a></h4>";
                    }
                    ?>

                </div>
            </div>
            <hr/>

            <div class="row">
                <div class="col-md-12">
                    <?= $problem['statement'] ?>
                </div>
            </div>

            <div class="row"
                 style="background-color:#ececec; border-top-left-radius: 10px; border-top-right-radius: 10px; margin-top: 40px;">
                <div class="col-md-12"
                     style="background-color:#c3c3c3; border-top-left-radius: 10px; border-top-right-radius: 10px; text-align: center; margin-bottom: 6px">
                    <h5 style="color: #505050;">Submit a solution</h5>
                </div>
                <form action="/problems/submit/<?= $problem['id'] ?>" method="post">
                    <div class="col-md-6 col-md-offset-3" style="margin-bottom: 15px;">
                        <textarea class="form-control" rows="10" name="code" id="text"></textarea>
                    </div>

                    <div class="col-md-2 col-md-offset-4">
                        <select class="form-control" name="language" id="language" required>
                            <?php
                            foreach ($compilers as $compiler) {
                                echo "<option value='" . $compiler['value'] . "'>" . $compiler['name'] . "</option>";
                            }
                            ?>
                        </select>
                    </div>

                    <div class="col-md-2">
                        <input class="form-control" type="submit" name="Submit" value="Submit"/>
                    </div>
                    <?php
                        if(!isset($_SESSION['user'])) {
                            echo '<div class="col-md-12 alert alert-danger" role="alert">You cannot submit solutions until you log in.</div>';
                    }
                    ?>
                </form>
            </div>
            <?php
            if (isset($submissions)) {
                ?>
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Timestamp</th>
                        <th>Username</th>
                        <th>Verdict</th>
                        <th>Language</th>
                        <th>Options</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach ($submissions as $row) {
                        echo "<tr>";
                        echo "<td>" . $row['timestamp'] . "</td>";
                        echo "<td>" . $row['username'] . "</td>";
                        if ($row['message'] == "") {
                            echo "<td>Checking...</td>";
                        } else {
                            echo "<td>" . $row['message'] . "</td>";
                        }
                        echo "<td>" . $row['language'] . "</td>";
                        echo "<td><a href='/submissions/" . $row['submission_id'] . "'>View Details</a></td>";
                        echo "</tr>";
                    }
                    ?>
                    </tbody>
                </table>
            <?php } ?>
        </div>
    </div>
    <!-- /#page-content-wrapper -->
</div>
<?php
if (isset($problem_posted) && $problem_posted == true) {
    echo "<script> window.location = window.location.href; </script>";
}

?>
</body>
</html>
