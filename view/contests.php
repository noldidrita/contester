<?php
/**
 * Created by PhpStorm.
 * User: luba
 * Date: 1/22/16
 * Time: 9:21 PM
 */

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Contests</title>
    <meta class="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/view/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css"
          href="https://cdn.datatables.net/s/bs/dt-1.10.10,se-1.1.0/datatables.min.css"/>
    <link rel="stylesheet" href="/view/css/styles.css">

    <script src="/view/js/jquery-1.11.3.min.js"></script>
    <script src="/view/js/bootstrap.min.js"></script>
    <script src="/view/js/sidebar_menu.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/s/bs/dt-1.10.10,se-1.1.0/datatables.min.js"></script>
    <script type="text/javascript" src="/view/js/moment.min.js"></script>
    <style>
        input {
            min-width: 210px;
        }
    </style>
    <script type="text/javascript" charset="utf-8">
        $(document).ready(function () {
            var table = $('.table').DataTable({
                select: true
            });
            $('#delete').click(function () {
                var ids = $.map(table.rows('.selected').data(), function (item) {
                    return item[0]
                });
                if (ids.length == 0) {
                    alert("You haven't selected any contests.");
                    return;
                }
                if (ids[0] == '1') {
                    alert("You cannot delete the contest 'No Contest'.");
                    return;
                }
                var r = confirm("Are you sure you want to delete " + ids.length + " contest(s)? This action cannot be reversed.");
                if (r == true) {
                    $.post('/contests', {'action': 'delete', 'ids[]': ids}, function (data) {
                        console.log(data);
                        location.reload();
                    });
                }
            });

        });
    </script>

</head>
<!--<xs->phone,sm->tablets,md->normal desktop,lg->large desktop >-->

<body>
<?php
include("header.php");
?>
<!-- /navbar-->
<div id="wrapper">
    <?php include("sidebar.php");?>
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <?php
                if($error == true) {
                    echo '<div class="col-md-12 alert alert-danger" role="alert"><strong>Error: </strong><br/>'.$insert_result['message'].'</div>';
                }
                ?>
                <div class="col-md-4">
                    <h2 class="h2-style">Contests</h2>
                </div>
                <div class="col-md-8" style="text-align: right">
                    <?php if (isset($_SESSION['user']) && $_SESSION['user']->getType() > 2) { ?>
                        <div class="" style="text-align: right;margin-right: 16px">
                            <form action="/contests" method="post">
                                <input type="text" name="contest_name" placeholder="Contest Name" required>
                                <input type="text" name="contest_description" placeholder="Contest Description"
                                       required><br/>
                                <input type="datetime" name="start_time" placeholder="Start(YYYY-MM-dd HH:mm:ss)"
                                       required>
                                <input type="datetime" name="end_time" placeholder="End(YYYY-MM-dd HH:mm:ss)"
                                       required><br/>
                                <input type="submit" name="submit" value="Add">
                            </form>
                            <br/>
                            <input type="button" id="delete" value="Delete Selected">
                        </div>
                    <?php } ?>
                </div>
            </div>
            <hr/>
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Number of problems</th>
                    <th>Time</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach($result as $row){
                    $crtime=date('Y-m-d H:i:s');

                    if($crtime >= $row['start_time'] && $crtime <$row['end_time'])
                        echo "<tr class='success'>";
                    else echo "<tr>";
                    echo "<td>".$row['id']."</td>";
                    echo "<td><a href='/contests/".$row['id']."'>".$row['title']."</a></td>";
                    echo "<td>".$row['description']."</td>";
                    echo "<td>".$row['nr_problems']."</td>";
                    if($crtime >= $row['start_time'] && $crtime <$row['end_time']) {
                            ?>
                        <td id="<?=$row['id']?>">Left Time: 0 days 00:00:00 </td>
                        <script>

                            var updateFunction = function(){
                                var now = moment();
                                var end = moment('<?=$row['end_time']?>');
                                var diff = moment.duration(end.diff(now));
                                $('#<?=$row['id']?>').html("Left time: "+((diff.days()>0) ? (diff.days()+" days ") : "")+diff.hours()+":"+diff.minutes()+":"+diff.seconds());
                            };
                            updateFunction();
                            setInterval(updateFunction, 1000);
                        </script>
                <?php
                        }
                    else if($row['id']!=1)
                        echo "<td class='text-danger'>Finished: ".$row['end_time']."</td>";
                    else echo "<td></td>";

                    echo "</tr>";
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
    <!-- /#page-content-wrapper -->
</div>
</body>
</html>