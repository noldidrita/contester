<?php
/**
 * Created by PhpStorm.
 * User: luba
 * Date: 1/23/16
 * Time: 02:30 PM
 */

include_once("../model/users/User.class.php");
include_once("../model/homeworks/Homework.crud.php");
include_once("../controller/util.php");
include_once("../model/groups/Group.crud.php");

use model\users\User as User;
use model\homeworks\Crud as HomeworkCrud;
use model\groups\Crud as GroupCrud;

session_start();

$crud = new HomeworkCrud();
$result = $crud->read();
$groupCrud = new GroupCrud();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Help</title>
    <meta class="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="view/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css"
          href="https://cdn.datatables.net/s/bs/dt-1.10.10,se-1.1.0/datatables.min.css"/>
    <link rel="stylesheet" href="view/css/styles.css">

    <script src="view/js/jquery-1.11.3.min.js"></script>
    <script src="view/js/bootstrap.min.js"></script>
    <script src="view/js/sidebar_menu.js"></script>



</head>
<!--<xs->phone,sm->tablets,md->normal desktop,lg->large desktop >-->

<body>
<?php
include("header.php");
?>
<!-- /navbar-->
<div id="wrapper">
    <?php
    include("sidebar.php");
    ?>
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <h2 class="h2-style">Help</h2>
            <hr/>

            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            <a class="collapsed"  role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                How do i write a solution in C++?
                            </a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body ">
                          Your solution should read the input data from the standard input (console) or from <i>input.txt</i> file, and write the output data to the standard output (console). Below are the example solutions of A+B problem:
                          <div class="row">
                            <div class="col-sm-6">
                                <pre>
#include &lt;iostream&gt;
using namespace std;

int main()
{

     int a, b;
     cin >> a;
     cin >> b;
     cout << a + b << "\n";
     return 0;
}                               </pre>
                            </div>
                            <div class="col-sm-6"> <pre>
#include &lt;iostream&gt;
using namespace std;

int main()
{
   freopen("input.txt", "rt", stdin);
   int a, b;
   cin >> a >> b;
   cout << a + b;
   return 0;
}
                  </pre>
                            </div>
                        </div>
                            Exceptions, thrown by solutions during runtime inside a <i>try..catch</i> block, are ignored by the system. Exceptions, thrown by solutions outside a <i>try..catch </i>block, cause a Runtime Error. Solutions are compiled on the server with an ONLINE_JUDGE directive turned on.
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwo">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                How do i write a solution in Java?
                            </a>
                        </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                        <div class="panel-body">
                            Your solution should read the input data from the standard input (console) or from <i>input.txt</i> file, and write the output data to the standard output (console). Below are the example solutions of A+B problem:
                            <pre>
import java.io.*;
import java.util.*;

class Solver
{
  public static void main(String[] args) throws IOException
  {
    BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    PrintWriter out = new PrintWriter(new OutputStreamWriter(System.out));
    StringTokenizer str;
    str = new StringTokenizer(in.readLine());
    int a = Integer.parseInt(str.nextToken());
    str = new StringTokenizer(in.readLine());
    int b = Integer.parseInt(str.nextToken());
    out.println(a + b);
    out.flush();
  }
}

                        </pre>
                            <pre>
import java.io.*;
import java.util.*;

class Solver
{
  public static void main(String[] args) throws IOException
  {
    BufferedReader in = new BufferedReader(new FileReader("input.txt"));
    PrintWriter out = new PrintWriter(new OutputStreamWriter(System.out));
    StringTokenizer str;
    str = new StringTokenizer(in.readLine());
    int a = Integer.parseInt(str.nextToken());
    str = new StringTokenizer(in.readLine());
    int b = Integer.parseInt(str.nextToken());
    out.println(a + b);
    out.flush();
  }
}
                            </pre>
                            Solutions are compiled on the server with an ONLINE_JUDGE directive turned on.
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                What results of checking mean?
                            </a>
                        </h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                        <div class="panel-body">
                          <div class="row">
                            <div class="col-sm-3">

                                 <p class="text-success">
                                     Accepted!
                                 </p>
                            </div>
                              <div class="col-sm-9">
                                <p>Solution was successfully compiled and passed all tests.</p>
                              </div>
                              <hr/>
                              <div class="col-sm-3">

                                  <p>
                                      Compilation Error
                                  </p>
                              </div>
                              <div class="col-sm-9">
                                  <dl>
                                      <dd>-Server compiler "considered" that solution had a syntax error.</dd>
                                      <dd>-Solution file was not saved in development environment or a wrong file was submitted.</dd>
                                      <dd>-Incorrect compiler / programming language was chosen.
                                          If compilation error occure, the compilation log becomes available to the user.</dd>
                                  </dl>
                              </div>
                              <hr/>
                              <div class="col-sm-3">

                                  <p>Wrong Answer</p>
                              </div>
                              <div class="col-sm-9">
                                  <dl>
                                      <dd>-Solution gave an incorrect answer for the test.</dd>
                                      <dd>-Solution file was not saved in development environment or a wrong file was submitted.</dd>
                                      <dd>-Solution contains uninitialized variables.</dd>
                                      <dd>-Iteration variable is used after "for". </dd>
                                  </dl>
                              </div>
                              <div class="col-sm-3">

                                  <p>Runtime Error</p>
                              </div>
                              <div class="col-sm-9">
                                  <dl>
                                      <dd>-A runtime error (exception) occured.</dd>
                                      <dd>-Solution works with files (can work only with <i>input.txt</i>).</dd>
                                  </dl>
                              </div>
                              <hr/>
                              <div class="col-sm-3">

                                  <p>Time Limit</p>
                              </div>
                              <div class="col-sm-9">
                                  <dl>
                                      <dd>-Solution consumed more time, than allowed by the statement.</dd>
                                      <dd>-Solution "hang".</dd>
                                  </dl>
                              </div>
                              <hr/>
                              <div class="col-sm-3">

                                  <p>Memory Limit</p>
                              </div>
                              <div class="col-sm-9">
                                  <dl>
                                      <dd>-Solution consumed more memory, than allowed by the statement.</dd>
                                  </dl>
                              </div>
                              <hr/>
                              <div class="col-sm-3">

                                  <p>Waiting</p>
                              </div>
                              <div class="col-sm-9">
                                  <dl>
                                      <dd>-Solutions waits for being checked. Server is busy checking other solutions.</dd>
                                  </dl>
                              </div>
                           </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /#page-content-wrapper -->
</div>
</body>
</html>