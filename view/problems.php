<?php
/**
 * Created by PhpStorm.
 * User: luba
 * Date: 1/24/16
 * Time: 2:13 PM
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Problems</title>
    <meta class="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/view/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/s/bs/dt-1.10.10/datatables.min.css"/>
    <link rel="stylesheet" href="/view/css/styles.css">

    <script src="/view/js/jquery-1.11.3.min.js"></script>
    <script src="/view/js/bootstrap.min.js"></script>
    <script src="/view/js/sidebar_menu.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/s/bs/dt-1.10.10/datatables.min.js"></script>
    <script type="text/javascript" src="/view/js/moment.min.js"></script>

    <script type="text/javascript" charset="utf-8">
        $(document).ready(function () {
            $('.table').DataTable();
        });
    </script>

</head>
<!--<xs->phone,sm->tablets,md->normal desktop,lg->large desktop >-->

<body>
<?php
include("header.php");
?>
<!-- /navbar-->
<div id="wrapper">
    <?php include("sidebar.php"); ?>
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <h2 class="h2-style"><?= $event['title'] ?></h2>
            <?php
            if (isset($event['start_time']) && isset($event['group_id']) && $event['id']!=1) { ?>
                <div style="text-align: right">
                    <h4>
                        <a href="/homeworks/scoreboard/<?= $event['id'] ?>">View scoreboard</a>
                    </h4>
                </div>
            <?php } else if (isset($event['start_time']) && $event['id']!=1) { ?>
                <div style="text-align: right">
                    <h4>
                        <a href="/contests/scoreboard/<?= $event['id'] ?>">View scoreboard</a>
                    </h4>
                </div>
            <?php } ?>
            <hr/>
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Problem Name</th>
                    <th>Description</th>
                    <th>Difficulty</th>
                    <th>Attempts</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($problems as $row) {

                    echo "<tr>";
                    echo "<td><a href='/problems/" . $row['id'] . "'>" . $row['title'] . "</a></td>";
                    echo "<td>" . $row['shortdescription'] . "</td>";
                    echo "<td style='text-align: center;'><img  height='40px'src='/view/img/" . $difficulty_array[$row['difficulty']] . "'></td>";
                    echo "<td>" . $row['attempts'] . "</td>";
                    echo "</tr>";
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
    <!-- /#page-content-wrapper -->
</div>
</body>
</html>
