<?php
/**
 * Created by PhpStorm.
 * User: luba
 * Date: 1/24/16
 * Time: 7:14 PM
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Profile</title>
    <meta class="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/view/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/s/bs/dt-1.10.10/datatables.min.css"/>
    <link rel="stylesheet" href="/view/css/styles.css">

    <script src="/view/js/jquery-1.11.3.min.js"></script>
    <script src="/view/js/bootstrap.min.js"></script>
    <script src="/view/js/sidebar_menu.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/s/bs/dt-1.10.10/datatables.min.js"></script>
    <script type="text/javascript" src="/view/js/moment.min.js"></script>

    <script type="text/javascript" charset="utf-8">
        $(document).ready(function () {
            var table = $('#table').DataTable({
                "order": [[0, "desc"]]
            });
        });
    </script>
</head>
<!--<xs->phone,sm->tablets,md->normal desktop,lg->large desktop >-->

<body>
<!--navbar and logo-->
<?php include("header.php") ?>
<!-- /navbar-->
<div id="wrapper">
    <!-- Sidebar -->
    <?php include("sidebar.php");
    ?>
    <!-- /#sidebar-wrapper -->
    <!-- Page Content -->
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="h2-style">Profile</h2>
                    <hr/>
                    <div class="profile-container">
                        <div class="col-sm-3" style="text-align: center;">
                            <img src='/uploads/profileImg/<?= !empty($user->getPictureUrl()) ? $user->getPictureUrl() : "placeholder.svg" ?>' class="img-rounded"
                                 width="170" height="170"><br/>
                            <?php
                            if (isset($_SESSION['user']) && ($_SESSION['user']->getId() == $user->getId() || $_SESSION['user']->getType() == 4)) {
                                ?>
                                <button type="button" class="btn btn-primary btn-edit"><span
                                        class="glyphicon glyphicon-pencil margin-icon-side"
                                        aria-hidden="true"></span><a href="/profile/edit/<?=$user->getId()?>">Edit</a></button>
                            <?php } ?>
                        </div>
                        <div class="table-cointainer col-sm-4 " style="line-height: 10px;">
                            <table class="table table-condensed">
                                <tr>
                                    <td class="td-style">Name :</td>
                                    <td><?php echo $user->getName() ?></td>
                                </tr>
                                <tr>
                                    <td class="td-style">Surname :</td>
                                    <td><?php echo $user->getSurname() ?></td>
                                </tr>
                                <tr>
                                    <td class="td-style">Username :</td>
                                    <td><?php echo $user->getUsername() ?></td>
                                </tr>
                                <tr>
                                    <td class="td-style">Email :</td>
                                    <td><?php echo $user->getEmail() ?></td>
                                </tr>
                                <tr>
                                    <td class="td-style">Gender :</td>
                                    <td><?php echo $user->getGender() ?></td>
                                </tr>
                                <tr>
                                    <td class="td-style">Birthday :</td>
                                    <td><?php $date = new DateTime($user->getBirthdate());

                                        echo $date->format('Y-m-d');
                                        ?></td>
                                </tr>
                                <tr>
                                    <td class="td-style">Role :</td>
                                    <td><?php echo getTypeString($user->getType()) ?></td>
                                </tr>
                                <tr>
                                    <td class="td-style">Signed up :</td>
                                    <td><?php $date = new DateTime($user->getTimestamp());

                                        echo $date->format('Y-m-d');
                                        ?></td>
                                </tr>
                                <tr>
                                    <td class="td-style">Accepted :</td>
                                    <td><?php echo $user->getAcceptCnt() ?></td>
                                </tr>
                                <tr>
                                    <td class="td-style">Attempted :</td>
                                    <td><?php echo $user->getAttemptsCnt() ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <?php
                    if (isset($submissions)) {
                        ?>
                        <table class="table table-bordered table-striped" id="table">
                            <thead>
                            <tr>
                                <th>Timestamp</th>
                                <th>Username</th>
                                <th>Verdict</th>
                                <th>Language</th>
                                <th>Options</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach ($submissions as $row) {
                                echo "<tr>";
                                echo "<td>" . $row['timestamp'] . "</td>";
                                echo "<td>" . $row['username'] . "</td>";
                                if ($row['message'] == "") {
                                    echo "<td>Checking...</td>";
                                } else {
                                    echo "<td>" . $row['message'] . "</td>";
                                }
                                echo "<td>" . $row['language'] . "</td>";
                                echo "<td><a href='/submissions/" . $row['submission_id'] . "'>View Details</a></td>";
                                echo "</tr>";
                            }
                            ?>
                            </tbody>
                        </table>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <!-- /#page-content-wrapper -->
</div>
</body>
</html>
