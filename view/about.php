<?php
/**
 * Created by PhpStorm.
 * User: TDC
 * Date: 25.1.2016
 * Time: 14:39
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>About</title>
    <meta class="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/view/css/bootstrap.min.css">
    <link rel="stylesheet" href="/view/css/styles.css">

    <script src="/view/js/jquery-1.11.3.min.js"></script>
    <script src="/view/js/bootstrap.min.js"></script>
    <script src="/view/js/sidebar_menu.js"></script>

</head>
<!--<xs->phone,sm->tablets,md->normal desktop,lg->large desktop >-->

<body>
<!--navbar and logo-->
<?php include("header.php") ?>
<!-- /navbar-->
<div id="wrapper">
    <!-- Sidebar -->
    <?php include("sidebar.php"); ?>
    <!-- /#sidebar-wrapper -->
    <!-- Page Content -->
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12" style="text-align: center;">
                    <h1>SAT</h1>
                    <h3>Student Assesment and Training system</h3>
                    <h4>Admin login: admin AdminAdmin123</h4>
                    <h2>Main Features over the old system(acm.epoka.edu.al):</h2>
                    <div class="col-md-6" style="text-align: left">
                        <h4>Multiple user types</h4>
                        <dl>
                            <dt>Not Approved</dt>
                            <dd>Users who are registered but are not yet approved by an Admin. They cannot submit
                                problems until they're registered
                            </dd>
                            <dt>Student</dt>
                            <dd>Approved users. They may submit solutions and view the verdict, but not the reports(The
                                actual test data). These users usually belong in groups(for ex: CEN II 2013).
                            </dd>
                            <dt>Mentors</dt>
                            <dd>Mentors are assigned to a group. They may participate in all events like normal users,
                                but they can also view reports for users belonging to the group they're assigned at.
                                This way, they may aid the students about what's wrong in their solution. They can not
                                see the solutions during contests.
                            </dd>
                            <dt>Assistants</dt>
                            <dd>Assistants have the power to approve users, assign mentors, view reports for any user,
                                even during contest times. They can also create new
                                problems/contests/homeworks/volumes/sections
                            </dd>
                            <dt>Admin</dt>
                            <dd>Administrators are the super users. They can do anything assistants can, but they can
                                also add/remove assistants, modify user data for any user registered in the platform,
                                create new admins
                            </dd>
                        </dl>

                    </div>
                    <div class="col-md-6" style="text-align: left">
                        <h4>Categorization of problems</h4>
                        <dl>
                            <dt>Volumes</dt>
                            <dd>Structure problems in groups like: Data Structures, Algorithms, Beginners, Intermediate
                                etc.
                            </dd>
                            <dt>Sections</dt>
                            <dd>Structure problems by their objective, for ex: Loops, Recursion, Graph Theory, Dynamic
                                Programming, Sorting etc.
                            </dd>
                            <dt>Contests</dt>
                            <dd>Contests have a time period (start time and end time) in which, every student submission
                                is added to a scoreboard. During contests, students may not see comments by other
                                students, but only by admins.
                            </dd>
                            <dt>Homeworks</dt>
                            <dd>Homeworks are essentially like contests, except they're longer in period and have a
                                group assigned to them.
                                While the aim of contests is to improve student skills/create an exam environment,
                                homeworks aim to allow the student to research the topic.
                            </dd>
                        </dl>
                    </div>
                    <div class="col-md-6" style="text-align: left">
                        <h4>Evaluation of problems</h4>
                        <p>The evaluation process is as follows.<br/> Whenever a user submits a problem, it is validated
                            and inserted in the database. Then the user is shown a success message while the problem is
                            evaluated in the background.<br/>
                            A compiler is loaded depending on the users language and it compiles the solution. Then, for
                            every test case of the problem, an input.txt, output.txt is provided to the program in the
                            same folder as the program itself. These are randomized to make sure the user cannot guess
                            the folder. The input/output is also piped into stdin/stdout so the user
                            can choose not to read from files.<br/>
                            The program is given [time_limit] time to finish(specific to each problem). If it does not
                            finish within that amount of time, it is killed by the system.
                            <br/>
                            Afterwards the judge program is loaded. The judge is stored into a different folder and it
                            is compiled and then executed, making sure to create the input.txt/output.txt/pattern.txt
                            files in it's folder. These files are the input, user's output, and problem's pattern(if
                            any).
                            The judge then gives the verdict for the given data. If the judge is not specified for a
                            problem, the system just compares the output and pattern files for each case.
                            If during it's execution, the program used more than [memory_limit] RAM, then it's solution
                            is not accepted.<Br/> All the data is stored in a randomly generated file name json file in
                            reports folder.
                            The test cases, differently from the old system, are assigned points. So, for example
                        <ul>
                            <li>Test 1 => 1 points</li>
                            <li>Test 2 => 5 points</li>
                            <li>Test 3 => 20 points</li>
                            <li>Test 4 => 30 points</li>
                        </ul>
                        This is done to better evaluate student submissions. For example, test 1 might be a simple one
                        that might be given as a sample in the problem statement as well. It should not have
                        the same points as test 4, which might be really hard.<br/>
                        Differently from the current system, all test cases are evalated. This is done to prevent cases
                        like the student failing a submissions only because test 2 didn't work, while all 10 other would
                        work.
                        Results are given as a commulative sum of all the points taken in all the tests.
                        </p>
                    </div>
                    <div class="col-md-6" style="text-align: left">
                        <h4>Compilers</h4>
                        <p>Compilers are loaded in runtime. The folder compilers should contain .xml files. This folder
                            is scanned and the xml files are loaded.
                            In the xml file, all the data needed to use that compiler should be specified according to
                            the sample compilers.</p>
                    </div>
                    <div class="col-md-6" style="text-align: left">
                        <h4>Reports</h4>
                        <p>Report files contain all the data for each test case. Only specific users may be able to view
                            these reports. When they do, the json file for that submissions is loaded, parsed and then
                            a pdf file is created with that data. Then the user may choose to send that report file to
                            the user that submited that solution.
                            If they do, an email is sent to the user's email address containing as an attachment the
                            report file.</p>
                    </div>
                    <div class="col-md-6" style="text-align: left">
                        <h4>Security</h4>
                        <ul>
                            <li>Every data that is fed into the database is validated by the validator classes and then
                                inserted using parameter binding to prevent SQL Injections.
                            </li>
                            <li>User codes are also sanitized after they've been evaluated to prevent XSS</li>
                            <li>Each page has a controller. The first thing the controller does is check the
                                session/user access type and get parameters to prevent unauthorized access
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-12">
                        <h3>Developed by Arnold Drita (CEN III) and Lubiana Saja(BINF III) as part of CEN 323
                            course </br>(Web Programming, PhD (c) Msc. Igli Hakrama)</h3>
                        <h4>This project is meant to be gifted to Epoka University's Computer Engineering Department and will
                            be open sourced later on. This of course after it's been
                            <br/><b>Redistribution of the project is <u>not allowed</u> unless given permission by the
                                authors.<b></h4>

                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->
    </div>
</body>
</html>