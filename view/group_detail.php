<?php
/**
 * Created by PhpStorm.
 * User: TDC
 * Date: 1/21/16
 * Time: 12:15 AM
 */
if (!isset($_SESSION['user']) && $_SESSION['user']->getType() < 3) {
    $host  = $_SERVER['HTTP_HOST'];
    $page = "home";
    header("Location: http://$host/$page");
    return;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Group</title>
    <meta class="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/view/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css"
          href="https://cdn.datatables.net/s/bs/dt-1.10.10,se-1.1.0/datatables.min.css"/>
    <link rel="stylesheet" href="/view/css/styles.css">

    <script src="/view/js/jquery-1.11.3.min.js"></script>
    <script src="/view/js/bootstrap.min.js"></script>
    <script src="/view/js/sidebar_menu.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/s/bs/dt-1.10.10,se-1.1.0/datatables.min.js"></script>

    <script type="text/javascript" charset="utf-8">
        $(document).ready(function () {
            var table = $('.table').DataTable({
                select: false
            });
        });
    </script>

</head>
<!--<xs->phone,sm->tablets,md->normal desktop,lg->large desktop >-->
<body>
<?php
include("header.php");
?>
<!-- /navbar-->
<div id="wrapper">
    <?php include("sidebar.php"); ?>
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <h2 class="col-md-5"><?= $group['name'] ?></h2>
                <div class="col-md-5 col-md-offset-2">
                    <div class="row">
                        <div class="row" style="text-align: right;">
                            <form action="/groups/<?=$group['id']?>" method="post">
                                <input type="hidden" name="group_id" value="<?=$group['id']?>"/>
                                <input type="text" name="group_name" placeholder="Group Name">
                                <input type="text" name="group_description" placeholder="Group Description">
                                <input type="submit" name="submit" value="Change">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <h4><?= $group['description'] ?></h4>
            <hr/>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Username</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Type</th>
                    <th>Registered</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($users as $row) {
                    echo "<tr>";
                    echo "<td>" . $row['id'] . "</td>";
                    echo "<td><a href='/profile/" . $row['id'] . "'>" . $row['username'] . "</a></td>";
                    echo "<td>" . $row['name'] . " " . $row['surname'] . "</td>";
                    echo "<td>" . $row['email'] . "</td>";
                    echo "<td>" . getTypeString($row['type']) . "</td>";
                    echo "<td>" . date_format(date_create($row['timestamp']), 'Y-m-d H:i:s') . "</td>";
                    echo "</tr>";
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
    <!-- /#page-content-wrapper -->
</div>
</body>
</html>