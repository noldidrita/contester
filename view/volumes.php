<?php
/**
 * Created by PhpStorm.
 * User: luba
 * Date: 1/21/16
 * Time: 12:15 AM
 */
include_once("../model/users/User.class.php");
include_once("../model/volumes/Volume.crud.php");
include_once("../controller/util.php");

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Volumes</title>
    <meta class="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/view/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css"
          href="https://cdn.datatables.net/s/bs/dt-1.10.10,se-1.1.0/datatables.min.css"/>
    <link rel="stylesheet" href="/view/css/styles.css">

    <script src="/view/js/jquery-1.11.3.min.js"></script>
    <script src="/view/js/bootstrap.min.js"></script>
    <script src="/view/js/sidebar_menu.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/s/bs/dt-1.10.10,se-1.1.0/datatables.min.js"></script>

    <script type="text/javascript" charset="utf-8">
        $(document).ready(function () {
            var table = $('.table').DataTable({
                select: true
            });

            $('#delete').click(function () {
                var ids = $.map(table.rows('.selected').data(), function (item) {
                    return item[0]
                });
                if (ids.length == 0) {
                    alert("You haven't selected any volumes.");
                    return;
                }
                if(ids[0]=='1') {
                    alert("You cannot delete the volume 'No Volume'.");
                    return;
                }
                var r = confirm("Are you sure you want to delete " + ids.length + " volume(s)? This action cannot be reversed.");
                if (r == true) {
                    $.post('/volumes', {'action': 'delete', 'ids[]': ids}, function (data) {
                        console.log(data);
                        location.reload();
                    });
                }
            });
        });
    </script>

</head>
<!--<xs->phone,sm->tablets,md->normal desktop,lg->large desktop >-->

<body>
<?php
include("header.php");
?>
<!-- /navbar-->
<div id="wrapper">
    <?php include("sidebar.php"); ?>
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <h2 class="h2-style col-md-3">Volumes</h2>
                <?php if (isset($_SESSION['user']) && $_SESSION['user']->getType() > 2) { ?>
                <div class="" style="text-align: right;margin-right: 16px">
                    <form action="/volumes" method="post">
                        <input type="text" name="volume_name" placeholder="Volume Name" required>
                        <input type="text" name="volume_description" placeholder="Volume Description" required>
                        <input type="submit" name="submit" value="Add">
                    </form>
                    <br/>
                    <input type="button" id="delete" value="Delete Selected">
                </div>
                <?php } ?>

            </div>
            <hr/>

            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Number of problems</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($result as $row) {
                    echo "<tr>";
                    echo "<td>" . $row['id'] . "</td>";
                    echo "<td><a href='/volumes/" . $row['id'] . "'>" . $row['name'] . "</a></td>";
                    echo "<td>" . $row['description'] . "</td>";
                    echo "<td>" . $row['nr_problems'] . "</td>";
                    echo "</tr>";
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
    <!-- /#page-content-wrapper -->
</div>
</body>
</html>