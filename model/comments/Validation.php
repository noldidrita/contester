<?php
/**
 * Created by PhpStorm.
 * User: luba
 * Date: 12/26/15
 * Time: 10:08 PM
 */

namespace model\comments;
use core\security;

include_once ('../core/security.php');

class Validation extends Security
{

    public $number = "/^[0-9]+$/";

    public function isNumber($number)//problem_id,user_id
    {
        return preg_match($this->number, $number);
    }

    public function get_number_criteria($element)
    {
        $s = " $element must contain only numbers";
        return $s;

    }
}