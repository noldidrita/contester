<?php
/**
 * Created by PhpStorm.
 * User: luba
 * Date: 12/26/15
 * Time: 10:08 PM
 */

namespace model\comments;

class Comment
{
    private $id;
    private $user_id;
    private $text;
    private $problem_id;
    private $timestamp;

    /**
     * Comment constructor.
     * @param $id
     * @param $user_id
     * @param $text
     * @param $problem_id
     * @param $timestamp
     */
    public function __construct($id="", $user_id="", $text="", $problem_id="", $timestamp="")
    {
        $this->id = $id;
        $this->user_id = $user_id;
        $this->text = $text;
        $this->problem_id = $problem_id;
        $this->timestamp = $timestamp;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param string $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function getProblemId()
    {
        return $this->problem_id;
    }

    /**
     * @param string $problem_id
     */
    public function setProblemId($problem_id)
    {
        $this->problem_id = $problem_id;
    }

    /**
     * @return string
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * @param string $timestamp
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;
    }


}