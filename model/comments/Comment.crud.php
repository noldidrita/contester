<?php
/**
 * Created by PhpStorm.
 * User: luba
 * Date: 12/26/15
 * Time: 10:08 PM
 */
namespace model\comments;

include_once("../core/crud_interface.php");
include_once("../core/database.php");
include_once("../model/comments/Validation.php");
include_once("../model/comments/Comment.class.php");

use \core\Crud as Crud_interface;
use \core\Database;
use \model\comments\Validation;

use \PDO;

class Crud implements Crud_interface
{
    private $insert_query = "INSERT INTO `comments`(`user_id`, `text`, `problem_id`, `timestamp`) VALUES (:user_id,:text,:problem_id,:timestamp)";
    private $select_query_all = "SELECT * FROM `comments`";
    private $select_query = "SELECT * FROM `comments` WHERE id=:id";
    private $update_query = "UPDATE `comments` SET `user_id`= :user_id,`text`= :text,`problem_id`= :problem_id,`timestamp`= :timestamp WHERE `id`= :id";
    private $delete_query = "DELETE FROM `comments` WHERE id=:id";
    private $problem_query = "SELECT comments.id as comment_id, users.id as user_id, users.username, users.type as user_type, users.pictureUrl, text, comments.timestamp as timestamp FROM `comments`, `users` WHERE problem_id=:id AND users.id=comments.user_id group by comments.id";

    private $db;

    public function __construct()
    {
        $db_object = new Database();
        $this->db = $db_object->get_db();
    }

    //  Implement create() method.
    public function create($comment)
    {

        $return_array = array(
            "success" => true,
            "message" => ""
        );

        //Validate the comment
        $validation = new Validation();

        if (!$validation->isNumber($comment->getProblemId())) {
            $return_array['success'] = false;
            $return_array['message'] .= $validation->get_number_criteria("problem_id") . "\n";
        }

        if (!$validation->isNumber($comment->getUserId())) {
            $return_array['success'] = false;
            $return_array['message'] .= $validation->get_number_criteria("user_id") . "\n";
        }


        //
        if ($return_array['success']) {

            //insert comment
            $stmt = $this->db->prepare($this->insert_query);
            $stmt->bindValue(":user_id", $comment->getUserId(), PDO::PARAM_INT);
            $stmt->bindValue(":problem_id", $comment->getProblemId(), PDO::PARAM_INT);
            $stmt->bindValue(":text", $validation->filter_html($comment->getText()), PDO::PARAM_STR);
            $stmt->bindValue(":timestamp", $comment->getTimestamp(), PDO::PARAM_STR);

            $stmt->execute();

            return $return_array;
        } else {
            return $return_array;
        }
    }

    //  Implement read() method.
    public function read($id = -1)
    {
        if ($id == -1) {
            $stmt = $this->db->prepare($this->select_query_all);
            $stmt->execute();

            $read_comments = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $read_comments;
        } else {
            $stmt = $this->db->prepare($this->select_query);
            $stmt->bindValue(":id", $id, PDO::PARAM_INT);
            $stmt->execute();

            $read_comments = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $read_comments;
        }
    }

    // Implement update() method.
    public function update($comment, $id)
    {
        $get_comment = $this->read($id);

        $update_comment = new Comment();

        $validation = new Validation();

        $return_array = array(
            "success" => true,
            "message" => ""
        );

        //validate//user_id
        if (!empty($comment->getUserId()) or $comment->getUserId() !== "") {
            if ($validation->isNumber($comment->getUserId())) {
                $update_comment->setUserId($comment->getUserId());
            } else {
                $return_array['success'] = false;
                $return_array['message'] = $validation->get_number_criteria("user_id");
            }
        } else {
            $update_comment->setUserId($get_comment[0]['user_id']);
        }
        //validate//problem_id
        if (!empty($comment->getProblemId()) or $comment->getProblemId() !== "") {
            if ($validation->isNumber($comment->getProblemId())) {
                $update_comment->setProblemId($comment->getProblemId());
            } else {
                $return_array['success'] = false;
                $return_array['message'] = $validation->get_number_criteria("problem_id");
            }
        } else {
            $update_comment->setProblemId($get_comment[0]['problem_id']);
        }

        $update_comment->setTimestamp($get_comment[0]['timestamp']);
        $update_comment->setId($get_comment[0]['id']);
        $update_comment->setText($validation->filter_html($get_comment[0]['text']));


        //update
        if ($return_array['success']) {

            $stmt = $this->db->prepare($this->update_query);
            $stmt->bindValue(":id", $update_comment->getId(), PDO::PARAM_INT);
            $stmt->bindValue(":user_id", $update_comment->getUserId(), PDO::PARAM_INT);
            $stmt->bindValue(":problem_id", $update_comment->getProblemId(), PDO::PARAM_INT);
            $stmt->bindValue(":text", $update_comment->getText(), PDO::PARAM_STR);
            $stmt->bindValue(":timestamp", $update_comment->getTimestamp(), PDO::PARAM_STR);

            $stmt->execute();

            return $return_array;
        } else {
            return $return_array;
        }
    }

    // Implement delete() method.
    public function delete($id)
    {
        $stmt = $this->db->prepare($this->delete_query);
        $stmt->bindValue(":id", $id, PDO::PARAM_INT);
        $stmt->execute();
        return true;
    }

    public function getForProblemId($problem_id)
    {
        $stmt = $this->db->prepare($this->problem_query);
        $stmt->bindValue(":id", $problem_id, PDO::PARAM_INT);
        $stmt->execute();

        $read_submissions = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $read_submissions;
    }
}