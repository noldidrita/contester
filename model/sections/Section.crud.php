<?php
/**
 * Created by PhpStorm.
 * User: luba
 * Date: 12/25/15
 * Time: 8:23 PM
 */
namespace model\sections;

include_once("../core/crud_interface.php");
include_once("../core/database.php");
include_once("../model/sections/Section.class.php");
include_once("../model/sections/Validation.php");

use \core\Crud as Crud_interface;
use \core\Database;
use \model\sections\Validation;

use \PDO;

class Crud implements Crud_interface
{
    private $insert_query = "INSERT INTO `sections`(`name`, `description`, `timestamp`) VALUES (:name,:description,:timestamp)";
    private $select_query_all = "SELECT sections.id, sections.name, sections.description, count(problems.id) as nr_problems from sections LEFT JOIN problems ON sections.id = problems.section_id group by sections.id";
    private $select_query = "SELECT sections.id, sections.name, sections.description, count(problems.id) as nr_problems from sections LEFT JOIN problems ON sections.id = problems.section_id WHERE sections.id=:id group by sections.id";
    private $update_query = "UPDATE `sections` SET `name`= :name,`description`= :description,`timestamp`= :timestamp WHERE `id`= :id";
    private $delete_query = "DELETE FROM `sections` WHERE `id`= :id";
    private $db;

    public function __construct()
    {
        $db_object = new Database();
        $this->db = $db_object->get_db();
    }

    //Implement create() method
    public function create($section)
    {
        $return_array = array(
            "success" => true,
            "message" => ""
        );

        //Validate the problem
        $validation = new Validation();

        if (!$validation->isText($section->getName())) {
            $return_array['success'] = false;
            $return_array['message'] .= $validation->get_text_criteria("name") . "\n";
        }
        if (!$validation->isText($section->getDescription())) {
            $return_array['success'] = false;
            $return_array['message'] .= $validation->get_text_criteria("description") . "\n";
        }

        if ($return_array['success']) {

            //insert problem
            $stmt = $this->db->prepare($this->insert_query);
            $stmt->bindValue(":name", $validation->filter_html($section->getName()), PDO::PARAM_STR);
            $stmt->bindValue(":description", $validation->filter_html($section->getDescription()), PDO::PARAM_STR);
            $stmt->bindValue(":timestamp", $section->getTimestamp(), PDO::PARAM_STR);
            $stmt->execute();

            return $return_array;
        } else {
            return $return_array;
        }
    }


    // Implement read() method.
    public function read($id = -1)
    {
        if ($id == -1) {
            $stmt = $this->db->prepare($this->select_query_all);
            $stmt->execute();

            $read_sections = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $read_sections;
        } else {
            $stmt = $this->db->prepare($this->select_query);
            $stmt->bindValue(":id", $id, PDO::PARAM_INT);
            $stmt->execute();

            $read_sections = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $read_sections;
        }
    }

    // Implement update() method.
    public function update($section, $id)
    {
        $get_section= $this->read($id);

        $update_section = new Section();

        $validation = new Validation();

        $return_array = array(
            "success" => true,
            "message" => ""
        );

        //validate//name
        if (!empty($section->getName()) or $section->getName() !== "") {
            if ($validation->isText($section->getName())) {
                $update_section->setName($section->getName());
            } else {
                $return_array['success'] = false;
                $return_array['message'] = $validation->get_text_criteria("name");
            }
        } else {
            $update_section->setName($get_section[0]['name']);
        }
        //validate//description
        if (!empty($section->getDescription()) or $section->getDescription() !== "") {
            if ($validation->isText($section->getDescription())) {
                $update_section->setDescription($section->getDescription());
            } else {
                $return_array['success'] = false;
                $return_array['message'] = $validation->get_text_criteria("description");
            }
        } else {
            $update_section->setDescription($get_section[0]['description']);
        }

        $update_section->setTimestamp($get_section[0]['timestamp']);
        $update_section->setId($get_section[0]['id']);

        //update
        if ($return_array['success']) {

            $stmt = $this->db->prepare($this->update_query);
            $stmt->bindValue(":id", $update_section->getId(), PDO::PARAM_INT);
            $stmt->bindValue(":name", $validation->filter_html($update_section->getName()), PDO::PARAM_STR);
            $stmt->bindValue(":description", $validation->filter_html($update_section->getDescription()), PDO::PARAM_STR);
            $stmt->bindValue(":timestamp", $update_section->getTimestamp(), PDO::PARAM_STR);
            $stmt->execute();

            return $return_array;
        }
        else {
            return $return_array;
        }
    }

    // Implement delete() method.
    public function delete($id)
    {
        $stmt = $this->db->prepare($this->delete_query);
        $stmt->bindValue(":id", $id, PDO::PARAM_INT);
        if(!$stmt->execute()) {
            $return_array['success'] = false;
            $return_array['message'] = json_encode($stmt->errorInfo());
        }
        return true;
    }

}