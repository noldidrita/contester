<?php
/**
 * Created by PhpStorm.
 * User: luba
 * Date: 12/25/15
 * Time: 7:01 PM
 */

namespace model\groups;

class Group
{

    private $id;
    private $name;
    private $description;
    private $timestamp;

    /**
     * Group constructor.
     * @param $id
     * @param $name
     * @param $description
     * @param $timestamp
     */
    public function __construct($id="", $name="", $description="", $timestamp="")
    {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
        $this->timestamp = $timestamp;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * @param mixed $timestamp
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;
    }


}