<?php
/**
 * Created by PhpStorm.
 * User: luba
 * Date: 12/25/15
 * Time: 7:01 PM
 */
namespace model\groups;

include_once("../core/crud_interface.php");
include_once("../core/database.php");
include_once("../model/groups/Group.class.php");
include_once("../model/groups/Validation.php");

use \core\Crud as Crud_interface;
use \core\Database;
use \model\groups\Validation;

use \PDO;

class Crud implements Crud_interface
{
    private $insert_query = "INSERT INTO `groups`(`name`, `description`, `timestamp`) VALUES (:name,:description,:timestamp)";
    private $select_query_all = "SELECT groups.id, groups.name, groups.description, groups.timestamp, count(users.id) as nr_users from groups LEFT JOIN users ON groups.id = users.group_id group by groups.id";
    private $select_query = "SELECT groups.id, groups.name, groups.description, groups.timestamp, count(users.id) as nr_users from groups LEFT JOIN users ON groups.id = users.group_id WHERE groups.id=:id group by groups.id";
    private $update_query = "UPDATE `groups` SET `name`= :name,`description`= :description,`timestamp`= :timestamp WHERE `id`= :id";
    private $delete_query = "DELETE FROM `groups` WHERE `id`= :id";
    private $db;

    public function __construct()
    {
        $db_object = new Database();
        $this->db = $db_object->get_db();
    }

    //Implement create() method
    public function create($group)
    {
        $return_array = array(
            "success" => true,
            "message" => ""
        );

        //Validate the problem
        $validation = new Validation();

        if (!$validation->isText($group->getName())) {
            $return_array['success'] = false;
            $return_array['message'] .= $validation->get_text_criteria("name") . "\n";
        }
        if (!$validation->isText($group->getDescription())) {
            $return_array['success'] = false;
            $return_array['message'] .= $validation->get_text_criteria("description") . "\n";
        }

        if ($return_array['success']) {

            //insert problem
            $stmt = $this->db->prepare($this->insert_query);
            $stmt->bindValue(":name", $validation->filter_html($group->getName()), PDO::PARAM_STR);
            $stmt->bindValue(":description", $validation->filter_html($group->getDescription()), PDO::PARAM_STR);
            $stmt->bindValue(":timestamp", $group->getTimestamp(), PDO::PARAM_STR);
            $stmt->execute();

            return $return_array;
        } else {
            return $return_array;
        }
    }

    // Implement read() method.
    public
    function read($id = -1)
    {
        if ($id == -1) {
            $stmt = $this->db->prepare($this->select_query_all);
            $stmt->execute();

            $read_groups = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $read_groups;
        } else {
            $stmt = $this->db->prepare($this->select_query);
            $stmt->bindValue(":id", $id, PDO::PARAM_INT);
            $stmt->execute();

            $read_groups = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $read_groups;
        }
    }

    // Implement update() method.
    public function update($group, $id)
    {
        $get_group = $this->read($id);

        $update_group = new Group();

        $validation = new Validation();

        $return_array = array(
            "success" => true,
            "message" => ""
        );

        //validate//name
        if (!empty($group->getName()) or $group->getName() !== "") {
            if ($validation->isText($group->getName())) {
                $update_group->setName($group->getName());
            } else {
                $return_array['success'] = false;
                $return_array['message'] = $validation->get_text_criteria("name");
            }
        } else {
            $update_group->setName($get_group[0]['name']);
        }
        //validate//description
        if (!empty($group->getDescription()) or $group->getDescription() !== "") {
            if ($validation->isText($group->getDescription())) {
                $update_group->setDescription($group->getDescription());
            } else {
                $return_array['success'] = false;
                $return_array['message'] = $validation->get_text_criteria("description");
            }
        } else {
            $update_group->setDescription($get_group[0]['description']);
        }

        $update_group->setTimestamp($get_group[0]['timestamp']);
        $update_group->setId($get_group[0]['id']);

        //update
        if ($return_array['success']) {

            $stmt = $this->db->prepare($this->update_query);
            $stmt->bindValue(":id", $update_group->getId(), PDO::PARAM_INT);
            $stmt->bindValue(":name", $validation->filter_html($update_group->getName()), PDO::PARAM_STR);
            $stmt->bindValue(":description", $validation->filter_html($update_group->getDescription()), PDO::PARAM_STR);
            $stmt->bindValue(":timestamp", $update_group->getTimestamp(), PDO::PARAM_STR);
            $stmt->execute();

            return $return_array;
        }
     else {
        return $return_array;
        }
    }

    // Implement delete() method.
    public function delete($id)
    {
        $stmt = $this->db->prepare($this->delete_query);
        $stmt->bindValue(":id", $id, PDO::PARAM_INT);
        $stmt->execute();
        return true;
    }

}