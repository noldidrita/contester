<?php
/**
 * Created by PhpStorm.
 * User: luba
 * Date: 12/25/15
 * Time: 9:02 PM
 */

namespace model\homeworks;
use core\security;

include_once ('../core/security.php');

class Validation extends Security
{

    public $text = "/^[a-zA-Z0-9.*#_'`\+\-\(\)\[\] ]+$/";
    public $timer = "/^(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})$/";
    public $array = "/^[0-9,]+$/";


    public function isText ($text)//title,description,language,
    {
        return preg_match($this->text, $text);
    }

    public function get_text_criteria($value)
    {
        $s = " $value must contain only letters, numbers,round and square brackets,undescore,star,hashtag";
        return $s;
    }


    public function isArray($array)//title,description
    {
        return preg_match($this->$array, $array);
    }

    public function get_array_criteria($value)
    {
        $s = " $value must contain only numbers and commas";
        return $s;
    }

    public function isTime($time)//start_time,end_time
    {
        return preg_match($this->timer,$time);
    }
    public function get_timer_criteria($element)
    {
        $s = " $element must have the following format YYYY-MM-dd HH:mm:ss";
        return $s;

    }
}
