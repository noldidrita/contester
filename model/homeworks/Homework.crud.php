<?php
/**
 * Created by PhpStorm.
 * User: luba
 * Date: 12/25/15
 * Time: 9:01 PM
 */
namespace model\homeworks;

include_once("../core/crud_interface.php");
include_once("../core/database.php");
include_once("../model/homeworks/Homework.class.php");
include_once("../model/homeworks/Validation.php");

use \core\Crud as Crud_interface;
use \core\Database;
use model\homeworks\Homework;
use \model\homeworks\Validation;

use \PDO;

class Crud implements Crud_interface
{
    private $insert_query = "INSERT INTO `homeworks`(`title`, `start_time`, `end_time`, `description`, `language`,`group_id`) VALUES (:title,:start_time,:end_time,:description,:language,:group_id)";
    private $select_query_all = "SELECT homeworks.id, homeworks.title, homeworks.description,homeworks.start_time,homeworks.end_time , homeworks.group_id, count(problems.id) as nr_problems from homeworks LEFT JOIN problems ON homeworks.id = problems.homework_id group by homeworks.id";
    private $select_query = "SELECT homeworks.id, homeworks.title, homeworks.description,homeworks.start_time,homeworks.end_time, homeworks.group_id, count(problems.id) as nr_problems from homeworks LEFT JOIN problems ON homeworks.id = problems.homework_id WHERE homeworks.id=:id  group by homeworks.id";
    private $update_query = "UPDATE `homeworks` SET `title`= :title,`description`= :description,`start_time`= :start_time,`end_time`=:end_time,`language`=:language,`group_id`=:group_id WHERE `id`= :id";
    private $delete_query = "DELETE FROM `homeworks` WHERE `id`= :id";
    private $homework_query = "SELECT id, title FROM `problems` WHERE homework_id=:id";

    private $scoreboard_query = 'SELECT users.id as user_id, users.name, problems.id as problem_id, problems.title, count(submissions.id) as nr_submissions,
    MAX(CAST(SUBSTRING_INDEX(submissions.message, "/", 1) AS UNSIGNED)) as points, max(CAST(RIGHT(submissions.message, LOCATE("/", submissions.message)-1)AS UNSIGNED)) as total,
    TIMESTAMPDIFF(MINUTE, homeworks.start_time, max(submissions.timestamp)) as time, max(submissions.timestamp) as timestamp
    FROM submissions, users, problems, homeworks WHERE submissions.timestamp > homeworks.start_time AND submissions.timestamp < homeworks.end_time
    AND users.id = submissions.user_id AND problems.homework_id = homeworks.id AND submissions.problem_id = problems.id AND homeworks.id = :id group by user_id, problem_id';
    private $db;

    public function __construct()
    {
        $db_object = new Database();
        $this->db = $db_object->get_db();
    }
    //implemented create method
    public function create($homework)
    {

        $return_array = array(
            "success" => true,
            "message" => ""
        );

        $validation = new Validation();

        if (!$validation->isText($homework->getTitle())) {
            $return_array['success'] = false;
            $return_array['message'] .= $validation->get_text_criteria("title") . "\n";
        }
        if (!$validation->isText($homework->getDescription())) {
            $return_array['success'] = false;
            $return_array['message'] .= $validation->get_text_criteria("description") . "\n";
        }
        /*
        if (!$validation->isText($homework->getLanguage())) {
            $return_array['success'] = false;
            $return_array['message'] .= $validation->get_text_criteria("language") . "\n";
        }
        */
        if (!$validation->isTime($homework->getStartTime())) {
            $return_array['success'] = false;
            $return_array['message'] .= $validation->get_timer_criteria("start_time") . "\n";
        }
        if (!$validation->isTime($homework->getEndTime())) {
            $return_array['success'] = false;
            $return_array['message'] .= $validation->get_timer_criteria("end_time") . "\n";
        }

        if ($return_array['success']) {
            $stmt = $this->db->prepare($this->insert_query);
            $stmt->bindValue(":title", $validation->filter_html($homework->getTitle()), PDO::PARAM_STR);
            $stmt->bindValue(":description", $validation->filter_html($homework->getDescription()), PDO::PARAM_STR);
            $stmt->bindValue(":start_time", $homework->getStartTime(), PDO::PARAM_STR);
            $stmt->bindValue(":end_time", $homework->getEndTime(), PDO::PARAM_STR);
            $stmt->bindValue(":language", $homework->getLanguage(), PDO::PARAM_STR);
            $stmt->bindValue(":group_id", $homework->getGroupId(), PDO::PARAM_INT);

            $stmt->execute();

            return $return_array;
        } else {
            return $return_array;
        }
    }
    //  Implement read() method.
    public function read($id = -1)
    {
        if ($id == -1) {
            $stmt = $this->db->prepare($this->select_query_all);
            $stmt->execute();

            $read_homeworks = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $read_homeworks;
        } else {
            $stmt = $this->db->prepare($this->select_query);
            $stmt->bindValue(":id", $id, PDO::PARAM_INT);
            $stmt->execute();

            $read_homeworks = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $read_homeworks;
        }
    }
    //  Implement update() method.
    public function update($homework, $id)
    {

        $homework = new Homework();
        $get_homework = $this->read($id);

        $update_homework = new Homework();

        $validation = new Validation();

        $return_array = array(
            "success" => true,
            "message" => ""
        );

        //validate//title
        if (!empty($homework->getTitle()) or $homework->getTitle() !== "") {
            if ($validation->isText($homework->getTitle())) {
                $update_homework->setTitle($homework->getTitle());
            } else {
                $return_array['success'] = false;
                $return_array['message'] = $validation->get_text_criteria("title");
            }
        } else {
            $update_homework->setTitle($get_homework[0]['title']);
        }
        //validate//description
        if (!empty($homework->getDescription()) or $homework->getDescription() !== "") {
            if ($validation->isText($homework->getDescription())) {
                $update_homework->setDescription($homework->getDescription());
            } else {
                $return_array['success'] = false;
                $return_array['message'] = $validation->get_text_criteria("description");
            }
        } else {
            $update_homework->setDescription($get_homework[0]['description']);
        }
        //validate//language
        if (!empty($homework->getLanguage()) or $homework->getLanguage() !== "") {
            if ($validation->isText($homework->getLanguage())) {
                $update_homework->setLanguage($homework->getLanguage());
            } else {
                $return_array['success'] = false;
                $return_array['message'] = $validation->get_text_criteria("language");
            }
        } else {
            $update_homework->setLanguage($get_homework[0]['language']);
        }
        //validate//start_time
        if (!empty($homework->getStartTime()) or $homework->getStartTime() !== "") {
            if ($validation->isTime($homework->getStartTime())) {
                $update_homework->setStartTime($homework->getStartTime());
            } else {
                $return_array['success'] = false;
                $return_array['message'] = $validation->get_timer_criteria("start_time");
            }
        } else {
            $update_homework->setStartTime($get_homework[0]['start_time']);
        }
        //validate//end_time
        if (!empty($homework->getEndTime()) or $homework->getEndTime() !== "") {
            if ($validation->isTime($homework->getEndTime())) {
                $update_homework->setEndTime($homework->getEndTime());
            } else {
                $return_array['success'] = false;
                $return_array['message'] = $validation->get_timer_criteria("end_time");
            }
        } else {
            $update_homework->setEndTime($get_homework[0]['end_time']);
        }

        $update_homework->setId($get_homework[0]['id']);
        $update_homework->setGroupId($get_homework[0]['group_id']);


        //update
        if ($return_array['success']) {

            $stmt = $this->db->prepare($this->update_query);
            $stmt->bindValue(":id", $update_homework->getId(), PDO::PARAM_INT);
            $stmt->bindValue(":title", $validation->filter_html($update_homework->getTitle()), PDO::PARAM_STR);
            $stmt->bindValue(":description", $validation->filter_html($update_homework->getDescription()), PDO::PARAM_STR);
            $stmt->bindValue(":start_time", $update_homework->getStartTime(), PDO::PARAM_STR);
            $stmt->bindValue(":end_time", $update_homework->getEndTime(), PDO::PARAM_STR);
            $stmt->bindValue(":language", $validation->filter_html($update_homework->getLanguage()), PDO::PARAM_STR);
            $stmt->bindValue(":group_id", $update_homework->getGroupId(), PDO::PARAM_STR);
            $stmt->execute();

            return $return_array;
        }
        else {
            return $return_array;
        }
    
    }
    public function getScoreboardResults($id = -1)
    {
        $stmt = $this->db->prepare($this->scoreboard_query);
        $stmt->bindValue(":id", $id, PDO::PARAM_INT);
        $stmt->execute();

        $read_submissions = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $read_submissions;
    }

    //  Implement delete() method
    public function delete($id)
    {
        $stmt = $this->db->prepare($this->delete_query);
        $stmt->bindValue(":id", $id, PDO::PARAM_INT);
        $stmt->execute();
        return true;
    }
}