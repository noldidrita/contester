<?php
/**
 * Created by PhpStorm.
 * User: luba
 * Date: 12/26/15
 * Time: 5:24 PM
 */

namespace model\submissions;
use core\security;

include_once ('../core/security.php');

class Validation extends Security
{

    public $language = "/^[a-zA-Z0-9.*#_'`\+\-\(\)\[\] ]+$/";
    public $number = "/^[0-9]+$/";

    public function language($language)
    {
        return preg_match($this->language, $language);
    }

    public function get_language_criteria()
    {
        $s = "Language must contain only capital letters or lowercase letters , numbers,round and square brackets,undescore,star,hashtag";
        return $s;
    }

   public function isNumber($number)//user_id,problem_id
   {
       return preg_match($this->number,$number);
   }
    public function get_number_criteria($element)
    {
        $s = " $element must contain only numbers";
        return $s;

    }
}
