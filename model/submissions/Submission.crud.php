<?php
/**
 * Created by PhpStorm.
 * User: luba
 * Date: 12/26/15
 * Time: 5:24 PM
 */

namespace model\submissions;

include_once("../core/crud_interface.php");
include_once("../core/database.php");
include_once("../model/submissions/Submission.class.php");
include_once("../model/submissions/Validation.php");

use \core\Crud as Crud_interface;
use \core\Database;
use \model\submissions\Validation;

use \PDO;

class Crud implements Crud_interface
{
    private $insert_query = "INSERT INTO `submissions`(`user_id`, `problem_id`, `code`, `language`, `verdict`, `message`, `timestamp`, `report_path`) VALUES (:user_id,:problem_id,:code,:language,:verdict,:message,:timestamp, :report_path)";
    private $select_query_all = "SELECT * FROM `submissions`";
    private $select_query = "SELECT * FROM `submissions` WHERE id=:id";
    private $update_query = "UPDATE `submissions` SET `user_id`= :user_id,`problem_id`= :problem_id,`code`= :code,`language`= :language, `verdict`= :verdict,`message`= :message,`report_path`= :report_path,`timestamp`= :timestamp WHERE `id`= :id";
    private $delete_query = "DELETE FROM `submission` WHERE id=:id";

    private $problem_query = "SELECT submissions.id as submission_id, submissions.timestamp as timestamp, users.username as username, problems.title as problem_name, user_id, submissions.verdict as verdict, submissions.message, submissions.language FROM `submissions`, users, problems WHERE problem_id=:problem_id AND users.id = submissions.user_id AND problems.id = problem_id ORDER BY `submissions`.`timestamp` DESC";
    private $user_id_problem_query = "SELECT submissions.id as submission_id, submissions.timestamp as timestamp, users.username as username, problems.title as problem_name, submissions.verdict as verdict, user_id, submissions.message, submissions.language FROM `submissions`, users, problems WHERE problem_id=:problem_id AND users.id = submissions.user_id AND problems.id = problem_id AND users.id=:user_id
ORDER BY `submissions`.`timestamp` DESC";
    private $user_id_group_query = "SELECT submissions.id as submission_id, submissions.timestamp as timestamp, users.username as username, problems.title as problem_name, submissions.verdict as verdict, user_id, submissions.message, submissions.language FROM `submissions`, users, problems WHERE problem_id=:problem_id AND users.id = submissions.user_id AND problems.id = problem_id AND (users.id=:user_id OR users.group_id = :group_id)
ORDER BY `submissions`.`timestamp` DESC";
    private $select_with_user_problem = "SELECT submissions.id as submission_id, submissions.timestamp as timestamp, submissions.report_path, submissions.code as code, submissions.verdict as verdict, users.username as username, problems.title as problem_name, problems.id as problem_id, user_id, submissions.message, submissions.language FROM `submissions`, users, problems WHERE submissions.id=:id AND users.id = submissions.user_id AND problems.id = problem_id";
    private $user_id_query = "SELECT submissions.id as submission_id, submissions.timestamp as timestamp, users.username as username, problems.title as problem_name, user_id, submissions.verdict as verdict, submissions.message, submissions.language FROM `submissions`, users, problems WHERE users.id = submissions.user_id AND problems.id = problem_id AND users.id=:user_id ORDER BY timestamp DESC";
    private $db;

    public function __construct()
    {
        $db_object = new Database();
        $this->db = $db_object->get_db();
    }

    //implement create() method
    public function create($submission)
    {
        $return_array = array(
            "success" => true,
            "message" => "",
            "id" => -1
        );

        //Validate the submission
        $validation = new Validation();

        if (!$validation->language($submission->getLanguage())) {
            $return_array['success'] = false;
            $return_array['message'] .= $validation->get_language_criteria() . "\n";
        }

        if (!$validation->isNumber($submission->getUserId())) {
            $return_array['success'] = false;
            $return_array['message'] .= $validation->get_number_criteria("user_id") . "\n";
        }

        if (!$validation->isNumber($submission->getProblemId())) {
            $return_array['success'] = false;
            $return_array['message'] .= $validation->get_number_criteria("Problem ID") . "\n";
        }


        if ($return_array['success']) {

            //insert submission
            $stmt = $this->db->prepare($this->insert_query);
            $stmt->bindValue(":user_id", $submission->getUserId(), PDO::PARAM_INT);
            $stmt->bindValue(":problem_id", $submission->getProblemId(), PDO::PARAM_INT);
            $stmt->bindValue(":code", $submission->getCode(), PDO::PARAM_STR);
            $stmt->bindValue(":language", $validation->filter_html($submission->getLanguage()), PDO::PARAM_STR);
            $stmt->bindValue(":verdict", $submission->getVerdict(), PDO::PARAM_STR);
            $stmt->bindValue(":message", $submission->getMessage(), PDO::PARAM_STR);
            $stmt->bindValue(":timestamp", $submission->getTimestamp(), PDO::PARAM_STR);
            $stmt->bindValue(":report_path", $submission->getReportPath(), PDO::PARAM_STR);

            $stmt->execute();
            $return_array["id"] = $this->db->lastInsertId();
            return $return_array;
        } else {
            return $return_array;
        }
    }

    //implement read() method
    public function read($id = -1)
    {
        if ($id == -1) {
            $stmt = $this->db->prepare($this->select_query_all);
            $stmt->execute();

            $read_submissions = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $read_submissions;
        } else {
            $stmt = $this->db->prepare($this->select_query);
            $stmt->bindValue(":id", $id, PDO::PARAM_INT);
            $stmt->execute();

            $read_submissions = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $read_submissions;
        }
    }

    // Implement update() method.
    public function update($submission, $id)
    {
        $get_submission = $this->read($id);

        $update_submission = new Submission();

        $validation = new Validation();

        $return_array = array(
            "success" => true,
            "message" => ""
        );

        //validate//user_id
        if (!empty($submission->getUserId()) or $submission->getUserId() !== "") {
            if ($validation->isNumber($submission->getUserId())) {
                $update_submission->setUserId($submission->getUserId());
            } else {
                $return_array['success'] = false;
                $return_array['message'] = $validation->get_number_criteria("user_id");
            }
        } else {
            $update_submission->setUserId($get_submission[0]['user_id']);
        }
        //validate//problem_id
        if (!empty($submission->getProblemId()) or $submission->getProblemId() !== "") {
            if ($validation->isNumber($submission->getProblemId())) {
                $update_submission->setProblemId($submission->getProblemId());
            } else {
                $return_array['success'] = false;
                $return_array['message'] = $validation->get_number_criteria("problem_id");
            }
        } else {
            $update_submission->setProblemId($get_submission[0]['problem_id']);
        }
        //validate//language
        if (!empty($submission->getLanguage()) or $submission->getLanguage() !== "") {
            if ($validation->language($submission->getLanguage())) {
                $update_submission->setLanguage($submission->getLanguage());
            } else {
                $return_array['success'] = false;
                $return_array['message'] = $validation->get_language_criteria();
            }
        } else {
            $update_submission->setLanguage($get_submission[0]['language']);
        }

        $update_submission->setTimestamp($get_submission[0]['timestamp']);
        $update_submission->setId($get_submission[0]['id']);

        if (!empty($submission->getVerdict()) or $submission->getVerdict() !== "") {
            $update_submission->setVerdict($submission->getVerdict());
        } else {
            $update_submission->setVerdict($get_submission[0]['verdict']);
        }

        if (!empty($submission->getMessage()) or $submission->getMessage() !== "") {
            $update_submission->setMessage($submission->getMessage());
        } else {
            $update_submission->setMessage($get_submission[0]['message']);
        }

        if (!empty($submission->getReportPath()) or $submission->getReportPath() !== "") {
            $update_submission->setReportPath($submission->getReportPath());
        } else {
            $update_submission->setReportPath($get_submission[0]['report_path']);
        }

        $update_submission->setCode($get_submission[0]['code']);

        //update
        if ($return_array['success']) {
            $stmt = $this->db->prepare($this->update_query);
            $stmt->bindValue(":id", $update_submission->getId(), PDO::PARAM_INT);
            $stmt->bindValue(":user_id", $update_submission->getUserId(), PDO::PARAM_INT);
            $stmt->bindValue(":problem_id", $update_submission->getProblemId(), PDO::PARAM_INT);
            $stmt->bindValue(":code", $update_submission->getCode(), PDO::PARAM_STR);
            $stmt->bindValue(":language", $validation->filter_html($update_submission->getLanguage()), PDO::PARAM_STR);
            $stmt->bindValue(":verdict", $update_submission->getVerdict(), PDO::PARAM_STR);
            $stmt->bindValue(":message", $update_submission->getMessage(), PDO::PARAM_STR);
            $stmt->bindValue(":timestamp", $update_submission->getTimestamp(), PDO::PARAM_STR);
            $stmt->bindValue(":report_path", $update_submission->getReportPath(), PDO::PARAM_STR);

            $stmt->execute();

            return $return_array;
        } else {
            return $return_array;
        }
    }


    //implement delete() method
    public function delete($id)
    {
        $stmt = $this->db->prepare($this->delete_query);
        $stmt->bindValue(":id", $id, PDO::PARAM_INT);
        $stmt->execute();
        return true;
    }

    public function getForProblemId($id){
        $stmt = $this->db->prepare($this->problem_query);
        $stmt->bindValue(":problem_id", $id, PDO::PARAM_INT);
        $stmt->execute();

        $read_submissions = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $read_submissions;
    }

    public function getForSubmissionId($id){
        $stmt = $this->db->prepare($this->select_with_user_problem);
        $stmt->bindValue(":id", $id, PDO::PARAM_INT);
        $stmt->execute();

        $read_submissions = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $read_submissions;
    }

    public function getForGroupUserId($id, $getId, $getGroupId=-1){
        if($getGroupId!=-1) {
            $stmt = $this->db->prepare($this->user_id_group_query);

            $stmt->bindValue(":problem_id", $id, PDO::PARAM_INT);
            $stmt->bindValue(":user_id", $getId, PDO::PARAM_INT);
            $stmt->bindValue(":group_id", $getGroupId, PDO::PARAM_INT);

            $stmt->execute();
            $read_submissions = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $read_submissions;

        } else {
            $stmt = $this->db->prepare($this->user_id_problem_query);
            $stmt->bindValue(":problem_id", $id, PDO::PARAM_INT);
            $stmt->bindValue(":user_id", $getId, PDO::PARAM_INT);
            $stmt->execute();

            $read_submissions = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $read_submissions;
        }
    }

    public function getForUserId($id){
        $stmt = $this->db->prepare($this->user_id_query);
        $stmt->bindValue(":user_id", $id, PDO::PARAM_INT);
        $stmt->execute();

        $read_submissions = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $read_submissions;
    }
}