<?php
/**
 * Created by PhpStorm.
 * User: luba
 * Date: 12/26/15
 * Time: 5:24 PM
 */

namespace model\submissions;

class Submission
{
    private $id;
    private $user_id;
    private $problem_id;
    private $code;
    private $language;
    private $verdict;
    private $message;
    private $timestamp;
    private $report_path;

    /**
     * Submission constructor.
     * @param $id
     * @param $user_id
     * @param $problem_id
     * @param $code
     * @param $language
     * @param $verdict
     * @param $message
     * @param $timestamp
     */
    public function __construct($id="", $user_id="", $problem_id="", $code="", $language="", $verdict="", $message="", $timestamp="", $report_path="")
    {
        $this->id = $id;
        $this->user_id = $user_id;
        $this->problem_id = $problem_id;
        $this->code = $code;
        $this->language = $language;
        $this->verdict = $verdict;
        $this->message = $message;
        $this->timestamp = $timestamp;
        $this->report_path = $report_path;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param mixed $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * @return mixed
     */
    public function getProblemId()
    {
        return $this->problem_id;
    }

    /**
     * @param mixed $problem_id
     */
    public function setProblemId($problem_id)
    {
        $this->problem_id = $problem_id;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return mixed
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param mixed $language
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }

    /**
     * @return mixed
     */
    public function getVerdict()
    {
        return $this->verdict;
    }

    /**
     * @param mixed $verdict
     */
    public function setVerdict($verdict)
    {
        $this->verdict = $verdict;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * @param mixed $timestamp
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;
    }

    /**
     * @return string
     */
    public function getReportPath()
    {
        return $this->report_path;
    }

    /**
     * @param string $report_path
     */
    public function setReportPath($report_path)
    {
        $this->report_path = $report_path;
    }
}