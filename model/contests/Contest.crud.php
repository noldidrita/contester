<?php
/**
 * Created by PhpStorm.
 * User: luba
 * Date: 12/26/15
 * Time: 4:44 PM
 */

namespace model\contests;

include_once("../core/crud_interface.php");
include_once("../core/database.php");
include_once("../model/contests/Contest.class.php");
include_once("../model/contests/Validation.php");

use \core\Crud as Crud_interface;
use \core\Database;
use model\contests\Contest;
use \model\contests\Validation;

use \PDO;
class Crud implements Crud_interface
{
    private $insert_query = "INSERT INTO `contests`(`title`, `start_time`, `end_time`, `description`) VALUES (:title,:start_time,:end_time,:description)";
    private $select_query_all = "SELECT contests.id, contests.title, contests.description,contests.start_time,contests.end_time , count(problems.id) as nr_problems from contests LEFT JOIN problems ON contests.id = problems.contest_id group by contests.id";
    private $select_query = "SELECT contests.id, contests.title, contests.description,contests.start_time,contests.end_time, count(problems.id) as nr_problems from contests LEFT JOIN problems ON contests.id = problems.contest_id WHERE contests.id=:id group by contests.id";
    private $update_query = "UPDATE `contests` SET `title`= :title,`description`= :description,`start_time`= :start_time,`end_time`=:end_time WHERE `id`= :id";
    private $delete_query = "DELETE FROM `contests` WHERE `id`= :id";

    private $scoreboard_query = 'SELECT users.id as user_id, users.name, problems.id as problem_id, problems.title, count(submissions.id) as nr_submissions, submissions.id as submission_id,
    MAX(CAST(SUBSTRING_INDEX(submissions.message, "/", 1) AS UNSIGNED)) as points, max(CAST(RIGHT(submissions.message, LOCATE("/", submissions.message)-1)AS UNSIGNED)) as total,
    TIMESTAMPDIFF(MINUTE, contests.start_time, max(submissions.timestamp)) as time, max(submissions.timestamp) as timestamp
    FROM submissions, users, problems, contests WHERE submissions.timestamp > contests.start_time AND submissions.timestamp < contests.end_time
    AND users.id = submissions.user_id  AND problems.contest_id = contests.id AND submissions.problem_id = problems.id AND contests.id = :id group by user_id, problem_id';

    private $db;

    public function __construct()
    {
        $db_object = new Database();
        $this->db = $db_object->get_db();
    }

    //implement create method
    public function create($contest)
    {
        $return_array = array(
            "success" => true,
            "message" => ""
        );

        //Validate the problem
        $validation = new Validation();

        if (!$validation->isText($contest->getTitle())) {
            $return_array['success'] = false;
            $return_array['message'] .= $validation->get_text_criteria("title") . "\n";
        }
        if (!$validation->isText($contest->getDescription())) {
            $return_array['success'] = false;
            $return_array['message'] .= $validation->get_text_criteria("description") . "\n";
        }

        if (!$validation->isTime($contest->getStartTime())) {
            $return_array['success'] = false;
            $return_array['message'] .= $validation->get_timer_criteria("start_time") . "\n";
        }
        if (!$validation->isTime($contest->getEndTime())) {
            $return_array['success'] = false;
            $return_array['message'] .= $validation->get_timer_criteria("end_time") . "\n";
        }

        if ($return_array['success']) {

            //insert problem
            $stmt = $this->db->prepare($this->insert_query);
            $stmt->bindValue(":title", $validation->filter_html($contest->getTitle()), PDO::PARAM_STR);
            $stmt->bindValue(":description", $validation->filter_html($contest->getDescription()), PDO::PARAM_STR);
            $stmt->bindValue(":start_time", $contest->getStartTime(), PDO::PARAM_STR);
            $stmt->bindValue(":end_time", $contest->getEndTime(), PDO::PARAM_STR);

            if(!$stmt->execute()) {
                $return_array['success'] = false;
                $return_array['message'] = json_encode($stmt->errorInfo());
            }
            return $return_array;
        } else {
            return $return_array;
        }
    }

    //implement read  method
    public function read($id = -1)
    {
        if ($id == -1) {
            $stmt = $this->db->prepare($this->select_query_all);
            $stmt->execute();

            $read_contests = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $read_contests;
        } else {
            $stmt = $this->db->prepare($this->select_query);
            $stmt->bindValue(":id", $id, PDO::PARAM_INT);
            $stmt->execute();

            $read_contests = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $read_contests;
        }
    }

    //implement update method
    public function update($contest, $id)
    {
        $get_contest = $this->read($id);

        $update_contest = new contest();

        $validation = new Validation();

        $return_array = array(
            "success" => true,
            "message" => ""
        );

        //validate//title
        if (!empty($contest->getTitle()) or $contest->getTitle() !== "") {
            if ($validation->isText($contest->getTitle())) {
                $update_contest->setTitle($contest->getTitle());
            } else {
                $return_array['success'] = false;
                $return_array['message'] = $validation->get_text_criteria("title");
            }
        } else {
            $update_contest->setTitle($get_contest[0]['title']);
        }
        //validate//description
        if (!empty($contest->getDescription()) or $contest->getDescription() !== "") {
            if ($validation->isText($contest->getDescription())) {
                $update_contest->setDescription($contest->getDescription());
            } else {
                $return_array['success'] = false;
                $return_array['message'] = $validation->get_text_criteria("description");
            }
        } else {
            $update_contest->setDescription($get_contest[0]['description']);
        }

        //validate//start_time
        if (!empty($contest->getStartTime()) or $contest->getStartTime() !== "") {
            if ($validation->isTime($contest->getStartTime())) {
                $update_contest->setStartTime($contest->getStartTime());
            } else {
                $return_array['success'] = false;
                $return_array['message'] = $validation->get_timer_criteria("start_time");
            }
        } else {
            $update_contest->setStartTime($get_contest[0]['start_time']);
        }
        //validate//end_time
        if (!empty($contest->getEndTime()) or $contest->getEndTime() !== "") {
            if ($validation->isTime($contest->getEndTime())) {
                $update_contest->setEndTime($contest->getEndTime());
            } else {
                $return_array['success'] = false;
                $return_array['message'] = $validation->get_timer_criteria("end_time");
            }
        } else {
            $update_contest->setEndTime($get_contest[0]['end_time']);
        }

        $update_contest->setId($get_contest[0]['id']);

        //update
        if ($return_array['success']) {

            $stmt = $this->db->prepare($this->update_query);
            $stmt->bindValue(":id", $update_contest->getId(), PDO::PARAM_INT);
            $stmt->bindValue(":title", $update_contest->getTitle(), PDO::PARAM_STR);
            $stmt->bindValue(":description", $update_contest->getDescription(), PDO::PARAM_STR);
            $stmt->bindValue(":start_time", $update_contest->getStartTime(), PDO::PARAM_STR);
            $stmt->bindValue(":end_time", $update_contest->getEndTime(), PDO::PARAM_STR);

            $stmt->execute();

            return $return_array;
        }
        else {
            return $return_array;
        }
    }

    //implement read() method
    public function getScoreboardResults($id = -1)
    {
        $stmt = $this->db->prepare($this->scoreboard_query);
        $stmt->bindValue(":id", $id, PDO::PARAM_INT);
        $stmt->execute();

        $read_submissions = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $read_submissions;
    }

    public function delete($id)
    {
        $stmt = $this->db->prepare($this->delete_query);
        $stmt->bindValue(":id", $id, PDO::PARAM_INT);
        $stmt->execute();
        return true;
    }
}