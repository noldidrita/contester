<?php
/**
 * Created by PhpStorm.
 * User: luba
 * Date: 12/26/15
 * Time: 4:44 PM
 */

namespace model\contests;

class Contest
{
    private $id;
    private $title;
    private $start_time;
    private $end_time;
    private $description;

    /**
     * Contest constructor.
     * @param $id
     * @param $title
     * @param $start_time
     * @param $end_time
     * @param $description
     * @param $problems
     */
    public function __construct($id="", $title="", $start_time="", $end_time="", $description="")
    {
        $this->id = $id;
        $this->title = $title;
        $this->start_time = $start_time;
        $this->end_time = $end_time;
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getStartTime()
    {
        return $this->start_time;
    }

    /**
     * @param mixed $start_time
     */
    public function setStartTime($start_time)
    {
        $this->start_time = $start_time;
    }

    /**
     * @return mixed
     */
    public function getEndTime()
    {
        return $this->end_time;
    }

    /**
     * @param mixed $end_time
     */
    public function setEndTime($end_time)
    {
        $this->end_time = $end_time;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

}

