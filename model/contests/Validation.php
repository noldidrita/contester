<?php
/**
 * Created by PhpStorm.
 * User: luba
 * Date: 12/26/15
 * Time: 4:44 PM
 */

namespace model\contests;
use core\security;

include_once ('../core/security.php');

class Validation extends Security
{

    public $text = "/^[a-zA-Z0-9.*#_'`\+\-\(\)\[\] ]+$/";
    public $timer = "/^(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})$/";

    public function isText($text)//title,description
    {
        return preg_match($this->text, $text);
    }

    public function get_text_criteria($value)
    {
        $s = " $value must contain only capital letters or lowercase letters, numbers,round and square brackets,undescore,star,hashtag";
        return $s;
    }


    public function isTime($time)//start_time,end_time
    {
        return preg_match($this->timer, $time);
    }

    public function get_timer_criteria($element)
    {
        $s = " $element  must have the following format YYYY-MM-dd HH:mm:ss";
        return $s;
    }
}