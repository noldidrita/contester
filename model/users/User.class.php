<?php
/**
 * Created by PhpStorm.
 * User: luba
 * Date: 12/24/15
 * Time: 11:01 PM
 */

namespace model\users;


class User
{

    private $id;
    private $name;
    private $username;
    private $surname;
    private $email;
    private $birthdate;
    private $pictureUrl;
    private $gender;
    private $group_id;
    private $type;
    private $password;
    private $timestamp;
    private $attempts_cnt;
    private $accept_cnt;

    /**
     * User constructor.
     * @param $id
     * @param $name
     * @param $username
     * @param $surname
     * @param $email
     * @param $birthdate
     * @param $pictureUrl
     * @param $gender
     * @param $group_id
     * @param $type
     * @param $password
     * @param $timestamp
     * @param $attemp_cnt
     * @param $accept_cnt
     */
    public function __construct($id="", $name="", $username="", $surname="", $email="", $birthdate="", $pictureUrl="", $gender="", $group_id="", $type="", $password="", $timestamp="", $attempts_cnt="", $accept_cnt="")
    {
        $this->id = $id;
        $this->name = $name;
        $this->username = $username;
        $this->surname = $surname;
        $this->email = $email;
        $this->birthdate = $birthdate;
        $this->pictureUrl = $pictureUrl;
        $this->gender = $gender;
        $this->group_id = $group_id;
        $this->type = $type;
        $this->password = $password;
        $this->timestamp = $timestamp;
        $this->attempts_cnt = $attempts_cnt;
        $this->accept_cnt = $accept_cnt;
    }

    /**
     * Setup a new user object by reading the data from the array.
     * @param $user The row from the database for this user
     */
    public function buildFromArray($user)
    {
        $this->id = $user['id'];
        $this->name = $user['name'];
        $this->username = $user['username'];
        $this->surname = $user['surname'];
        $this->email = $user['email'];
        $this->birthdate = $user['birthdate'];
        $this->pictureUrl = $user['pictureUrl'];
        $this->gender = $user['gender'];
        $this->group_id = $user['group_id'];
        $this->type = $user['type'];
        $this->password = $user['password'];
        $this->timestamp = $user['timestamp'];
        $this->attempts_cnt = $user['attempts_cnt'];
        $this->accept_cnt = $user['accept_cnt'];
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param mixed $surname
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getBirthdate()
    {
        return $this->birthdate;
    }

    /**
     * @param mixed $birthdate
     */
    public function setBirthdate($birthdate)
    {
        $this->birthdate = $birthdate;
    }

    /**
     * @return mixed
     */
    public function getPictureUrl()
    {
        return $this->pictureUrl;
    }

    /**
     * @param mixed $pictureUrl
     */
    public function setPictureUrl($pictureUrl)
    {
        $this->pictureUrl = $pictureUrl;
    }

    /**
     * @return mixed
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param mixed $gender
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    /**
     * @return mixed
     */
    public function getGroupId()
    {
        return $this->group_id;
    }

    /**
     * @param mixed $group_id
     */
    public function setGroupId($group_id)
    {
        $this->group_id = $group_id;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * @param mixed $timestamp
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;
    }

    /**
     * @return mixed
     */
    public function getAttemptsCnt()
    {
        return $this->attempts_cnt;
    }

    /**
     * @param mixed $attempt_cnt
     */
    public function setAttemptsCnt($attempts_cnt)
    {
        $this->attempts_cnt = $attempts_cnt;
    }

    /**
     * @return mixed
     */
    public function getAcceptCnt()
    {
        return $this->accept_cnt;
    }

    /**
     * @param mixed $accept_cnt
     */
    public function setAcceptCnt($accept_cnt)
    {
        $this->accept_cnt = $accept_cnt;
    }



}