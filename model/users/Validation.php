<?php
/**
 * Created by PhpStorm.
 * User: luba
 * Date: 12/24/15
 * Time: 11:33 PM
 */

namespace model\users;
use core\security;
include_once('../core/security.php');
    /**
     * Validation for the user class
     */
class Validation extends Security
{
    public $username = "/^[a-z0-9_]+$/i";
    public $password = "/^\S*(?=\S{8,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])\S*$/";
    public $email = "/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/";
    public $name = "/^[A-Za-z]+$/";
    public $surname = "/^[A-Za-z]+$/";
    public $birthdate = "/^\d{4}\-\d{2}-\d{2}$/";
    public $number = "/^[0-9]+$/";

    public function username($user)
    {
        return preg_match($this->username, $user);
    }

    public function password($pass)
    {
        return preg_match($this->password, $pass);
    }

    public function get_username_criteria()
    {
        $s = "Username must contain only letters and numbers";
        return $s;
    }

    public function get_password_criteria()
    {
        $s = "Password must be more than 8 characters, ";
        $s .= "must contain at least one small case letter, ";
        $s .= "must contain at least one capital letter, ";
        $s .= "must contain at least one number.";

        return $s;
    }
    public function email($email)
    {
        return preg_match($this->email, $email);
    }
    public function name($name)
    {
        return preg_match($this->name, $name);
    }
    public function surname($surname)
    {
        return preg_match($this->surname, $surname);
    }
    public function gender($gender)
    {
        if($gender=="Male")
            return true;
        if($gender=="Female")
            return true;
        return false;
    }
    public function birthdate($birthdate)
    {
        return preg_match($this->birthdate, $birthdate);
    }
    public function get_email_criteria()
    {
        $s = "Email must contain only letters and numbers ";
        return $s;
    }
    public function get_name_criteria()
    {
        $s = "Name must contain only letters and start with capital letter.";
        return $s;
    }
    public function get_surname_criteria()
    {
        $s = "Surname must contain only letters and start with capital letter.";
        return $s;
    }
    public function get_gender_criteria()
    {
        $s = "Gender can only be male or female";
        return $s;
    }
    public function get_birthdate_criteria()
    {
        $s = "Birthdate must be in the format YYYY-MM-DD";
        return $s;
    }
    public function isNumber($number)//user_id,problem_id
    {
        return preg_match($this->number,$number);
    }
    public function get_number_criteria($element)
    {
        $s = " $element must contain only numbers";
        return $s;

    }
}