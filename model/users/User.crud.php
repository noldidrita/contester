<?php
/**
 * Created by PhpStorm.
 * User: luba
 * Date: 12/24/15
 * Time: 11:08 PM
 */

namespace model\users;
include_once("../core/crud_interface.php");
include_once("../core/database.php");
include_once("../model/users/User.class.php");
include_once("../model/users/Validation.php");

use \core\Crud as Crud_interface;
use \core\Database;
use \model\users\Validation;
use \model\users\User;
use \PDO;

class Crud implements Crud_interface
{
    private $insert_query = "INSERT INTO `users`(`username`, `name`, `surname`, `email`, `birthdate`, `pictureUrl`, `gender`, `group_id`, `type`, `password`, `timestamp`, `attempts_cnt`, `accept_cnt`) VALUES (:username,:name,:surname,:email,:birthdate,:pictureUrl,:gender,:group_id,:type,:password,:timestamp,:attempts_cnt,:accept_cnt)";
    private $select_query_all = "SELECT * FROM `users`";
    private $select_query = "SELECT * FROM `users` WHERE id=:id";
    private $update_query = "UPDATE `users` SET `username`=:username,`name`= :name,`surname`= :surname,`email`= :email,`birthdate`= :birthdate,`pictureUrl`=:pictureUrl,`gender`= :gender,`group_id`= :group_id,`type`= :type,`password`= :password,`timestamp`= :timestamp,`attempts_cnt`= :attempts_cnt,`accept_cnt`= :accept_cnt WHERE `id`= :id";
    private $delete_query = "DELETE FROM `users` WHERE `id`=:id";

    private $login_query = "SELECT * FROM `users` WHERE `username`=:username AND `password`=:password";
    private $username_query = "SELECT * FROM `users` WHERE `username`=:username";
    private $email_query = "SELECT * FROM `users` WHERE `email`=:email";
    private $group_query = "SELECT * FROM `users` WHERE `group_id`=:id";
    private $inc_attempts_query = "UPDATE `users` SET `attempts_cnt`= `attempts_cnt` + 1 WHERE `id` = :id";
    private $inc_accepted_query = "UPDATE `users` SET `accept_cnt`= `accept_cnt` + 1 WHERE `id` = :id";

    private $db;

    public function __construct()
    {
        $db_object = new Database();
        $this->db = $db_object->get_db();
    }

    //create
    public function create($user)
    {
        $return_array = array(
            "success" => true,
            "message" => ""
        );

        //Validate the user
        $validation = new Validation();

        if (!$validation->username($user->getUsername())) {
            $return_array['success'] = false;
            $return_array['message'] .= $validation->get_username_criteria() . "\n";
        }
        if(!empty($this->getByUsername($user->getUsername()))) {
            $return_array['success'] = false;
            $return_array['message'] .= "Username already exists. \n";
        }
        if (!$validation->password($user->getPassword())) {
            $return_array['success'] = false;
            $return_array['message'] .= $validation->get_password_criteria() . "\n";
        }
        if (!$validation->email($user->getEmail())) {
            $return_array['success'] = false;
            $return_array['message'] .= $validation->get_email_criteria() . "\n";
        }
        if(!empty($this->getByEmail($user->getEmail()))) {
            $return_array['success'] = false;
            $return_array['message'] .= "Email already exists. \n";
        }
        if (!$validation->name($user->getName())) {
            $return_array['success'] = false;
            $return_array['message'] .= $validation->get_name_criteria() . "\n";
        }
        if (!$validation->surname($user->getSurname())) {
            $return_array['success'] = false;
            $return_array['message'] .= $validation->get_surname_criteria() . "\n";
        }
        if (!$validation->gender($user->getGender())) {
            $return_array['success'] = false;
            $return_array['message'] .= $validation->get_gender_criteria() . "\n";
        }
        if (!$validation->birthdate($user->getBirthdate())) {
            $return_array['success'] = false;
            $return_array['message'] .= $validation->get_birthdate_criteria() . "\n";
        }
        if (!$validation->isNumber($user->getGroupId())) {
            $return_array['success'] = false;
            $return_array['message'] .= $validation->get_number_criteria("group id");
        }
        if ($return_array['success']) {
            //encrypt password
            $user->setPassword(sha1($user->getPassword()));

            //insert user
            $stmt = $this->db->prepare($this->insert_query);
            $stmt->bindValue(":username", $user->getUsername(), PDO::PARAM_STR);
            $stmt->bindValue(":password", $user->getPassword(), PDO::PARAM_STR);
            $stmt->bindValue(":name", $user->getName(), PDO::PARAM_INT);
            $stmt->bindValue(":surname", $user->getSurname(), PDO::PARAM_STR);
            $stmt->bindValue(":email", $user->getEmail(), PDO::PARAM_STR);
            $stmt->bindValue(":gender", $user->getGender(), PDO::PARAM_STR);
            $stmt->bindValue(":birthdate", $user->getBirthdate(), PDO::PARAM_STR);
            $stmt->bindValue(":group_id", $user->getGroupId(), PDO::PARAM_INT);
            $stmt->bindValue(":type", $user->getType(), PDO::PARAM_STR);
            $stmt->bindValue(":pictureUrl", $validation->filter_html($user->getPictureUrl()), PDO::PARAM_STR);
            $stmt->bindValue(":attempts_cnt", $user->getAttemptsCnt(), PDO::PARAM_INT);
            $stmt->bindValue(":accept_cnt", $user->getAcceptCnt(), PDO::PARAM_INT);
            $stmt->bindValue(":timestamp", $user->getTimestamp(), PDO::PARAM_STR);

            if(!$stmt->execute()) {
                $return_array['success'] = false;
                $return_array['message'] = json_encode($stmt->errorInfo());
            }
            return $return_array;
        } else {
            return $return_array;
        }
    }

    //read
    public function read($id = -1)
    {
        if ($id == -1) {
            $stmt = $this->db->prepare($this->select_query_all);
            $stmt->execute();

            $read_users = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $read_users;
        } else {
            $stmt = $this->db->prepare($this->select_query);
            $stmt->bindValue(":id", $id, PDO::PARAM_INT);
            $stmt->execute();

            $read_users = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $read_users;
        }
    }

    //update
    public function update($user, $id)
    {
        $get_user = $this->read($id);

        $update_user = new User();

        $validation = new Validation();

        $return_array = array(
            "success" => true,
            "message" => ""
        );

        //validate//username
        if (!empty($user->getUsername()) or $user->getUsername() !== "") {
            if (!$validation->username($user->getUsername())) {
                $return_array['success'] = false;
                $return_array['message'] = $validation->get_username_criteria();
            } else if(!empty($this->getByUsername($user->getUsername())) && $user->getUsername()!=$get_user[0]['username']) {
                $return_array['success'] = false;
                $return_array['message'] .= "Username already exists. \n";
            } else {
                $update_user->setUsername($user->getUsername());
            }
        } else {
            $update_user->setUsername($get_user[0]['username']);
        }
        //validate//password
        if (!empty($user->getPassword()) or $user->getPassword() !== "") {
            if ($validation->password($user->getPassword())) {
                $update_user->setPassword(sha1($user->getPassword()));
            } else {
                $return_array['success'] = false;
                $return_array['message'] = $validation->get_password_criteria();
            }
        } else {
            $update_user->setPassword($get_user[0]['password']);
        }

        //validate//name
        if (!empty($user->getName()) or $user->getName() !== "") {
            if ($validation->name($user->getName())) {
                $update_user->setName($user->getName());
            } else {
                $return_array['success'] = false;
                $return_array['message'] = $validation->get_name_criteria();
            }
        } else {
            $update_user->setName($get_user[0]['name']);
        }

        //validate//email
        if (!empty($user->getEmail()) or $user->getEmail() !== "") {
            if (!$validation->email($user->getEmail())) {
                $return_array['success'] = false;
                $return_array['message'] = $validation->get_email_criteria();
            }
            else if(!empty($this->getByEmail($user->getEmail()))&& $user->getEmail()!=$get_user[0]['email']) {
                $return_array['success'] = false;
                $return_array['message'] .= "Email already exists. \n";
            }
            else {
                $update_user->setEmail($user->getEmail());
            }
        } else {
            $update_user->setEmail($get_user[0]['email']);
        }
        //validate//surname
        if (!empty($user->getSurname()) or $user->getSurname() !== "") {
            if ($validation->surname($user->getSurname())) {
                $update_user->setSurname($user->getSurname());
            } else {
                $return_array['success'] = false;
                $return_array['message'] = $validation->get_surname_criteria();
            }
        } else {
            $update_user->setSurname($get_user[0]['surname']);
        }
        //validate//gender
        if (!empty($user->getGender()) or $user->getGender() !== "") {
            if ($validation->gender($user->getGender())) {
                $update_user->setGender($user->getGender());
            } else {
                $return_array['success'] = false;
                $return_array['message'] = $validation->get_gender_criteria();
            }
        } else {
            $update_user->setGender($get_user[0]['gender']);
        }
        //validate//birthdate
        if (!empty($user->getBirthdate()) or $user->getBirthdate() !== "") {
            if ($validation->birthdate($user->getBirthdate())) {
                $update_user->setBirthdate($user->getBirthdate());
            } else {
                $return_array['success'] = false;
                $return_array['message'] = $validation->get_birthdate_criteria();
            }
        } else {
            $update_user->setBirthdate($get_user[0]['birthdate']);
        }
        //validate//type
        if (!empty($user->getType()) or $user->getType() !== "") {
            if ($user->getType() >= 0 and $user->getType() < 5) {
                $update_user->setType($user->getType());
            } else {
                $return_array['success'] = false;
                $return_array['message'] = "type must be btw 0 and 4";
            }
        } else {
            $update_user->setType($get_user[0]['type']);
        }

        //validate//group_id
        if (!empty($user->getGroupId()) or $user->getGroupId() !== "") {
            if ($validation->isNumber($user->getGroupId())) {
                $update_user->setGroupId($user->getGroupId());
            } else {
                $return_array['success'] = false;
                $return_array['message'] = $validation->get_number_criteria("group id");
            }
        } else {
            $update_user->setGroupId($get_user[0]['group_id']);
        }

        //validate//pictureUrl
        if (!empty($user->getPictureUrl()) or $user->getPictureUrl() !== "") {
            $update_user->setPictureUrl($user->getPictureUrl());
        } else {
            $update_user->setPictureUrl($get_user[0]['pictureUrl']);
        }

        if (!empty($user->getAcceptCnt()) or $user->getAcceptCnt() !== "") {
            $update_user->setAcceptCnt($user->getAcceptCnt());
        } else {
            $update_user->setAcceptCnt($get_user[0]['accept_cnt']);
        }

        if (!empty($user->getAttemptsCnt()) or $user->getAttemptsCnt() !== "") {
            $update_user->setAttemptsCnt($user->getAttemptsCnt());
        } else {
            $update_user->setAttemptsCnt($get_user[0]['attempts_cnt']);
        }

        $update_user->setTimestamp($get_user[0]['timestamp']);
        $update_user->setId($get_user[0]['id']);

        //update
        if ($return_array['success']) {
            $stmt = $this->db->prepare($this->update_query);
            $stmt->bindValue(":id", $update_user->getId(), PDO::PARAM_INT);
            $stmt->bindValue(":username", $update_user->getUsername(), PDO::PARAM_STR);
            $stmt->bindValue(":password", $update_user->getPassword(), PDO::PARAM_STR);
            $stmt->bindValue(":name", $update_user->getName(), PDO::PARAM_INT);
            $stmt->bindValue(":surname", $update_user->getSurname(), PDO::PARAM_STR);
            $stmt->bindValue(":email", $update_user->getEmail(), PDO::PARAM_STR);
            $stmt->bindValue(":gender", $update_user->getGender(), PDO::PARAM_STR);
            $stmt->bindValue(":birthdate", $update_user->getBirthdate(), PDO::PARAM_STR);
            $stmt->bindValue(":group_id", $update_user->getGroupId(), PDO::PARAM_INT);
            $stmt->bindValue(":type", $update_user->getType(), PDO::PARAM_INT);
            $stmt->bindValue(":pictureUrl", $validation->filter_html($update_user->getPictureUrl()), PDO::PARAM_STR);
            $stmt->bindValue(":attempts_cnt", $update_user->getAttemptsCnt(), PDO::PARAM_INT);
            $stmt->bindValue(":accept_cnt", $update_user->getAcceptCnt(), PDO::PARAM_INT);
            $stmt->bindValue(":timestamp", $update_user->getTimestamp(), PDO::PARAM_STR);
            $stmt->execute();

            return $return_array;
        } else {
            return $return_array;
        }
    }

    //delete
    public function delete($id)
    {
        $stmt = $this->db->prepare($this->delete_query);
        $stmt->bindValue(":id", $id, PDO::PARAM_INT);
        $stmt->execute();
        return true;
    }


    //check login
    public function checkLogin($username, $password)
    {
        $stmt = $this->db->prepare($this->login_query);
        $stmt->bindValue(":username", $username, PDO::PARAM_STR);
        $stmt->bindValue(":password", sha1($password), PDO::PARAM_STR);

        $stmt->execute();

        $read_users = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $read_users;
    }

    public function getByUsername($username)
    {
        $stmt = $this->db->prepare($this->username_query);
        $stmt->bindValue(":username", $username, PDO::PARAM_STR);

        $stmt->execute();

        $read_users = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $read_users;
    }

    public function getByEmail($email)
    {
        $stmt = $this->db->prepare($this->email_query);
        $stmt->bindValue(":email", $email, PDO::PARAM_STR);

        $stmt->execute();

        $read_users = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $read_users;
    }

    public function incrementAttempts($id)
    {
        $stmt = $this->db->prepare($this->inc_attempts_query);
        $stmt->bindValue(":id", $id, PDO::PARAM_INT);

        if($stmt->execute())
            return true;
        else return false;
    }
    public function incrementAccepted($id)
    {
        $stmt = $this->db->prepare($this->inc_accepted_query);
        $stmt->bindValue(":id", $id, PDO::PARAM_INT);

        if($stmt->execute())
            return true;
        else return false;
    }

    public function getByGroup($id) {
        $stmt = $this->db->prepare($this->group_query);
        $stmt->bindValue(":id", $id, PDO::PARAM_STR);

        $stmt->execute();

        $read_users = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $read_users;
    }
}
