<?php
/**
 * Created by PhpStorm.
 * User: luba
 * Date: 12/25/15
 * Time: 8:45 PM
 */
namespace model\volumes;

include_once("../core/crud_interface.php");
include_once("../core/database.php");
include_once("../model/volumes/Volume.class.php");
include_once("../model/volumes/Validation.php");

use \core\Crud as Crud_interface;
use \core\Database;
use \model\volumes\Validation;

use \PDO;

class Crud implements Crud_interface
{
    private $insert_query = "INSERT INTO `volumes`(`name`, `description`, `timestamp`) VALUES (:name,:description,:timestamp)";
    private $select_query_all = "SELECT volumes.id, volumes.name, volumes.description, count(problems.id) as nr_problems from volumes LEFT JOIN problems ON volumes.id = problems.volume_id  group by volumes.id";
    private $select_query = "SELECT volumes.id, volumes.name, volumes.description, count(problems.id) as nr_problems from volumes LEFT JOIN problems ON volumes.id = problems.volume_id WHERE volumes.id =:id group by volumes.id";
    private $update_query = "UPDATE `volumes` SET `name`= :name,`description`= :description,`timestamp`= :timestamp WHERE id= :id";
    private $delete_query = "DELETE FROM `volumes` WHERE `id`= :id";

    private $db;

    public function __construct()
    {
        $db_object = new Database();
        $this->db = $db_object->get_db();
    }

    //Implement create() method
    public function create($volume)
    {
        $return_array = array(
            "success" => true,
            "message" => ""
        );

        //Validate the problem
        $validation = new Validation();

        if (!$validation->isText($volume->getName())) {
            $return_array['success'] = false;
            $return_array['message'] .= $validation->get_text_criteria("name") . "\n";
        }
        if (!$validation->isText($volume->getDescription())) {
            $return_array['success'] = false;
            $return_array['message'] .= $validation->get_text_criteria("description") . "\n";
        }

        if ($return_array['success']) {

            //insert problem
            $stmt = $this->db->prepare($this->insert_query);
            $stmt->bindValue(":name", $validation->filter_html($volume->getName()), PDO::PARAM_STR);
            $stmt->bindValue(":description", $validation->filter_html($volume->getDescription()), PDO::PARAM_STR);
            $stmt->bindValue(":timestamp", $volume->getTimestamp(), PDO::PARAM_STR);
            $stmt->execute();

            return $return_array;
        } else {
            return $return_array;
        }
    }


    // Implement read() method.
    public
    function read($id = -1)
    {
        if ($id == -1) {
            $stmt = $this->db->prepare($this->select_query_all);
            $stmt->execute();

            $read_volumes = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $read_volumes;
        } else {
            $stmt = $this->db->prepare($this->select_query);
            $stmt->bindValue(":id", $id, PDO::PARAM_INT);
            $stmt->execute();

            $read_volumes = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $read_volumes;
        }
    }

    // Implement update() method.
    public function update($volume, $id)
    {
        $get_volume = $this->read($id);

        $update_volume = new Volume();

        $validation = new Validation();

        $return_array = array(
            "success" => true,
            "message" => ""
        );

        //validate//name
        if (!empty($volume->getName()) or $volume->getName() !== "") {
            if ($validation->isText($volume->getName())) {
                $update_volume->setName($volume->getName());
            } else {
                $return_array['success'] = false;
                $return_array['message'] = $validation->get_text_criteria("name");
            }
        } else {
            $update_volume->setName($get_volume[0]['name']);
        }
        //validate//description
        if (!empty($volume->getDescription()) or $volume->getDescription() !== "") {
            if ($validation->isText($volume->getDescription())) {
                $update_volume->setDescription($volume->getDescription());
            } else {
                $return_array['success'] = false;
                $return_array['message'] = $validation->get_text_criteria("description");
            }
        } else {
            $update_volume->setDescription($get_volume[0]['description']);
        }

        $update_volume->setTimestamp($get_volume[0]['timestamp']);
        $update_volume->setId($get_volume[0]['id']);

        //update
        if ($return_array['success']) {

            $stmt = $this->db->prepare($this->update_query);
            $stmt->bindValue(":id", $update_volume->getId(), PDO::PARAM_INT);
            $stmt->bindValue(":name", $validation->filter_html($update_volume->getName()), PDO::PARAM_STR);
            $stmt->bindValue(":description", $validation->filter_html($update_volume->getDescription()), PDO::PARAM_STR);
            $stmt->bindValue(":timestamp", $update_volume->getTimestamp(), PDO::PARAM_STR);
            $stmt->execute();

            return $return_array;
        }
        else {
            return $return_array;
        }
    }

    // Implement delete() method.
    public function delete($id)
    {
        $stmt = $this->db->prepare($this->delete_query);
        $stmt->bindValue(":id", $id, PDO::PARAM_INT);
        $stmt->execute();
        return true;
    }

    public function read_withproblems($id = -1)
    {
        if ($id == -1) {
            $stmt = $this->db->prepare($this->select_query_all);
            $stmt->execute();

            $read_volumes = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $read_volumes;
        } else {
            $stmt = $this->db->prepare($this->select_query);
            $stmt->bindValue(":id", $id, PDO::PARAM_INT);
            $stmt->execute();

            $read_volumes = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $read_volumes;
        }
    }


}