<?php
/**
 * Created by PhpStorm.
 * User: luba
 * Date: 12/25/15
 * Time: 8:45 PM
 */

namespace model\volumes;
use core\security;

include_once ('../core/security.php');

class Validation extends Security
{
    public $text = "/^[a-zA-Z0-9_*+'`,.\-\(\)\[\] ]+$/";

    public function isText ($text)//name,description
    {
        return preg_match($this->text, $text);
    }

    public function get_text_criteria($value)
    {
        $s = " $value must contain only  capital letters or lowercas letters, numbers,round and square brackets,underscore,comma,dot,plus and multiply,";
        return $s;
    }
}