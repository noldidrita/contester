<?php
/**
 * Created by PhpStorm.
 * User: luba
 * Date: 12/26/15
 * Time: 9:22 PM
 */

namespace model\problem_cases;

class ProblemCase
{
    private $id;
    private $problem_id;
    private $test_nr;
    private $input;
    private $pattern;
    private $points;

    /**
     * ProblemCase constructor.
     * @param $id
     * @param $problem_id
     * @param $test_nr
     * @param $input
     * @param $pattern
     */
    public function __construct($id="", $problem_id="", $test_nr="", $input="", $pattern="", $points=1)
    {
        $this->id = $id;
        $this->problem_id = $problem_id;
        $this->test_nr = $test_nr;
        $this->input = $input;
        $this->pattern = $pattern;
        $this->points = $points;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getPoints()
    {
        return $this->points;
    }

    /**
     * @param mixed $points
     */
    public function setPoints($points)
    {
        $this->points = $points;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getProblemId()
    {
        return $this->problem_id;
    }

    /**
     * @param mixed $problem_id
     */
    public function setProblemId($problem_id)
    {
        $this->problem_id = $problem_id;
    }

    /**
     * @return mixed
     */
    public function getTestNr()
    {
        return $this->test_nr;
    }

    /**
     * @param mixed $test_nr
     */
    public function setTestNr($test_nr)
    {
        $this->test_nr = $test_nr;
    }

    /**
     * @return mixed
     */
    public function getInput()
    {
        return $this->input;
    }

    /**
     * @param mixed $input
     */
    public function setInput($input)
    {
        $this->input = $input;
    }

    /**
     * @return mixed
     */
    public function getPattern()
    {
        return $this->pattern;
    }

    /**
     * @param mixed $pattern
     */
    public function setPattern($pattern)
    {
        $this->pattern = $pattern;
    }



}