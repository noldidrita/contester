<?php
/**
 * Created by PhpStorm.
 * User: luba
 * Date: 12/26/15
 * Time: 9:22 PM
 */

namespace model\problem_cases;

include_once("../core/crud_interface.php");
include_once("../core/database.php");
include_once("../model/problem_cases/ProblemCase.class.php");
include_once("../model/problem_cases/Validation.php");

use \core\Crud as Crud_interface;
use \core\Database;
use \model\problem_cases\Validation;

use \PDO;

class Crud implements Crud_interface
{
    private $insert_query = "INSERT INTO `problem_cases`(`problem_id`, `test_nr`, `input`, `pattern`, `points`) VALUES (:problem_id,:test_nr,:input,:pattern, :points)";
    private $select_query_all = "SELECT * FROM `problem_cases`";
    private $select_query = "SELECT * FROM `problem_cases` WHERE id=:id";
    private $select_query_problem_id = "SELECT * FROM `problem_cases` WHERE problem_id=:id ORDER BY test_nr ASC";
    private $update_query = "UPDATE `problem_cases` SET `user_id`= :user_id,`problem_id`= :problem_id,`code`= :code,`language`= :language,`verdict`= :verdict,`message`= :messsage,`timestamp`= :timestamp, `points`= :points WHERE `id`= :id";
    private $delete_query = "DELETE FROM `problem_cases` WHERE id=:id";

    private $db;

    public function __construct()
    {
        $db_object = new Database();
        $this->db = $db_object->get_db();
    }

    // Implement create() method.
    public function create($problem_case)
    {
        $return_array = array(
            "success" => true,
            "message" => ""
        );

        //Validate the problem_case
        $validation = new Validation();

        if (!$validation->isNumber($problem_case->getTestNr())) {
            $return_array['success'] = false;
            $return_array['message'] .= $validation->get_number_criteria("test_nr") . "\n";
        }

        if (!$validation->isNumber($problem_case->getProblemId())) {
            $return_array['success'] = false;
            $return_array['message'] .= $validation->get_number_criteria("problem_id") . "\n";
        }
        if ($problem_case->getPoints()=="") {
            $problem_case->setPoints(1);
        }
        if (!$validation->isNumber($problem_case->getPoints())) {
            $return_array['success'] = false;
            $return_array['message'] .= $validation->get_number_criteria("points") . "\n";
        }

        //
        if ($return_array['success']) {

            //insert problem_case
            $stmt = $this->db->prepare($this->insert_query);
            $stmt->bindValue(":test_nr", $problem_case->getTestNr(), PDO::PARAM_INT);
            $stmt->bindValue(":problem_id", $problem_case->getProblemId(), PDO::PARAM_INT);
            $stmt->bindValue(":input", $problem_case->getInput(), PDO::PARAM_STR);
            $stmt->bindValue(":pattern", $problem_case->getPattern(), PDO::PARAM_STR);
            $stmt->bindValue(":points", $problem_case->getPoints(), PDO::PARAM_INT);

            $stmt->execute();

            return $return_array;
        } else {
            return $return_array;
        }
    }

    public function readForProblemId($id)
    {
        $stmt = $this->db->prepare($this->select_query_problem_id);
        $stmt->bindValue(":id", $id, PDO::PARAM_INT);
        $stmt->execute();

        $read_problem_cases = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $read_problem_cases;

    }

    // Implement read() method.
    public function read($id = -1)
    {
        if ($id == -1) {
            $stmt = $this->db->prepare($this->select_query_all);
            $stmt->execute();

            $read_problem_cases = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $read_problem_cases;
        } else {
            $stmt = $this->db->prepare($this->select_query);
            $stmt->bindValue(":id", $id, PDO::PARAM_INT);
            $stmt->execute();

            $read_problem_cases = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $read_problem_cases;
        }
    }

    //  Implement update() method.
    public function update($problem_case, $id)
    {
        $get_problem_case = $this->read($id);

        $update_problem_case = new ProblemCase();

        $validation = new Validation();

        $return_array = array(
            "success" => true,
            "message" => ""
        );

        //validate//test_nr
        if (!empty($problem_case->getTestNr()) or $problem_case->getTestNr() !== "") {
            if ($validation->isNumber($problem_case->getTestNr())) {
                $update_problem_case->setTestNr($problem_case->getTestNr());
            } else {
                $return_array['success'] = false;
                $return_array['message'] = $validation->get_number_criteria("test_nr");
            }
        } else {
            $update_problem_case->setTestNr($get_problem_case[0]['test_nr']);
        }
        //validate//problem_id
        if (!empty($problem_case->getProblemId()) or $problem_case->getProblemId() !== "") {
            if ($validation->isNumber($problem_case->getProblemId())) {
                $update_problem_case->setProblemId($problem_case->getProblemId());
            } else {
                $return_array['success'] = false;
                $return_array['message'] = $validation->get_number_criteria("problem_id");
            }
        } else {
            $update_problem_case->setProblemId($get_problem_case[0]['problem_id']);
        }

        if (!empty($problem_case->getInput()) or $problem_case->getInput() !== "") {
            $update_problem_case->setInput($problem_case->getInput());
        } else {
            $update_problem_case->setInput($get_problem_case[0]['input']);
        }

        if (!empty($problem_case->getPattern()) or $problem_case->getPattern() !== "") {
            $update_problem_case->setPattern($problem_case->getPattern());
        } else {
            $update_problem_case->setPattern($get_problem_case[0]['pattern']);
        }
        if (!empty($problem_case->getPoints()) or $problem_case->getPoints() !== "") {
            if ($validation->isNumber($problem_case->getPoints())) {
                $update_problem_case->setPoints($problem_case->getPoints());
            } else {
                $return_array['success'] = false;
                $return_array['message'] = $validation->get_number_criteria("points");
            }
        } else {
            $update_problem_case->setPoints($get_problem_case[0]['points']);
        }

        $update_problem_case->setTimestamp($get_problem_case[0]['timestamp']);
        $update_problem_case->setId($get_problem_case[0]['id']);

        //update
        if ($return_array['success']) {

            $stmt = $this->db->prepare($this->update_query);
            $stmt->bindValue(":id", $update_problem_case->getId(), PDO::PARAM_INT);
            $stmt->bindValue(":test_nr", $update_problem_case->getTestNr(), PDO::PARAM_INT);
            $stmt->bindValue(":problem_id", $update_problem_case->getProblemId(), PDO::PARAM_INT);
            $stmt->bindValue(":input", $update_problem_case->getInput(), PDO::PARAM_STR);
            $stmt->bindValue(":pattern", $update_problem_case->getPattern(), PDO::PARAM_STR);
            $stmt->bindValue(":points", $problem_case->getPoints(), PDO::PARAM_INT);

            $stmt->execute();

            return $return_array;
        } else {
            return $return_array;
        }
    }

    //  Implement delete() method.
    public function delete($id)
    {
        $stmt = $this->db->prepare($this->delete_query);
        $stmt->bindValue(":id", $id, PDO::PARAM_INT);
        $stmt->execute();
        return true;
    }
}
