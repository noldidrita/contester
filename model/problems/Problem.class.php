<?php
/**
 * Created by PhpStorm.
 * User: luba
 * Date: 12/25/15
 * Time: 2:31 PM
 */

 namespace model\problems;

 class Problem
 {
     private $id;
     private $title;
     private $subtitle;
     private $statement;
     private $time_limit_1;
     private $time_limit_2;
     private $time_limit_3;
     private $volume_id;
     private $section_id;
     private $memory_limit_1;
     private $memory_limit_2;
     private $memory_limit_3;
     private $difficulty;
     private $shortdescription;
     private $timestamp;
     private $judge_code;
     private $judge_language;
     private $contest_id;
     private $homework_id;
     private $ready;

     /**
      * Problem constructor.
      * @param $id
      * @param $title
      * @param $subtitle
      * @param $statement
      * @param $time_limit_1
      * @param $time_limit_2
      * @param $time_limit_3
      * @param $volume_id
      * @param $section_id
      * @param $memory_limit_1
      * @param $memory_limit_2
      * @param $memory_limit_3
      * @param $difficulty
      * @param $shortdescription
      * @param $timestamp
      * @param $judge_code
      * @param $judge_language
      * @param $contest_id
      * @param $homework_id
      */
     public function __construct($id="", $title="", $subtitle="", $statement="", $time_limit_1="", $time_limit_2="", $time_limit_3="", $volume_id="", $section_id="", $memory_limit_1="", $memory_limit_2="", $memory_limit_3="", $difficulty="", $shortdescription="", $timestamp="", $judge_code="", $judge_language="", $contest_id="", $homework_id="", $ready="")
     {
         $this->id = $id;
         $this->title = $title;
         $this->subtitle = $subtitle;
         $this->statement = $statement;
         $this->time_limit_1 = $time_limit_1;
         $this->time_limit_2 = $time_limit_2;
         $this->time_limit_3 = $time_limit_3;
         $this->volume_id = $volume_id;
         $this->section_id = $section_id;
         $this->memory_limit_1 = $memory_limit_1;
         $this->memory_limit_2 = $memory_limit_2;
         $this->memory_limit_3 = $memory_limit_3;
         $this->difficulty = $difficulty;
         $this->shortdescription = $shortdescription;
         $this->timestamp = $timestamp;
         $this->judge_code = $judge_code;
         $this->judge_language = $judge_language;
         $this->contest_id = $contest_id;
         $this->homework_id = $homework_id;
         $this->ready = $ready;

     }

     /**
      * @return string
      */
     public function getId()
     {
         return $this->id;
     }

     /**
      * @param string $id
      */
     public function setId($id)
     {
         $this->id = $id;
     }

     /**
      * @return string
      */
     public function getTitle()
     {
         return $this->title;
     }

     /**
      * @param string $title
      */
     public function setTitle($title)
     {
         $this->title = $title;
     }

     /**
      * @return string
      */
     public function getSubtitle()
     {
         return $this->subtitle;
     }

     /**
      * @param string $subtitle
      */
     public function setSubtitle($subtitle)
     {
         $this->subtitle = $subtitle;
     }

     /**
      * @return string
      */
     public function getStatement()
     {
         return $this->statement;
     }

     /**
      * @param string $statement
      */
     public function setStatement($statement)
     {
         $this->statement = $statement;
     }

     /**
      * @return string
      */
     public function getTimeLimit1()
     {
         return $this->time_limit_1;
     }

     /**
      * @param string $time_limit_1
      */
     public function setTimeLimit1($time_limit_1)
     {
         $this->time_limit_1 = $time_limit_1;
     }

     /**
      * @return string
      */
     public function getTimeLimit2()
     {
         return $this->time_limit_2;
     }

     /**
      * @param string $time_limit_2
      */
     public function setTimeLimit2($time_limit_2)
     {
         $this->time_limit_2 = $time_limit_2;
     }

     /**
      * @return string
      */
     public function getTimeLimit3()
     {
         return $this->time_limit_3;
     }

     /**
      * @param string $time_limit_3
      */
     public function setTimeLimit3($time_limit_3)
     {
         $this->time_limit_3 = $time_limit_3;
     }

     /**
      * @return string
      */
     public function getVolumeId()
     {
         return $this->volume_id;
     }

     /**
      * @param string $volume_id
      */
     public function setVolumeId($volume_id)
     {
         $this->volume_id = $volume_id;
     }

     /**
      * @return string
      */
     public function getSectionId()
     {
         return $this->section_id;
     }

     /**
      * @param string $section_id
      */
     public function setSectionId($section_id)
     {
         $this->section_id = $section_id;
     }

     /**
      * @return string
      */
     public function getMemoryLimit1()
     {
         return $this->memory_limit_1;
     }

     /**
      * @param string $memory_limit_1
      */
     public function setMemoryLimit1($memory_limit_1)
     {
         $this->memory_limit_1 = $memory_limit_1;
     }

     /**
      * @return string
      */
     public function getMemoryLimit2()
     {
         return $this->memory_limit_2;
     }

     /**
      * @param string $memory_limit_2
      */
     public function setMemoryLimit2($memory_limit_2)
     {
         $this->memory_limit_2 = $memory_limit_2;
     }

     /**
      * @return string
      */
     public function getMemoryLimit3()
     {
         return $this->memory_limit_3;
     }

     /**
      * @param string $memory_limit_3
      */
     public function setMemoryLimit3($memory_limit_3)
     {
         $this->memory_limit_3 = $memory_limit_3;
     }

     /**
      * @return string
      */
     public function getDifficulty()
     {
         return $this->difficulty;
     }

     /**
      * @param string $difficulty
      */
     public function setDifficulty($difficulty)
     {
         $this->difficulty = $difficulty;
     }

     /**
      * @return string
      */
     public function getShortdescription()
     {
         return $this->shortdescription;
     }

     /**
      * @param string $shortdescription
      */
     public function setShortdescription($shortdescription)
     {
         $this->shortdescription = $shortdescription;
     }

     /**
      * @return string
      */
     public function getTimestamp()
     {
         return $this->timestamp;
     }

     /**
      * @param string $timestamp
      */
     public function setTimestamp($timestamp)
     {
         $this->timestamp = $timestamp;
     }

     /**
      * @return string
      */
     public function getJudgeCode()
     {
         return $this->judge_code;
     }

     /**
      * @param string $judge_code
      */
     public function setJudgeCode($judge_code)
     {
         $this->judge_code = $judge_code;
     }

     /**
      * @return string
      */
     public function getJudgeLanguage()
     {
         return $this->judge_language;
     }

     /**
      * @param string $judge_language
      */
     public function setJudgeLanguage($judge_language)
     {
         $this->judge_language = $judge_language;
     }

     /**
      * @return string
      */
     public function getContestId()
     {
         return $this->contest_id;
     }

     /**
      * @param string $contest_id
      */
     public function setContestId($contest_id)
     {
         $this->contest_id = $contest_id;
     }

     /**
      * @return string
      */
     public function getHomeworkId()
     {
         return $this->homework_id;
     }

     /**
      * @param string $homework_id
      */
     public function setHomeworkId($homework_id)
     {
         $this->homework_id = $homework_id;
     }

     /**
      * @return string
      */
     public function getReady()
     {
         return $this->ready;
     }

     /**
      * @param string $ready
      */
     public function setReady($ready)
     {
         $this->ready = $ready;
     }

 }