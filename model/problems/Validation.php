<?php
/**
 * Created by PhpStorm.
 * User: luba
 * Date: 12/25/15
 * Time: 2:44 PM
 */

namespace model\problems;
use core\security;

include_once ('../core/security.php');

class Validation extends Security
{
    public $text = "/^[a-zA-Z0-9:_\\+\-\(\)\[\] ]+$/";
    public $number = "/^[0-9]+$/";
    public $difficulty = "/^[1-5]$/";

    public function isText ($text)//title,subtitle,shortdescription,judge_language
    {
        return preg_match($this->text, $text);
    }

    public function get_text_criteria($value)
    {
        $s = " $value must contain only capital letters or lowercase letters, numbers,round and square brackets";
        return $s;
    }

    public function isNumber($number)//time_limit-1,time_limit_2,time_limit_3,volume_id,section_id,memory_limit_1,memory_limit_2,memory_limit_3,contest_id,homework_id
    {
        return preg_match($this->number,$number);
    }
    public function get_number_criteria($element)
    {
        $s = " $element must contain only numbers";
        return $s;

    }
    public function difficulty ($difficulty)
    {
        return preg_match($this->difficulty,$difficulty);
    }
    public function get_difficulty_criteria()
    {
        $s = " Difficulty must contain only number from 1 to 5 " ;
        return $s;
    }

    public function remove_script($html) {
        return preg_replace('#<script(.*?)>(.*?)</script>#is', '', $html);
    }

}
