<?php
/**
 * Created by PhpStorm.
 * problem: luba
 * Date: 12/25/15
 * Time: 4:01 PM
 */

namespace model\problems;

include_once("../core/crud_interface.php");
include_once("../core/database.php");
include_once("../model/problems/Problem.class.php");
include_once("../model/problems/Validation.php");

use \core\Crud as Crud_interface;
use \core\Database;
use model\problem_cases\ProblemCase;
use \model\problems\Validation;

use \PDO;

class Crud implements Crud_interface
{
    private $insert_query = "INSERT INTO `problems`(`title`, `subtitle`, `statement`, `time_limit_1`, `time_limit_2`, `time_limit_3`, `volume_id`, `section_id`, `memory_limit_1`, `memory_limit_2`, `memory_limit_3`, `difficulty`, `shortdescription`,`timestamp`,`judge_code`,`judge_language`,`contest_id`,`homework_id`, `ready`) VALUES (:title,:subtitle,:statement,:time_limit_1,:time_limit_2,:time_limit_3,:volume_id,:section_id,:memory_limit_1,:memory_limit_2,:memory_limit_3,:difficulty,:shortdescription,:timestamp,:judge_code,:judge_language,:contest_id,:homework_id, :ready)";
    private $select_query_all = "SELECT * FROM `problems`";
    private $select_query = "SELECT * FROM `problems`  WHERE id=:id";
    private $update_query = "UPDATE `problems` SET `title`= :title,`subtitle`= :subtitle,`statement`= :statement,`time_limit_1`= :time_limit_1,`time_limit_2`= :time_limit_2,`time_limit_3`= :time_limit_3,`volume_id`= :volume_id,`section_id`= :section_id,`memory_limit_1`= :memory_limit_1,`memory_limit_2`= :memory_limit_2,`memory_limit_3`= :memory_limit_3,`difficulty`= :difficulty,`shortdescription`= :shortdescription,`timestamp`= :timestamp,`judge_code`= :judge_code,`judge_language`= :judge_language,`contest_id`= :contest_id,`homework_id`= :homework_id, `ready`=:ready WHERE `id`= :id";
    private $delete_query = "DELETE FROM `problems` WHERE id=:id";
    private $is_during_event_query = "SELECT count(*) as cnt FROM contests, homeworks WHERE (contests.id = :contest_id and contests.start_time <=now() and contests.end_time >=now()) or (homeworks.id=:homework_id and homeworks.start_time <=now() and homeworks.end_time >=now())";

    private $contest_query = "SELECT problems.id as id, problems.title as title,problems.shortdescription,problems.difficulty ,count(submissions.id) as attempts  FROM `problems` LEFT JOIN submissions on submissions.problem_id = problems.id WHERE problems.contest_id = :id group by problems.id";
    private $homework_query = "SELECT problems.id as id, problems.title as title, problems.shortdescription, problems.difficulty , count(submissions.id) as attempts FROM `problems` LEFT JOIN submissions ON submissions.problem_id = problems.id WHERE problems.homework_id = :id group by problems.id";
    private $section_query = "SELECT problems.id as id,problems.title as title,problems.shortdescription,problems.difficulty,count(submissions.id) as attempts FROM `problems` LEFT JOIN submissions on submissions.problem_id = problems.id WHERE problems.section_id = :id group by problems.id ";
    private $volume_query = "SELECT problems.id as id,problems.title as title ,problems.shortdescription,problems.difficulty,count(submissions.id) as attempts FROM `problems` LEFT JOIN submissions on submissions.problem_id = problems.id WHERE problems.volume_id = :id group by problems.id ";
    private $db;

    public function __construct()
    {
        $db_object = new Database();
        $this->db = $db_object->get_db();
    }

    //Implement create() method.
    public function create($problem)
    {
        $return_array = array(
            "success" => true,
            "message" => ""
        );

        //Validate the problem
        $validation = new Validation();

        if (!$validation->isText($problem->getTitle())) {
            $return_array['success'] = false;
            $return_array['message'] .= $validation->get_text_criteria("title") . "\n";
        }
        if (!$validation->isText($problem->getSubtitle())) {
            $return_array['success'] = false;
            $return_array['message'] .= $validation->get_text_criteria("subtitle") . "\n";
        }

        if (!$validation->isText($problem->getShortdescription())) {
            $return_array['success'] = false;
            $return_array['message'] .= $validation->get_text_criteria("shortdescription") . "\n";
        }
        if (!$validation->isText($problem->getSubtitle())) {
            $return_array['success'] = false;
            $return_array['message'] .= $validation->get_text_criteria("judge_language") . "\n";
        }
        if (!$validation->isNumber($problem->getTimeLimit1())) {
            $return_array['success'] = false;
            $return_array['message'] .= $validation->get_number_criteria("time_limit_1") . "\n";
        }

        if (!$validation->isNumber($problem->getTimeLimit2())) {
            $return_array['success'] = false;
            $return_array['message'] .= $validation->get_number_criteria("time_limit_2") . "\n";
        }
        if (!$validation->isNumber($problem->getTimeLimit3())) {
            $return_array['success'] = false;
            $return_array['message'] .= $validation->get_number_criteria("time_limit_3") . "\n";
        }
        if (!$validation->isNumber($problem->getMemoryLimit1())) {
            $return_array['success'] = false;
            $return_array['message'] .= $validation->get_number_criteria("memory_limit_1") . "\n";
        }
        if (!$validation->isNumber($problem->getMemoryLimit2())) {
            $return_array['success'] = false;
            $return_array['message'] .= $validation->get_number_criteria("memory_limit_2") . "\n";
        }
        if (!$validation->isNumber($problem->getMemoryLimit3())) {
            $return_array['success'] = false;
            $return_array['message'] .= $validation->get_number_criteria("memory_limit_3") . "\n";
        }
        if (!$validation->isNumber($problem->getSectionId())) {
            $return_array['success'] = false;
            $return_array['message'] .= $validation->get_number_criteria("section_id") . "\n";
        }
        if (!$validation->isNumber($problem->getContestId())) {
            $return_array['success'] = false;
            $return_array['message'] .= $validation->get_number_criteria("contest_id") . "\n";
        }
        if (!$validation->isNumber($problem->getHomeworkId())) {
            $return_array['success'] = false;
            $return_array['message'] .= $validation->get_number_criteria("homework_id") . "\n";
        }
        if (!$validation->isNumber($problem->getVolumeId())) {
            $return_array['success'] = false;
            $return_array['message'] .= $validation->get_number_criteria("volume_id") . "\n";
        }
        if (!$validation->difficulty($problem->getDifficulty())) {
            $return_array['success'] = false;
            $return_array['message'] .= $validation->get_difficulty_criteria() . "\n";
        }

        //
        if ($return_array['success']) {

            //insert problem
            $stmt = $this->db->prepare($this->insert_query);
            $stmt->bindValue(":title", $validation->filter_html($problem->getTitle()), PDO::PARAM_STR);
            $stmt->bindValue(":subtitle", $validation->filter_html($problem->getSubtitle()), PDO::PARAM_STR);
            $stmt->bindValue(":statement", $validation->remove_script($problem->getStatement()), PDO::PARAM_STR);
            $stmt->bindValue(":time_limit_1", $problem->getTimeLimit1(), PDO::PARAM_INT);
            $stmt->bindValue(":time_limit_2", $problem->getTimeLimit2(), PDO::PARAM_INT);
            $stmt->bindValue(":time_limit_3", $problem->getTimeLimit3(), PDO::PARAM_INT);
            $stmt->bindValue(":volume_id", $problem->getVolumeId(), PDO::PARAM_INT);
            $stmt->bindValue(":section_id", $problem->getSectionId(), PDO::PARAM_INT);
            $stmt->bindValue(":memory_limit_1", $problem->getMemoryLimit1(), PDO::PARAM_INT);
            $stmt->bindValue(":memory_limit_2", $problem->getMemoryLimit2(), PDO::PARAM_INT);
            $stmt->bindValue(":memory_limit_3", $problem->getMemoryLimit3(), PDO::PARAM_INT);
            $stmt->bindValue(":difficulty", $problem->getDifficulty(), PDO::PARAM_INT);
            $stmt->bindValue(":shortdescription", $validation->filter_html($problem->getShortdescription()), PDO::PARAM_STR);
            $stmt->bindValue(":timestamp", $problem->getTimestamp(), PDO::PARAM_STR);
            $stmt->bindValue(":judge_code", $problem->getJudgeCode(), PDO::PARAM_STR);
            $stmt->bindValue(":judge_language", $validation->filter_html($problem->getJudgeLanguage()), PDO::PARAM_STR);
            $stmt->bindValue(":contest_id", $problem->getContestId(), PDO::PARAM_INT);
            $stmt->bindValue(":homework_id", $problem->getHomeworkId(), PDO::PARAM_INT);
            $stmt->bindValue(":ready", $problem->getReady(), PDO::PARAM_INT);

            if($stmt->execute()){
                $return_array['id'] = $this->db->lastInsertId();
            }

            return $return_array;
        } else {
            return $return_array;
        }
    }

    //Implement read() method.
    public function read($id = -1)
    {
        if ($id == -1) {
            $stmt = $this->db->prepare($this->select_query_all);
            $stmt->execute();

            $read_problems = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $read_problems;
        } else {
            $stmt = $this->db->prepare($this->select_query);
            $stmt->bindValue(":id", $id, PDO::PARAM_INT);
            $stmt->execute();

            $read_problems = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $read_problems;
        }
    }

    //  Implement update() method.
    public function update($problem, $id)
    {
        $get_problem = $this->read($id);

        $update_problem = new Problem();

        $validation = new Validation();

        $return_array = array(
            "success" => true,
            "message" => ""
        );

        //validate//title
        if (!empty($problem->getTitle()) or $problem->getTitle() !== "") {
            if ($validation->isText($problem->getTitle())) {
                $update_problem->setTitle($problem->getTitle());
            } else {
                $return_array['success'] = false;
                $return_array['message'] = $validation->get_text_criteria("title");
            }
        } else {
            $update_problem->setTitle($get_problem[0]['title']);
        }
        //validate//subtitle
        if (!empty($problem->getSubtitle()) or $problem->getSubtitle() !== "") {
            if ($validation->isText($problem->getSubtitle())) {
                $update_problem->setSubtitle($problem->getSubtitle());
            } else {
                $return_array['success'] = false;
                $return_array['message'] = $validation->get_text_criteria("subtitle");
            }
        } else {
            $update_problem->setSubtitle($get_problem[0]['subtitle']);
        }

        //validate//shortdescription
        if (!empty($problem->getShortdescription()) or $problem->getShortdescription() !== "") {
            if ($validation->isText($problem->getShortdescription())) {
                $update_problem->setShortdescription($problem->getShortdescription());
            } else {
                $return_array['success'] = false;
                $return_array['message'] = $validation->get_text_criteria("shortdescription");
            }
        } else {
            $update_problem->setShortdescription($get_problem[0]['shortdescription']);
        }

        //validate//judge_language

        if (!empty($problem->getJudgeLanguage()) or $problem->getJudgeLanguage() !== "") {
            if ($validation->isText($problem->getJudgeLanguage())) {
                $update_problem->setJudgeLanguage($problem->getJudgeLanguage());
            } else {
                $return_array['success'] = false;
                $return_array['message'] = $validation->get_text_criteria("judge_language");
            }
        } else {
            $update_problem->setJudgeLanguage($get_problem[0]['judge_language']);
        }
        //validate//time_limit_1
        if (!empty($problem->getTimeLimit1()) or $problem->getTimeLimit1() !== "") {
            if ($validation->isNumber($problem->getTimeLimit1())) {
                $update_problem->setTimeLimit1($problem->getTimeLimit1());
            } else {
                $return_array['success'] = false;
                $return_array['message'] = $validation->get_number_criteria("time_limit_1");
            }
        } else {
            $update_problem->setTimeLimit1($get_problem[0]['time_limit_1']);
        }
        //validate//time_limit_2
        if (!empty($problem->getTimeLimit2()) or $problem->getTimeLimit2() !== "") {
            if ($validation->isNumber($problem->getTimeLimit2())) {
                $update_problem->setTimeLimit2($problem->getTimeLimit2());
            } else {
                $return_array['success'] = false;
                $return_array['message'] = $validation->get_number_criteria("time_limit_2");
            }
        } else {
            $update_problem->setTimeLimit2($get_problem[0]['time_limit_2']);
        }
        //validate//time_limit_3
        if (!empty($problem->getTimeLimit3()) or $problem->getTimeLimit3() !== "") {
            if ($validation->isNumber($problem->getTimeLimit3())) {
                $update_problem->setTimeLimit3($problem->getTimeLimit3());
            } else {
                $return_array['success'] = false;
                $return_array['message'] = $validation->get_number_criteria("time_limit_3");
            }
        } else {
            $update_problem->setTimeLimit3($get_problem[0]['time_limit_3']);
        }
        //validate//memory_limit_1
        if (!empty($problem->getMemoryLimit1()) or $problem->getMemoryLimit1() !== "") {
            if ($validation->isNumber($problem->getMemoryLimit1())) {
                $update_problem->setMemoryLimit1($problem->getMemoryLimit1());
            } else {
                $return_array['success'] = false;
                $return_array['message'] = $validation->get_number_criteria("memory_limit_1");
            }
        } else {
            $update_problem->setMemoryLimit1($get_problem[0]['memory_limit_1']);
        }
        //validate//memory_limit_2
        if (!empty($problem->getMemoryLimit2()) or $problem->getMemoryLimit2() !== "") {
            if ($validation->isNumber($problem->getMemoryLimit2())) {
                $update_problem->setMemoryLimit2($problem->getMemoryLimit2());
            } else {
                $return_array['success'] = false;
                $return_array['message'] = $validation->get_number_criteria("memory_limit_2");
            }
        } else {
            $update_problem->setMemoryLimit2($get_problem[0]['memory_limit_2']);
        }
        //validate//memory_limit_3
        if (!empty($problem->getMemoryLimit3()) or $problem->getMemoryLimit3() !== "") {
            if ($validation->isNumber($problem->getMemoryLimit3())) {
                $update_problem->setMemoryLimit3($problem->getMemoryLimit3());
            } else {
                $return_array['success'] = false;
                $return_array['message'] = $validation->get_number_criteria("memory_limit_3");
            }
        } else {
            $update_problem->setMemoryLimit3($get_problem[0]['memory_limit_3']);
        }
        //validate//section_id
        if (!empty($problem->getSectionId()) or $problem->getSectionId() !== "") {
            if ($validation->isNumber($problem->getSectionId())) {
                $update_problem->setSectionId($problem->getSectionId());
            } else {
                $return_array['success'] = false;
                $return_array['message'] = $validation->get_number_criteria("section_id");
            }
        } else {
            $update_problem->setSectionId($get_problem[0]['section_id']);
        }
        //validate//volume_id
        if (!empty($problem->getVolumeId()) or $problem->getVolumeId() !== "") {
            if ($validation->isNumber($problem->getVolumeId())) {
                $update_problem->setVolumeId($problem->getVolumeId());
            } else {
                $return_array['success'] = false;
                $return_array['message'] = $validation->get_number_criteria("volume_id");
            }
        } else {
            $update_problem->setVolumeId($get_problem[0]['volume_id']);
        }
        //validate//contest_id
        if (!empty($problem->getContestId()) or $problem->getContestId() !== "") {
            if ($validation->isNumber($problem->getContestId())) {
                $update_problem->setContestId($problem->getContestId());
            } else {
                $return_array['success'] = false;
                $return_array['message'] = $validation->get_number_criteria("contest_id");
            }
        } else {
            $update_problem->setContestId($get_problem[0]['contest_id']);
        }
        //validate//homework_id
        if (!empty($problem->getHomeworkId()) or $problem->getHomeworkId() !== "") {
            if ($validation->isNumber($problem->getHomeworkId())) {
                $update_problem->setHomeworkId($problem->getHomeworkId());
            } else {
                $return_array['success'] = false;
                $return_array['message'] = $validation->get_number_criteria("homework_id");
            }
        } else {
            $update_problem->setHomeworkId($get_problem[0]['homework_id']);
        }
        //validate//statement
        if (!empty($problem->getStatement()) or $problem->getStatement() !== "") {
                $update_problem->setStatement($problem->getStatement());
        } else {
            $update_problem->setStatement($get_problem[0]['statement']);
        }
        //validate//judge_code
        if (!empty($problem->getJudgeCode()) or $problem->getJudgeCode() !== "") {
            $update_problem->setJudgeCode($problem->getJudgeCode());
        } else {
            $update_problem->setJudgeCode($get_problem[0]['judge_code']);
        }
        //validate//judge_code
        if (!empty($problem->getReady()) or $problem->getReady() !== "") {
            $update_problem->setReady($problem->getReady());
        } else {
            $update_problem->setReady($get_problem[0]['ready']);
        }
        if (!$validation->difficulty($problem->getDifficulty())) {
            $return_array['success'] = false;
            $return_array['message'] .= $validation->get_difficulty_criteria() . "\n";
        }
        if (!empty($problem->getDifficulty()) or $problem->getDifficulty() !== "") {
            if ($validation->difficulty($problem->getDifficulty())) {
                $update_problem->setDifficulty($problem->getDifficulty());
            } else {
                $return_array['success'] = false;
                $return_array['message'] = $validation->get_difficulty_criteria();
            }
        } else {
            $update_problem->setDifficulty($get_problem[0]['difficulty']);
        }
        $update_problem->setTimestamp($get_problem[0]['timestamp']);
        $update_problem->setId($get_problem[0]['id']);

        //update
        if ($return_array['success']) {
            $stmt = $this->db->prepare($this->update_query);
            $stmt->bindValue(":id", $update_problem->getId(), PDO::PARAM_INT);
            $stmt->bindValue(":title", $validation->filter_html($update_problem->getTitle()), PDO::PARAM_STR);
            $stmt->bindValue(":subtitle", $validation->filter_html($update_problem->getSubtitle()), PDO::PARAM_STR);
            $stmt->bindValue(":statement", $validation->remove_script($update_problem->getStatement()), PDO::PARAM_STR);
            $stmt->bindValue(":time_limit_1", $update_problem->getTimeLimit1(), PDO::PARAM_INT);
            $stmt->bindValue(":time_limit_2", $update_problem->getTimeLimit2(), PDO::PARAM_INT);
            $stmt->bindValue(":time_limit_3", $update_problem->getTimeLimit3(), PDO::PARAM_INT);
            $stmt->bindValue(":volume_id", $update_problem->getVolumeId(), PDO::PARAM_INT);
            $stmt->bindValue(":section_id", $update_problem->getSectionId(), PDO::PARAM_INT);
            $stmt->bindValue(":memory_limit_1", $update_problem->getMemoryLimit1(), PDO::PARAM_INT);
            $stmt->bindValue(":memory_limit_2", $update_problem->getMemoryLimit2(), PDO::PARAM_INT);
            $stmt->bindValue(":memory_limit_3", $update_problem->getMemoryLimit3(), PDO::PARAM_INT);
            $stmt->bindValue(":difficulty", $update_problem->getDifficulty(), PDO::PARAM_INT);
            $stmt->bindValue(":shortdescription", $validation->filter_html($update_problem->getShortdescription()), PDO::PARAM_STR);
            $stmt->bindValue(":timestamp", $update_problem->getTimestamp(), PDO::PARAM_STR);
            $stmt->bindValue(":judge_code", $update_problem->getJudgeCode(), PDO::PARAM_STR);
            $stmt->bindValue(":judge_language", $validation->filter_html($update_problem->getJudgeLanguage()), PDO::PARAM_STR);
            $stmt->bindValue(":contest_id", $update_problem->getContestId(), PDO::PARAM_INT);
            $stmt->bindValue(":homework_id", $update_problem->getHomeworkId(), PDO::PARAM_INT);
            $stmt->bindValue(":ready", $update_problem->getReady(), PDO::PARAM_INT);

            $stmt->execute();
            return $return_array;
        } else {
            return $return_array;
        }
    }

    public function getProblemsForContest($id = -1)
    {
        $stmt = $this->db->prepare($this->contest_query);
        $stmt->bindValue(":id", $id, PDO::PARAM_INT);
        $stmt->execute();

        $read_submissions = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $read_submissions;
    }
    public function getProblemsForHomeWork($id = -1)
    {
        $stmt = $this->db->prepare($this->homework_query);
        $stmt->bindValue(":id", $id, PDO::PARAM_INT);
        $stmt->execute();

        $read_submissions = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $read_submissions;
    }
    public function getProblemsForSection($id = -1)
    {
        $stmt = $this->db->prepare($this->section_query);
        $stmt->bindValue(":id", $id, PDO::PARAM_INT);
        $stmt->execute();

        $read_submissions = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $read_submissions;
    }
    public function getProblemsForVolume($id = -1)
    {
        $stmt = $this->db->prepare($this->volume_query);
        $stmt->bindValue(":id", $id, PDO::PARAM_INT);
        $stmt->execute();

        $read_submissions = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $read_submissions;
    }


    // Implement delete() method.
    public function delete($id)
    {
        $stmt = $this->db->prepare($this->delete_query);
        $stmt->bindValue(":id", $id, PDO::PARAM_INT);
        $stmt->execute();
        return true;
    }

    public function isDuringEvent($contest_id, $homework_id){
        $stmt = $this->db->prepare($this->is_during_event_query);
        $stmt->bindValue(":contest_id", $contest_id, PDO::PARAM_INT);
        $stmt->bindValue(":homework_id", $homework_id, PDO::PARAM_INT);

        $stmt->execute();

        $read = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if(!empty($read) && $read[0]['cnt']=='1')
            return true;
        else return false;
    }
}
