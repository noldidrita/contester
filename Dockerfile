FROM php:7.0-apache
RUN a2enmod rewrite
RUN docker-php-ext-install pdo pdo_mysql
RUN apt-get update && apt-get -y install time

#JAVA Compiler
RUN apt-get -y install default-jdk

#Python compiler
RUN apt-get install -y python-software-properties

WORKDIR /var/www/html/

COPY . .

RUN chown -R www-data:www-data /var/www/html
RUN chgrp -R www-data reports uploads
RUN chmod -R ug+rwx reports uploads