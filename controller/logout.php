<?php
/**
 * Created by PhpStorm.
 * User: tdc
 * Date: 1/9/16
 * Time: 6:44 PM
 */
session_start();

include("util.php");
$_SESSION['user'] = null;
session_destroy();
$host  = $_SERVER['HTTP_HOST'];
$page = "login";
header("Location: http://$host/$page");
?>
