<?php
/**
 * Created by PhpStorm.
 * User: tdc
 * Date: 1/19/16
 * Time: 8:57 PM
 */

include_once("../model/users/User.crud.php");
include_once("util.php");

use model\users\Crud as UserCrud;

session_start();

if(!isset($_SESSION['user'])) {
    $host  = $_SERVER['HTTP_HOST'];
    $page = "login";
    header("Location: http://$host/$page");
    return;
}

include("../view/index-logged.php");