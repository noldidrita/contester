<?php
/**
 * Created by PhpStorm.
 * User: tdc
 * Date: 1/20/16
 * Time: 2:55 PM
 */
define("ROOT", "");

function load_compilers() {
    $compilers_dir = "../compilers";
    $files = Array();
    if ($handle = opendir($compilers_dir)) {
        while (($file = readdir($handle))!==false) {
            if ($file != "." && $file != "..") {
                $file = explode(".", $file)[0];
                array_push($files, $file);
            }
        }
        closedir($handle);
    }
    return $files;
}

function getTypeString($type) {
    if($type == 0) {
        return "Not Approved";
    }else if($type == 1)
        return "Student";
    else if($type == 2)
        return "Mentor";
    else if($type == 3)
        return "Assistant";
    else if($type == 4)
        return "Admin";
}

function dateDiff($time1, $time2, $precision = 6) {
    // If not numeric then convert texts to unix timestamps
    if (!is_int($time1)) {
        $time1 = strtotime($time1);
    }
    if (!is_int($time2)) {
        $time2 = strtotime($time2);
    }

    // If time1 is bigger than time2
    // Then swap time1 and time2
    if ($time1 > $time2) {
        $ttime = $time1;
        $time1 = $time2;
        $time2 = $ttime;
    }

    // Set up intervals and diffs arrays
    $intervals = array('year','month','day','hour','minute','second');
    $diffs = array();

    // Loop thru all intervals
    foreach ($intervals as $interval) {
        // Create temp time from time1 and interval
        $ttime = strtotime('+1 ' . $interval, $time1);
        // Set initial values
        $add = 1;
        $looped = 0;
        // Loop until temp time is smaller than time2
        while ($time2 >= $ttime) {
            // Create new temp time from time1 and interval
            $add++;
            $ttime = strtotime("+" . $add . " " . $interval, $time1);
            $looped++;
        }

        $time1 = strtotime("+" . $looped . " " . $interval, $time1);
        $diffs[$interval] = $looped;
    }

    $count = 0;
    $times = array();
    // Loop thru all diffs
    foreach ($diffs as $interval => $value) {
        // Break if we have needed precission
        if ($count >= $precision) {
            break;
        }
        // Add value and interval
        // if value is bigger than 0
        if ($value > 0) {
            // Add s if value is not 1
            if ($value != 1) {
                $interval .= "s";
            }
            // Add value and interval to times array
            $times[] = $value . " " . $interval;
            $count++;
        }
    }

    // Return string with times
    return implode(", ", $times);
}

function makeReportPdf($reportFile, $show) {
    require('../core/fpdf.php');
    $report = json_decode(file_get_contents($reportFile));
    $pdf = new FPDF();
    $pdf->AddPage();
    $pdf->SetFont('Arial','U',16);
    $pdf->SetTextColor(0,0,0);

    //width: 0 (Full page)
    //height: 10mm
    //Text: submission Report
    //border: 0
    //1: new line
    //C: Center
    $pdf->Cell(0,10,"Submission Report", 0, 1, "C");

    $pdf->SetFont('Arial','',12);
    $pdf->Cell(0,10,"User ID: ".$report->user_id, 0, 1);
    $pdf->Cell(0,10,"Username: ".$report->username, 0, 1);
    $pdf->Cell(0,10,"Submission Language: ".$report->language, 0, 1);
    $pdf->Cell(0,10,"Compiler Version: ".$report->compiler_version, 0, 1);
    $pdf->SetTextColor(0,0,255);
    $pdf->Cell(0,10,"Problem id: ".$report->problem_id, 0, 1, "", "", "http://stud-proj.epoka.edu.al/~adrita13/web16_adrita13/problems/".$report->problem_id);
    $pdf->SetTextColor(0);

    $pdf->Cell(0,10,"Submission Time: ".$report->timestamp, 0, 1);
    $pdf->Multicell(0,6,"Compilation: ".$report->Compiler, 0);
    $pdf->Line(0,$pdf->GetY(), 300, $pdf->GetY());
    $pdf->Line(0,$pdf->GetY()+1, 300, $pdf->GetY()+1);
    if(isset($report->test_cases)) {
        foreach ($report->test_cases as $case) {

            $pdf->SetFont('Arial', 'B', 15);
            $pdf->Cell(0, 20, "Case Nr: " . $case->test_nr, 0, 1, "C");

            $pdf->SetFont('Arial', 'U', 14);
            $pdf->Cell(0, 15, "Problem's Input", 0, 1, "");
            $pdf->SetFont('Arial', '', 12);
            $pdf->Multicell(0, 6, $case->input, 0);

            $pdf->SetFont('Arial', 'U', 14);
            $pdf->Cell(0, 15, "Your Output", 0, 1, "");
            $pdf->SetFont('Arial', '', 12);
            $pdf->Multicell(0, 6, $case->output, 0);

            $pdf->SetFont('Arial', 'U', 14);
            $pdf->Cell(0, 15, "Judge's Pattern", 0, 1, "");
            $pdf->SetFont('Arial', '', 12);
            $pdf->Multicell(0, 6, $case->pattern, 0);

            if (isset($case->error) && $case->error != "") {
                $pdf->SetFont('Arial', 'U', 14);
                $pdf->Cell(0, 15, "Error output", 0, 1, "");
                $pdf->SetFont('Arial', '', 12);
                $pdf->Multicell(0, 6, $case->error, 0);
            }

            $pdf->Ln(10);
            $pdf->Cell(0, 10, "Memory Usage: " . $case->memory_usage . " KBytes", 0, 1);
            $pdf->Cell(0, 10, "Memory Limit: " . $case->memory_limit . " KBytes", 0, 1);
            if (strpos($case->verdict, "Accepted") !== false) {
                $pdf->SetTextColor(0, 153, 51);
            } else {
                $pdf->SetTextColor(255, 51, 0);
            }
            $pdf->Cell(0, 15, "Verdict: " . $case->verdict, 0, 1);
            $pdf->SetTextColor(0, 0, 0);

            $pdf->Line(0, $pdf->GetY(), 300, $pdf->GetY());
        }
    }
    $pdf->Ln(20);
    $pdf->SetFont('Arial','U',16);
    $pdf->SetTextColor(220,50,50);
    $pdf->Cell(0,10,"Final: ".$report->final." Points", 0, 1, "R");
    if($show) {
        $pdf->Output();
    } else {
        return $pdf->Output("", "S");
    }
}

function sendReportByEmail($reportFile, $to, $name, $problem_name) {
    $from = "noldidrita@gmail.com";
    $subject = "SAT submission report";
    $message = "<p>Dear $name,<br/><br/> Attached you can see the report file for the solution to the problem: '$problem_name' that you've recently submitted.<br/><br/>Thank you,<br/>SAT Administration Board</p>";
    $separator = md5(time());
    $eol = PHP_EOL;
    $filename = "report.pdf";
    $pdfdoc = makeReportPdf($reportFile, false);
    $attachment = chunk_split(base64_encode($pdfdoc));

    // main header
    $headers  = "From: ".$from.$eol;
    $headers .= "MIME-Version: 1.0".$eol;
    $headers .= "Content-Type: multipart/mixed; boundary=\"".$separator."\"";

    // message
    $body = "--".$separator.$eol;
    $body .= "Content-Type: text/html; charset=\"iso-8859-1\"".$eol;
    $body .= "Content-Transfer-Encoding: 8bit".$eol.$eol;
    $body .= $message.$eol;

    // attachment
    $body .= "--".$separator.$eol;
    $body .= "Content-Type: application/octet-stream; name=\"".$filename."\"".$eol;
    $body .= "Content-Transfer-Encoding: base64".$eol;
    $body .= "Content-Disposition: attachment".$eol.$eol;
    $body .= $attachment.$eol;
    $body .= "--".$separator."--";

    // send message
    mail($to, $subject, $body, $headers);
}

$difficulty_array=[" ","difficulties_1.png","difficulties_2.png","difficulties_3.png","difficulties_4.png","difficulties_5.png"];

