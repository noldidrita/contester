<?php
/**
 * Created by PhpStorm.
 * User: TDC
 * Date: 22.1.2016
 * Time: 20:17
 */

include_once("../model/users/User.crud.php");
include_once("../model/users/User.class.php");
include_once("../model/groups/Group.class.php");
include_once("../model/groups/Group.crud.php");

include_once("util.php");

use model\users\Crud as UserCrud;
use model\users\User as User;
use model\groups\Crud as GroupCrud;
use model\groups\Group as Group;

session_start();

if (!isset($_SESSION['user']) || $_SESSION['user']->getType() < 3) {
    $host  = $_SERVER['HTTP_HOST'];
    $page = "home";
    header("Location: http://$host/$page");
    return;
}

$userCrud = new UserCrud();
$groupCrud = new GroupCrud();
$error = false;

if (isset($_POST['action']) && $_POST['action'] == "delete" && isset($_POST['ids']) && is_array($_POST['ids'])) {
    foreach ($_POST['ids'] as $id) {
        if (ctype_digit($id)) {
            $groupCrud->delete($id);
        }
    }
} else {
    if (isset($_POST['submit']) && $_POST['submit'] == 'Add' && isset($_POST['group_name']) && isset($_POST['group_description'])) {
        $newGroup = new Group("", $_POST['group_name'], $_POST['group_description'], date('Y-m-d H:i:s'));
        $insert_result = $groupCrud->create($newGroup);
        if ($insert_result['success'] == false) {
            $error = true;
        }
    } else if (isset($_POST['submit']) && $_POST['submit'] == 'Change' && isset($_POST['group_id'])) {
        $newGroup = new Group("", $_POST['group_name'], $_POST['group_description'], "");
        $insert_result = $groupCrud->update($newGroup, $_POST['group_id']);
        if ($insert_result['success'] == false) {
            $error = true;
        }
    }
    $id = -1;
    if (isset($_GET['id']) && $_GET['id'] != "") {
        $id = $_GET['id'];
        $groups = $groupCrud->read($id);
        if(!empty($groups)) {
            $group = $groups[0];
            $users = $userCrud->getByGroup($id);
            include("../view/group_detail.php");
        } else {
            echo "Not found";
        }
    } else {
        $groups = $groupCrud->read($id);
        include("../view/groups.php");
    }
}