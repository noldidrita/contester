<?php
/**
 * Created by PhpStorm.
 * User: TDC
 * Date: 23.1.2016
 * Time: 19:17
 */


include_once("../model/users/User.crud.php");
include_once("../model/users/User.class.php");
include_once("../model/sections/Section.crud.php");
include_once("../model/volumes/Volume.crud.php");
include_once("../model/contests/Contest.crud.php");
include_once("../model/homeworks/Homework.crud.php");
include_once("../model/problems/Problem.crud.php");
include_once("../model/problems/Problem.class.php");
include_once("../model/problem_cases/ProblemCase.crud.php");
include_once("../model/problem_cases/ProblemCase.class.php");

include_once("../core/compiler.php");

include_once("util.php");

use model\users\Crud as UserCrud;
use model\users\User as User;
use model\sections\Crud as SectionCrud;
use model\volumes\Crud as VolumeCrud;
use model\homeworks\Crud as HomeworkCrud;
use model\contests\Crud as ContestCrud;
use model\problems\Problem as Problem;
use model\problems\Crud as ProblemCrud;
use model\problem_cases\Crud as ProblemCaseCrud;
use model\problem_Cases\ProblemCase as ProblemCase;

use core\Compiler as Compiler;

session_start();

if (!isset($_SESSION['user']) || $_SESSION['user']->getType() < 3) {
    $host  = $_SERVER['HTTP_HOST'];
    $page = "home";
    header("Location: http://$host/$page");
    return;
}

$error = false;
$message = "";
$edit = false;
$sectionCrud = new SectionCrud();
$volumeCrud = new VolumeCrud();
$homeworkCrud = new HomeworkCrud();
$contestCrud = new ContestCrud();
$problemCrud = new ProblemCrud();
$problemCaseCrud = new ProblemCaseCrud();

$sections = $sectionCrud->read();
$volumes = $volumeCrud->read();
$homeworks = $homeworkCrud->read();
$contests = $contestCrud->read();
$compilerFiles = load_compilers();
$compilers = Array();

foreach($compilerFiles as $compilerFile) {
    $compiler = new Compiler();
    $compiler->load($compilerFile);
    $data["value"] = $compilerFile;
    $data["name"] = $compiler->getName();

    array_push($compilers, $data);
}

if(isset($_POST['submit'])) {
    $ready = 0;
    if(isset($_POST['ready']) && $_POST['ready']==true)
        $ready = 1;
    $problem = new Problem("", $_POST['title'], $_POST['subtitle'], $_POST['statement'], $_POST['time_limit_1'], $_POST['time_limit_2'], $_POST['time_limit_3'], $_POST['volume_id'], $_POST['section_id'], $_POST['memory_limit_1'], $_POST['memory_limit_2'], $_POST['memory_limit_3'], $_POST['difficulty'], $_POST['short_description'], date('Y-m-d H:i:s'), $_POST['judge_code'],$_POST['judge_language'], $_POST['contest_id'], $_POST['homework_id'], $ready);
    if($_POST['submit']=="Save" && isset($_GET['id'])) {
        $result = $problemCrud->update($problem, $_GET['id']);
        $result['id'] = $_GET['id'];
    } else {
        $result = $problemCrud->create($problem);
    }
    if($result['success']==false) {
        $error = true;
        $message = $result['message'];
    } else {
        $id = $result['id'];
        //This shouldn't happen
        if ($id == "" || $id==0) {
            $error = true;
            $message = "Some error happened. Please try later.";
            return;
        } else {
            if (isset($_POST["input"])) {
                for ($i = 0; $i < count($_POST["input"]); $i++) {
                    $problemCase = new ProblemCase("", $id, $_POST["caseNr"][$i], $_POST["input"][$i], $_POST["pattern"][$i], $_POST["points"][$i]);
                    $result = $problemCaseCrud->create($problemCase);
                    if ($result['success'] == false) {
                        $error = true;
                        $message .= $result['message'];
                    }
                }
            }
            if($error==false) {
                $host  = $_SERVER['HTTP_HOST'];
                $page = "problems/$id";
                header("Location: http://$host/$page");
                return;
            }
        }
    }
}

if(isset($_GET['edit']) && isset($_GET['id'])) {
    $edit = true;
    $problem = $problemCrud->read($_GET['id']);
    $problemCases = $problemCaseCrud->readForProblemId($_GET['id']);
    if(empty($problem)) {
        $edit = false;
    } else {
        $problem = $problem[0];
    }
}

include("../view/addProblem.php");
