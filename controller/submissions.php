<?php
/**
 * Created by PhpStorm.
 * User: TDC
 * Date: 24.1.2016
 * Time: 20:35
 */

include_once("../model/users/User.crud.php");
include_once("../model/users/User.class.php");
include_once("../model/submissions/Submission.class.php");
include_once("../model/submissions/Submission.crud.php");

include_once("util.php");

use model\users\Crud as UserCrud;
use model\users\User as User;
use model\submissions\Crud as SubmissionCrud;

session_start();

if (!isset($_SESSION['user']) || !isset($_GET['id'])) {
    $host  = $_SERVER['HTTP_HOST'];
    $page = "home";
    header("Location: http://$host/$page");
    return;
}

$submissionCrud = new SubmissionCrud();
$userCrud = new UserCrud();
$error = false;
$success = false;
$submission = $submissionCrud->getForSubmissionId($_GET['id']);
if(empty($submission)){
    $host  = $_SERVER['HTTP_HOST'];
    $page = "home";
    header("Location: http://$host/$page");
    return;
}
$submission = $submission[0];
$user = $userCrud->read($submission['user_id'])[0];

if ($_SESSION['user']->getType()>2 || ($_SESSION['user']->getType()==2 && $_SESSION['user']->getGroupId()== $user['group_id'])) {
    if(isset($_POST['Submit']) && $_POST['Submit']=="View Report") {
        makeReportPdf($submission['report_path'], true);
    } else {
        if (isset($_POST['Submit']) && $_POST['Submit'] == "Send Report By Email") {
            sendReportByEmail($submission['report_path'], $user['email'], $user['name']." ".$user['surname'], $submission['problem_name']);
            $success = true;
        }
        include("../view/submissions.php");
    }
} else if($_SESSION['user']->getId()==$submission['user_id']) {
    include("../view/submissions.php");
}
else {
    header("location: home");
    return;
}