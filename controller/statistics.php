<?php
/**
 * Created by PhpStorm.
 * User: luba
 * Date: 25.1.2016
 * Time: 14:33
 */
include_once("../model/users/User.crud.php");
include_once("../model/users/User.class.php");
include_once("../model/groups/Group.class.php");
include_once("../model/groups/Group.crud.php");

include_once("util.php");
session_start();
use model\users\Crud as UserCrud;
use model\users\User as User;
use model\groups\Crud as GroupCrud;
use model\groups\Group as Group;

$userCrud = new UserCrud();
$groupCrud = new GroupCrud();

$users = $userCrud->read();
include("../view/statistics.php");