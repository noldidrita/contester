<?php
/**
 * Created by PhpStorm.
 * User: tdc
 * Date: 1/9/16
 * Time: 6:44 PM
 */
include_once("../model/users/User.crud.php");
include_once("../model/users/User.class.php");
include_once("util.php");

use model\users\Crud as UserCrud;
use model\users\User as User;
session_start();
if(isset($_SESSION['user'])) {
    $host  = $_SERVER['HTTP_HOST'];
    $page = "home";
    header("Location: http://$host/$page");
    return;
}
$error = false;
$registered = false;

if (isset($_POST['submit']) && $_POST['submit']==="login") {
    $crud = new UserCrud();
    $user = $crud->checkLogin($_POST['username'], $_POST['password']);
    if (empty($user)) {
        $error = true;
        $message = "Unsuccessful Login";
    } else {
        $user_object = new User();
        $user_object->buildFromArray($user[0]);
        $_SESSION['user'] = $user_object;
        $host  = $_SERVER['HTTP_HOST'];
        $page = "home";
        header("Location: http://$host/$page");
    }
} else if (isset($_POST['submit']) && $_POST['submit']==="signup") {
    if (!isset($_POST['password']) || !isset($_POST['confirm_password']) || $_POST['password'] !== $_POST['confirm_password']) {
        $error = true;
        $message = "Passwords do not match.";
    } else {
        $pictureUrl = "";
        if (!empty($_FILES['picture']['name'])) {
            $valid_formats = array("jpg", "png", "jpeg", "gif");
            //$max_file_size = 1024 * 200;
            $path = "../uploads/profileImg/";
            $filename = rand() . '_' . basename($_FILES["picture"]["name"]);
            $target_file = $path . $filename;
            if (in_array(pathinfo($_FILES['picture']['name'], PATHINFO_EXTENSION), $valid_formats)) {
                if (move_uploaded_file($_FILES["picture"]["tmp_name"], $target_file)) {
                    $pictureUrl =  $filename;
                }
            }
        }

        $crud = new UserCrud();
        $timestamp = date('Y-m-d H:i:s');
        $birthdate = $_POST['birthday_year'] . '-' . $_POST['birthday_month'] . '-' . $_POST['birthday_day'];
        $user = new User("", $_POST['name'], $_POST['username'], $_POST['surname'], $_POST['email'], $birthdate, $pictureUrl, $_POST['gender'], 1, 0, $_POST['password'], $timestamp, 0, 0);
        $result = $crud->create($user);
        if ($result['success']) {
            $registered = true;
        } else {
            $error = true;
            $message = $result['message'];
            if (!empty($pictureUrl)) {
                unlink($path.$pictureUrl);
            }
        }
    }
}
include("../view/index.php");
?>
