<?php
/**
 * Created by PhpStorm.
 * User: luba
 * Date: 23.1.2016
 * Time: 14:30
 */
include_once("../model/users/User.class.php");
include_once("../model/contests/Contest.crud.php");
include_once("../model/contests/Contest.class.php");
include_once("../model/problems/Problem.crud.php");
include_once("../model/groups/Group.crud.php");

include_once("../controller/util.php");

use model\users\User as User;
use model\contests\Crud as ContestCrud;
use model\contests\Contest as Contest;
use model\problems\Crud as ProblemCrud;
use model\groups\Crud as GroupCrud;

session_start();

$groupCrud = new GroupCrud();
$crud = new ContestCrud();
$problemCrud = new ProblemCrud();
$error = false;
if(isset($_GET['page']) && $_GET['page'] == "scoreboard" && isset($_GET['id'])) {
    $event= $crud->read($_GET['id']);
    $problems = $problemCrud->getProblemsForContest($_GET['id']);
    if(empty($event)) {
        echo "Contest not found.";
        return;
    }
    $event = $event[0];
    $title = $event['title'];
    $scoreboard = $crud->getScoreboardResults($_GET['id']);
    $crtime=date('Y-m-d H:i:s');

    include("../view/scoreboard.php");

} else if(isset($_GET['id'])){

    $event = $crud->read($_GET['id']);
    $problems = $problemCrud->getProblemsForContest($_GET['id']);
        if(empty($event)) {
            echo "Contest not found.";
            return;
        }
    $event = $event[0];
    include("../view/problems.php");
} else {

    if (!isset($_SESSION['user']) || $_SESSION['user']->getType() >2) {
        if (isset($_POST['action']) && $_POST['action'] == "delete" && isset($_POST['ids']) && is_array($_POST['ids'])) {
            foreach ($_POST['ids'] as $id) {
                if (ctype_digit($id)) {
                    $crud->delete($id);
                }
            }
        } else {
            if (isset($_POST['submit']) && $_POST['submit'] == 'Add' && isset($_POST['end_time'])&& isset($_POST['start_time'])&& isset($_POST['contest_name']) && isset($_POST['contest_description'])) {
                $newHomework = new Contest("", $_POST['contest_name'],$_POST['start_time'] ,$_POST['end_time'], $_POST['contest_description']);
                $insert_result = $crud->create($newHomework);
                if ($insert_result['success'] == false) {
                    $error = true;
                }
            }
        }
    }
    $result = $crud->read();

    include("../view/contests.php");
}

