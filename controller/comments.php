<?php
/**
 * Created by PhpStorm.
 * User: TDC
 * Date: 24.1.2016
 * Time: 12:36
 */

use model\problems\Crud as ProblemCrud;
use model\problems\Problem as Problem;

use model\comments\Comment as Comment;
use model\comments\Crud as CommentCrud;

include_once("../model/users/User.class.php");
include_once("../model/problems/Problem.class.php");
include_once("../model/problems/Problem.crud.php");
include_once("../model/comments/Comment.class.php");
include_once("../model/comments/Comment.crud.php");
include_once("util.php");

session_start();

if (!isset($_GET['id']) || empty($_GET['id']) || !ctype_digit($_GET['id'])) {
    $host  = $_SERVER['HTTP_HOST'];
    $page = "home";
    header("Location: http://$host/$page");
       return;
}

$problemCrud = new ProblemCrud();
$commentCrud = new CommentCrud();

$problem_id = $_GET['id'];
$problem = $problemCrud->read($problem_id);
$success = false;
$error = false;

if (empty($problem) || ($problem[0]['ready'] == 0 && (!isset($_SESSION['user']) || $_SESSION['user']->getType()<3))) {
    //   header("location: home");
    echo "Empty";
    return;
}
$problem = $problem[0];

if (isset($_GET['comment_id']) && ctype_digit($_GET['comment_id']) && isset($_GET['action']) && $_GET['action'] == "remove") {
    $comment = $commentCrud->read($_GET['comment_id']);

    if (empty($comment)) {
        $host  = $_SERVER['HTTP_HOST'];
        $page = "home";
        header("Location: http://$host/$page");
        return;
    }

    $comment = $comment[0];
    if (isset($_SESSION['user']) && $_SESSION['user']->getType() < 3 && $comment['user_id'] != $_SESSION['user']->getId()) {
        $error = true;
        $message = "You have no permission to remove this comment";
    } else {
        $commentCrud->delete($comment['id']);
    }
} else if (isset($_POST['Submit']) && isset($_GET['action']) && $_GET['action'] == "add" && isset($_POST['text'])) {
    if(!isset($_SESSION['user'])) {
        $error = true;
        $message = "You must be logged in to add a comment";
    } else if($_POST['text']=="") {
        $error = true;
        $message = "Comment text cannot be empty.";
    }else {
        $comment = new Comment("", $_SESSION['user']->getId(), $_POST['text'], $problem_id, date('Y-m-d H:i:s'));
        $result = $commentCrud->create($comment);
        if($result['success'] == false) {
            $error = true;
            $message = $result['message'];
        } else {
            $success = true;
        }
    }
}

$comments = $commentCrud->getForProblemId($problem_id);
include("../view/comments.php");
