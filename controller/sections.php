<?php
/**
 * Created by PhpStorm.
 * User: luba
 * Date: 1/24/16
 * Time: 5:51 PM
 */

include_once("../model/users/User.class.php");
include_once("../model/sections/Section.crud.php");
include_once("../model/sections/Section.class.php");

include_once("../model/problems/Problem.crud.php");

include_once("../controller/util.php");

use model\users\User as User;
use model\sections\Crud as SectionCrud;
use model\sections\Section as Section;
use model\problems\Crud as ProblemCrud;

session_start();

$crud = new SectionCrud();
$problemCrud = new ProblemCrud();

if (!isset($_SESSION['user']) || $_SESSION['user']->getType() >2) {
    if (isset($_POST['action']) && $_POST['action'] == "delete" && isset($_POST['ids']) && is_array($_POST['ids'])) {
        foreach ($_POST['ids'] as $id) {
            if (ctype_digit($id)) {
                $res = $crud->delete($id);
                if(!$res['success']) {
                    echo $res['message'];
                }
            }
        }
    } else {
        if (isset($_POST['submit']) && $_POST['submit'] == 'Add' && isset($_POST['section_name']) && isset($_POST['section_description'])) {
            $newSection = new Section("", $_POST['section_name'], $_POST['section_description'], date('Y-m-d H:i:s'));
            $insert_result = $crud->create($newSection);
            if ($insert_result['success'] == false) {
                $error = true;
            }
        }
    }
}

if(isset($_GET['id'])) {

    $event = $crud->read($_GET['id']);
    $problems = $problemCrud->getProblemsForSection($_GET['id']);
    if(empty($event)) {
        echo "Section not found.";
        return;
    }
    $event = $event[0];
    $event['title'] = $event['name'];

    include("../view/problems.php");

} else {
    $result = $crud->read();

    include("../view/sections.php");
}