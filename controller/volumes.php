<?php
/**
 * Created by PhpStorm.
 * User: luba
 * Date: 1/24/16
 * Time: 6:12 PM
 */
include_once("../model/users/User.class.php");
include_once("../model/volumes/Volume.crud.php");
include_once("../model/problems/Problem.crud.php");

include_once("../controller/util.php");

use model\users\User as User;
use model\volumes\Crud as VolumeCrud;
use model\volumes\Volume as Volume;
use model\problems\Crud as ProblemCrud;

session_start();

$crud = new VolumeCrud();
$problemCrud = new ProblemCrud();

if (!isset($_SESSION['user']) || $_SESSION['user']->getType() >2) {
    if (isset($_POST['action']) && $_POST['action'] == "delete" && isset($_POST['ids']) && is_array($_POST['ids'])) {
        foreach ($_POST['ids'] as $id) {
            if (ctype_digit($id)) {
                $crud->delete($id);
            }
        }
    } else {
        if (isset($_POST['submit']) && $_POST['submit'] == 'Add' && isset($_POST['volume_name']) && isset($_POST['volume_description'])) {
            $newVolume = new Volume("", $_POST['volume_name'], $_POST['volume_description'], date('Y-m-d H:i:s'));
            $insert_result = $crud->create($newVolume);
            if ($insert_result['success'] == false) {
                $error = true;
            }
        }
    }
}
if(isset($_GET['id'])) {

    $event = $crud->read($_GET['id']);
    $problems = $problemCrud->getProblemsForVolume($_GET['id']);
    if(empty($event)) {
        echo "Volume not found.";
        return;
    }
    $event = $event[0];
    $event['title'] = $event['name'];

    include("../view/problems.php");

} else {

    $result = $crud->read();

    include("../view/volumes.php");
}