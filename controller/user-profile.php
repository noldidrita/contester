<?php
/**
 * Created by PhpStorm.
 * User: tdc
 * Date: 21.1.2016
 * Time: 21:05
 */

include_once("../model/users/User.crud.php");
include_once("../model/users/User.class.php");
include_once("../model/submissions/Submission.crud.php");

include_once("util.php");

use model\users\Crud as UserCrud;
use model\users\User as User;
use model\submissions\Crud as SubmissionCrud;

session_start();

if(!isset($_GET['id']) && !isset($_SESSION['user'])) {
    $host  = $_SERVER['HTTP_HOST'];
    $page = "home";
    header("Location: http://$host/$page");
    return;
}

$userCrud = new UserCrud();
$submissionCrud = new SubmissionCrud();
if(isset($_GET['id'])) {
    $userData = $userCrud->read($_GET['id']);
    if (!empty($userData)) {
        $user = new User();
        $user->buildFromArray($userData[0]);
    } else {
        $host  = $_SERVER['HTTP_HOST'];
        $page = "home";
        header("Location: http://$host/$page");
        return;
    }
} else {
    $user = $_SESSION['user'];
}
$updated = false;
$error = false;

if(isset($_GET['action']) && $_GET['action']=="edit") {
    if (($_SESSION['user']->getType() == 4) || $_SESSION['user']->getId() == $user->getId()) {
        if(isset($_POST['Submit']) && $_POST['Submit']=="Save") {
            if ($_POST['password'] !== $_POST['confirm_password']) {
                $error = true;
                $message = "Passwords do not match.";
            } else {
                $pictureUrl = "";
                if (!empty($_FILES['picture']['name'])) {
                    $valid_formats = array("jpg", "png", "jpeg", "gif");
                    //$max_file_size = 1024 * 200;
                    $path = "../uploads/profileImg/";
                    $filename = rand() . '_' . basename($_FILES["picture"]["name"]);
                    $target_file = $path . $filename;
                    if (in_array(pathinfo($_FILES['picture']['name'], PATHINFO_EXTENSION), $valid_formats)) {
                        if (move_uploaded_file($_FILES["picture"]["tmp_name"], $target_file)) {
                            $pictureUrl =  $filename;
                        }
                    }
                }

                $user_update = new User("", $_POST['name'], $_POST['username'], $_POST['surname'], "", "", $pictureUrl, "", "", "", $_POST['password'], "", "", "");
                $result = $userCrud->update($user_update, $user->getId());
                if ($result['success']) {
                    $updated = true;
                    if($_SESSION['user']->getId()==$user->getId()) {
                        $user = new User();
                        $user->buildFromArray($userCrud->read($_SESSION['user']->getId())[0]);
                        $_SESSION['user'] = $user;
                    }
                } else {
                    $error = true;
                    $message = $result['message'];
                    if (!empty($pictureUrl)) {
                        unlink($path.$pictureUrl);
                    }
                }
            }

        }
        include("../view/profile-edit.php");
    }
}
else {
    if (isset($_SESSION['user'])) {
        if (($_SESSION['user']->getType() >= 3) || ($_SESSION['user']->getType() == 2 && $_SESSION['user']->getGroupId() == $user->getGroupId()) || $_SESSION['user']->getId() == $user->getId()) {
            $submissions = $submissionCrud->getForUserId($user->getId());
        }
    }

    include("../view/profile.php");
}