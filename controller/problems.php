<?php
/**
 * Created by PhpStorm.
 * User: TDC
 * Date: 24.1.2016
 * Time: 18:51
 */
use model\submissions\Crud as SubmissionCrud;
use model\submissions\Submission;
use model\users\User as User;
use model\users\Crud as UserCrud;

use core\Compiler as Compiler;
use model\problems\Crud as ProblemCrud;
use model\problems\Problem as Problem;
use model\problem_cases\Crud as ProblemCasesCrud;
use model\problem_cases\ProblemCase as ProblemCase;

include_once("../model/submissions/Submission.crud.php");
include_once("../model/users/User.class.php");
include_once("../model/users/User.crud.php");
include_once("../model/problems/Problem.class.php");
include_once("../model/problems/Problem.crud.php");
include_once("../model/problem_cases/ProblemCase.class.php");
include_once("../model/problem_cases/ProblemCase.crud.php");
include_once("../model/submissions/Submission.class.php");
include_once("../model/submissions/Submission.crud.php");

include_once("../core/compiler.php");
include_once("util.php");

session_start();

if (!isset($_GET['id']) || empty($_GET['id']) || !ctype_digit($_GET['id'])) {
    $host  = $_SERVER['HTTP_HOST'];
    $page = "home";
    header("Location: http://$host/$page");
    return;
}

$problemCrud = new ProblemCrud();
$submissionCrud = new SubmissionCrud();

$problem_id = $_GET['id'];
$problem = $problemCrud->read($problem_id);

if (empty($problem) || ($problem[0]['ready'] == 0 && (!isset($_SESSION['user']) || $_SESSION['user']->getType()<3))) {
    $host  = $_SERVER['HTTP_HOST'];
    $page = "home";
    header("Location: http://$host/$page");
    return;
}

$problem = $problem[0];

$compilerFiles = load_compilers();
$compilers = Array();

foreach ($compilerFiles as $compilerFile) {
    $compiler = new Compiler();
    $compiler->load($compilerFile);
    $data["value"] = $compilerFile;
    $data["name"] = $compiler->getName();

    array_push($compilers, $data);
}
$error = false;

if (isset($_SESSION['user'])) {
    //If admin, get every submission
    if ($_SESSION['user']->getType() >= 3) {
        $submissions = $submissionCrud->getForProblemId($problem['id']);
        //If mentor, get his and then the group's submission
    } else if ($_SESSION['user']->getType() == 2) {
        $submissions = $submissionCrud->getForGroupUserId($problem['id'], $_SESSION['user']->getId(), $_SESSION['user']->getGroupId());
        //If just a student, get his submissions only
    } else {
        $submissions = $submissionCrud->getForGroupUserId($problem['id'], $_SESSION['user']->getId());
    }
}

$success = false;
$error = false;
$problem_posted = false;

if (isset($_POST['Submit']) && isset($_GET['action']) && $_GET['action'] == "submit" && isset($_POST['code']) && isset($_SESSION['user']) && $_SESSION['user']->getType() != 0 && in_array($_POST['language'], load_compilers())) {
    $timestamp = date('Y-m-d H:i:s');
    $newSubmission = new Submission("", $_SESSION['user']->getId(), $_GET['id'], $_POST['code'], $_POST['language'], "", "", $timestamp);
    $submissionCrud = new SubmissionCrud();
    $userCrud = new UserCrud();
    $result = $submissionCrud->create($newSubmission);

    if ($result['success'] == true && $result['id'] != -1) {
        $userCrud->incrementAttempts($_SESSION['user']->getId());
        //End the connection with the user, as the judge process can take some time.

        ob_end_clean();
        header("Connection: close");
        ignore_user_abort(true); // just to be safe
        ob_start();

        if ($_SESSION['user']->getType() >= 3) {
            $submissions = $submissionCrud->getForProblemId($problem['id']);
            //If mentor, get his and then the group's submission
        } else if ($_SESSION['user']->getType() == 2) {
            $submissions = $submissionCrud->getForGroupUserId($problem['id'], $_SESSION['user']->getId(), $_SESSION['user']->getGroupId());
            //If just a student, get his submissions only
        } else {
            $submissions = $submissionCrud->getForGroupUserId($problem['id'], $_SESSION['user']->getId());
        }
        $problem_posted = true;
        include("../view/problem.php");

        $size = ob_get_length();
        header("Content-Length: $size");
        ob_end_flush();
        ob_flush();
        flush();
        if (session_id()) session_write_close();

        $time = date('Y-m-d-H-i-s');
        $rand = rand();
        $outputFolder = $time . "_" . $_SESSION['user']->getId() . "_" . $_GET['id'] . "_" . $rand;
        
        $ROOT = "var/www/html";

        mkdir("/$ROOT/uploads/userCode/" . $outputFolder, 0770);

        $inputPath = "/".$ROOT."/uploads/userCode/" . $outputFolder . "/input.txt";
        $outputPath = "/".$ROOT."/uploads/userCode/" . $outputFolder . "/output.txt";
        $errorPath = "/".$ROOT."/uploads/userCode/" . $outputFolder . "/error.txt";
        $patternPath = "/".$ROOT."/uploads/userCode/" . $outputFolder . "/pattern.txt";

        $reportPath = "/".$ROOT."/reports/" . $outputFolder . ".txt";
        $memoryUsagePath = "/".$ROOT."/uploads/userCode/" . $outputFolder . "/memory.txt";

        $submissionUpdate = new Submission();
        $problemCrud = new ProblemCrud();
        $problem = $problemCrud->read($_GET['id']);

        if (empty($problem)) {
            return;
        }
        $problemCasesCrud = new ProblemCasesCrud();
        $problemCases = $problemCasesCrud->readForProblemId($_GET['id']);

        if (empty($problemCases)) {
            return;
        }
        $totalPoints = 0;

        foreach ($problemCases as $problemCase) {
            $totalPoints += intval($problemCase['points']);
        }
        $compiler = new Compiler();
        if (!$compiler->load($_POST['language'])) {
            return;
        }

        $report = [];
        $report["user_id"] = $_SESSION['user']->getId();
        $report["username"] = $_SESSION['user']->getUsername();
        $report["language"] = $compiler->getName();
        $report["compiler_version"] = $compiler->getVersion();
        $report["problem_id"] = $_GET['id'];
        $report["timestamp"] = $problem[0]['timestamp'];

        $timelimit = $problem[0][(string)$compiler->getTimeLimitRule()];
        $memorylimit = $problem[0][(string)$compiler->getMemoryLimitRule()];

        $compile_result = $compiler->compile($_POST['code'], $outputFolder);

        if ($compile_result['code'] != 0 || $compile_result['output'] != "") {
            $report["Compiler"] = "Error: " . $compile_result['output'];
            $submissionUpdate->setMessage("0/" . $totalPoints);
            $submissionUpdate->setVerdict($report["Compiler"]);
        } else {
            $report["Compiler"] = "OK";
            $points = 0;
            $judgeAvailable = false;
            if($problem[0]['judge_code']!="") {
                $judgeCompiler = new Compiler();
                if (!$judgeCompiler->load($problem[0]["judge_language"])) {
                    return;
                }
                $judge_timelimit = $problem[0][(string)$judgeCompiler->getTimeLimitRule()];

                mkdir("/".$ROOT."/uploads/userCode/" . $outputFolder . "/judge", 0770);
                $compile_result = $judgeCompiler->compile($problem[0]['judge_code'], $outputFolder . "/judge");

                if ($compile_result['code'] != 0 || $compile_result['output'] != "") {
                    $judgeAvailable = false;
                } else {
                    $judgeAvailable = true;
                }
            }
            $verdict = "";
            foreach ($problemCases as $case) {

                $handle = fopen($inputPath, 'w+');
                fwrite($handle, $case['input']);
                fclose($handle);
                chmod($inputPath, 0770);

                $runResult = $compiler->run($outputFolder, $timelimit, false);

                $currOutput = trim(file_get_contents($outputPath));
                $currError = file_get_contents($errorPath);
                $memory = trim(file_get_contents($memoryUsagePath));

                $caseReport["memory_usage"] = $memory;
                $caseReport["memory_limit"] = $memorylimit;

                $caseReport["input"] = $case['input'];
                $caseReport["output"] = $currOutput;
                $caseReport["pattern"] = $case['pattern'];

                $caseReport["error"] = $currError;

                if ($memory !== false && $memory > 0 && false) {
                    if ($memory > $memorylimit) {
                        $caseReport["verdict"] = "Memory Limit";
                    }
                } else {
                    if($judgeAvailable) {
                        $handle = fopen($patternPath, 'w+');
                        fwrite($handle, $case['pattern']);
                        fclose($handle);

                        rename($inputPath, "/".$ROOT."/uploads/userCode/" . $outputFolder . "/judge/input.txt");
                        rename($outputPath, "/".$ROOT."/uploads/userCode/" . $outputFolder . "/judge/output.txt");
                        rename($patternPath, "/".$ROOT."/uploads/userCode/" . $outputFolder . "/judge/pattern.txt");
                        chmod("/".$ROOT."/uploads/userCode/" . $outputFolder . "/judge/input.txt", 0770);
                        chmod("/".$ROOT."/uploads/userCode/" . $outputFolder . "/judge/output.txt", 0770);
                        chmod("/".$ROOT."/uploads/userCode/" . $outputFolder . "/judge/pattern.txt", 0770);
                        $judgeOutputPath = "/".$ROOT."/uploads/userCode/" . $outputFolder . "/judge/judge_output.txt";
                        $judgeResult = $judgeCompiler->run($outputFolder . "/judge", $judge_timelimit, true);
                        $judgeOutput = trim(file_get_contents($judgeOutputPath));
                        if ($judgeOutput === false || $judgeOutput == "") {
                            $judgeOutput = 0;
                        }
                    } else {
                        if(trim($case['pattern'])===trim($currOutput)) {
                            $judgeOutput = 1;
                        } else {
                            $judgeOutput = 0;
                        }
                    }
                    if ($judgeOutput == 1) {
                        $caseReport["verdict"] = "Accepted(" . $case['points'] . " points)";
                        $verdict .= ("Case " . $case['test_nr'] . ": Accepted(" . $case['points'] . " points)\n");
                        $points += $case['points'];
                    } else if ($runResult["success"] == "ok") {
                        $caseReport["verdict"] = "Wrong Answer(0 points)";
                        $verdict .= ("Case " . $case['test_nr'] . ": Wrong Answer(0 points)\n");
                    } else if ($runResult["success"] == "terminated") {
                        $caseReport["verdict"] = "Time Limit(0 points)";
                        $verdict .= ("Case " . $case['test_nr'] . ": Time Limit(0 points)\n");
                    }
                }
                $caseReport['test_nr'] = $case['test_nr'];
                $report['test_cases'][$case['test_nr']] = $caseReport;
                if($judgeAvailable) {

                    unlink("/" . $ROOT . "/uploads/userCode/" . $outputFolder . "/judge/input.txt");
                    unlink("/" . $ROOT . "/uploads/userCode/" . $outputFolder . "/judge/output.txt");
                    unlink("/" . $ROOT . "/uploads/userCode/" . $outputFolder . "/judge/pattern.txt");
                    unlink("/" . $ROOT . "/uploads/userCode/" . $outputFolder . "/judge/judge_output.txt");

                } else {
                    unlink("/" . $ROOT . "/uploads/userCode/" . $outputFolder . "/input.txt");
                    unlink("/" . $ROOT . "/uploads/userCode/" . $outputFolder . "/output.txt");
                }
            }

            $submissionUpdate->setMessage($points . "/" . $totalPoints);
            $submissionUpdate->setVerdict($verdict);

            if ($points == $totalPoints) {
                $userCrud->incrementAccepted($_SESSION['user']->getId());
            }
        }
        $report["final"] = $submissionUpdate->getMessage();
        $handle = fopen($reportPath, 'w+');
        fwrite($handle, json_encode($report));
        fclose($handle);

        $submissionUpdate->setReportPath($reportPath);
        $submissionCrud->update($submissionUpdate, $result['id']);
        $compiler->cleanUp($outputFolder);
        return;
    } else {
        $error = true;
        $message = $result['message'];
    }
} else {
    include("../view/problem.php");
}

function loadSubmissions(){

}