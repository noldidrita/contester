<?php
/**
 * Created by PhpStorm.
 * User: luba
 * Date: 21.1.2016
 * Time: 21:05
 */

include_once("../model/users/User.crud.php");
include_once("../model/users/User.class.php");
include_once("../model/groups/Group.class.php");
include_once("../model/groups/Group.crud.php");

include_once("util.php");

use model\users\Crud as UserCrud;
use model\users\User as User;
use model\groups\Crud as GroupCrud;
use model\groups\Group as Group;

session_start();
if (!isset($_SESSION['user']) || $_SESSION['user']->getType() < 3) {
    $host  = $_SERVER['HTTP_HOST'];
    $page = "home";
    header("Location: http://$host/$page");
    return;
}

$userCrud = new UserCrud();
$groupCrud = new GroupCrud();

if (isset($_POST['action']) && isset($_POST['ids'])) {
    if ($_POST['action'] == 'approve') {
        if (isset($_POST['ids']) && is_array($_POST['ids'])) {
            $user = new User();
            $user->setType(1);
            foreach ($_POST['ids'] as $id) {
                if (ctype_digit($id)) {
                    $userCrud->update($user, $id);
                    $user = $userCrud->read($id);
                    sendApprovalEmail($user[0]['email'], $user[0]['name'] . " " . $user[0]['surname'], $user['username']);
                }
            }
        }
    } else if ($_POST['action'] == 'delete') {
        if (isset($_POST['ids']) && is_array($_POST['ids'])) {
            foreach ($_POST['ids'] as $id) {
                if (ctype_digit($id)) {
                    $user = $userCrud->read($id);
                    if ($_SESSION['user']->getType() > $user['type'] || $_SESSION['user']->getType() == 4)
                        $userCrud->delete($id);
                }
            }
        }
    } else if ($_POST['action'] == 'set_group') {
        if (isset($_POST['ids']) && is_array($_POST['ids']) && isset($_POST['group_id']) && ctype_digit($_POST['group_id'])) {
            $user = new User();
            $user->setGroupId($_POST['group_id']);

            foreach ($_POST['ids'] as $id) {
                if (ctype_digit($id)) {
                    $userCrud->update($user, $id);
                }
            }
        }
    } else if ($_POST['action'] == 'set_type') {
        if (isset($_POST['ids']) && is_array($_POST['ids']) && isset($_POST['type']) && $_POST['type'] > 0 && $_POST['type'] < 5) {
            $update = new User();
            $update->setType($_POST['type']);
            foreach ($_POST['ids'] as $id) {
                if (ctype_digit($id)) {
                    $user = $userCrud->read($id);
                    if ($_SESSION['user']->getType() > $user[0]['type'] || $_SESSION['user']->getType() == 4)
                        $userCrud->update($update, $id);
                }
            }
        }
    }
} else {
    $users = $userCrud->read();
    include("../view/users.php");
}

function sendApprovalEmail($to, $name, $username)
{
    $from = "noldidrita@gmail.com";
    $subject = "SAT Approval email";
    $eol = PHP_EOL;

    $message = "<h3>Dear ".$name.",</h3><p>Your account has just been approved. Click <a href='http://stud-proj.epoka.edu.al/~adrita13/web16_adrita13/contester'>here</a> to log in with your username '".$username."' and the password you've provided.</p><p>We hope to see you work your way to the top of the list,</p><br/><p>SAT Administration Board.</p>";

// Always set content-type when sending HTML email
    $headers = "MIME-Version: 1.0" . $eol;
    $headers .= "Content-type:text/html;charset=UTF-8" . $eol;
    $headers .= 'From: '.$from.$eol;

    mail($to, $subject, $message, $headers);
}

function sendDeletionEmail($to, $name, $username)
{
    $from = "noldidrita@gmail.com";
    $subject = "SAT Approval email";
    $eol = PHP_EOL;

    $message = "<h3>Dear ".$name.",</h3><p>Your account has just been removed from our site. We're sorry for this,<p>SAT Administration Board.</p>";

// Always set content-type when sending HTML email
    $headers = "MIME-Version: 1.0" . $eol;
    $headers .= "Content-type:text/html;charset=UTF-8" . $eol;
    $headers .= 'From: '.$from.$eol;

    mail($to, $subject, $message, $headers);
}