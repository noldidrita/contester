-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 01, 2016 at 09:58 AM
-- Server version: 5.5.49-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `contester`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `text` text,
  `problem_id` int(11) DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_problem_id_idx` (`problem_id`),
  KEY `fk_user_id_idx` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

-- --------------------------------------------------------

--
-- Table structure for table `contests`
--

CREATE TABLE IF NOT EXISTS `contests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `freeze_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `contests`
--

INSERT INTO `contests` (`id`, `title`, `start_time`, `end_time`, `description`, `freeze_time`) VALUES
  (1, 'No Contest', '2016-01-22 11:00:00', '2016-01-22 23:00:00', 'Problems that do no appear in any contest', NULL);
-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`, `timestamp`) VALUES
  (1, 'Not Assigned', 'No group is assigned to these users', '2016-01-19 00:00:00');
-- --------------------------------------------------------

--
-- Table structure for table `homeworks`
--

CREATE TABLE IF NOT EXISTS `homeworks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `language` varchar(45) DEFAULT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `homeworks`
--

INSERT INTO `homeworks` (`id`, `title`, `start_time`, `end_time`, `description`, `language`, `group_id`) VALUES
  (1, 'No Homework', '2016-01-22 00:00:00', '2016-01-25 00:00:00', 'Problems that do no appear in any homework', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `problems`
--

CREATE TABLE IF NOT EXISTS `problems` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) DEFAULT NULL,
  `subtitle` varchar(200) DEFAULT NULL,
  `statement` text,
  `time_limit_1` int(11) DEFAULT NULL,
  `time_limit_2` int(11) DEFAULT NULL,
  `time_limit_3` int(11) DEFAULT NULL,
  `volume_id` int(11) DEFAULT NULL,
  `section_id` int(11) DEFAULT NULL,
  `memory_limit_1` int(11) DEFAULT NULL,
  `memory_limit_2` int(11) DEFAULT NULL,
  `memory_limit_3` int(11) DEFAULT NULL,
  `difficulty` int(11) DEFAULT NULL,
  `shortdescription` varchar(200) DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  `judge_code` text,
  `judge_language` varchar(45) DEFAULT NULL,
  `contest_id` int(11) DEFAULT NULL,
  `homework_id` int(11) DEFAULT NULL,
  `ready` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_volume_id_idx` (`volume_id`),
  KEY `fk_section_id_idx` (`section_id`),
  KEY `fk_homework_id_idx` (`homework_id`),
  KEY `fk_contest_id_idx` (`contest_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `problems`
--

INSERT INTO `problems` (`id`, `title`, `subtitle`, `statement`, `time_limit_1`, `time_limit_2`, `time_limit_3`, `volume_id`, `section_id`, `memory_limit_1`, `memory_limit_2`, `memory_limit_3`, `difficulty`, `shortdescription`, `timestamp`, `judge_code`, `judge_language`, `contest_id`, `homework_id`, `ready`) VALUES
  (6, 'A+B', 'Author: Arnold', 'You are given two integers A and B. <br />\r\nCalculate their sum. <br />\r\n\r\n<b>Input</b> <br />\r\nInput contains A and B (0 < A, B < 10001 ). <br />\r\n<br />\r\n<b>Output</b><br />\r\nShow the sum of the given numbers.\r\n<br />\r\n<div id="myDiv"> </div> <br />\r\n<table class="xtable" cellspacing="0" border="0"><tr>\r\n<td class="xtd">\r\n<b>Input</b>\r\n</td></tr><tr>\r\n<td class="xtdcode">\r\n5<br />\r\n3<br />\r\n</td></tr><tr>\r\n<td class="xtd">\r\n<b>Output</b>\r\n</td></tr><tr>\r\n<td class="xtdcode">\r\n\r\n8<br />\r\n</td></tr></table>\r\n<br />', 2, 4, 4, 1, 1, 65000, 65000, 65000, 1, 'Find A+B', '2016-01-25 18:00:36', 'import java.util.Scanner;\r\nimport java.io.PrintWriter;\r\nimport java.io.File;\r\nimport java.io.FileNotFoundException;\r\n\r\npublic class Solver {\r\n     public static void main(String[] args) throws FileNotFoundException {\r\n      Scanner input = new Scanner(new File("input.txt"));\r\n      Scanner output = new Scanner(new File("output.txt"));\r\n       Scanner pattern = new Scanner(new File("pattern.txt"));\r\n\r\n    int a = input.nextInt();\r\n       int b= input.nextInt();\r\n     int s = output.nextInt();\r\n     PrintWriter writer;\r\n     try{\r\n      writer = new PrintWriter("judge_output.txt");\r\n           if(a+b==s)\r\n        writer.println(1);\r\n    else writer.println(0);\r\n    writer.close();\r\n\r\n     } catch(FileNotFoundException e) {\r\n         \r\n     }\r\n}\r\n}', 'java', 1, 1, 1),
  (7, 'Convert inches to cm', 'Taken from ACM', '<p>Write a program that convert inches to centimeters.  One inch equals 2.54 centimeters.\r\n</p>\r\n\r\n<p><strong>Input specification </strong> <br />\r\nYou will be given the a floating point number (inch) where 0 ? inch  ? 50000 </p>\r\n\r\n<p><strong>Output specification </strong> <br />\r\nShow the centimeter equavalent of the given number.\r\n</p><p>\r\n\r\n<table border=1>\r\n<tr>\r\n<td><strong>Sample Input I</strong>  <br />\r\n  4 <br /> </td>\r\n\r\n<td><strong>Sample Input II</strong>  <br />\r\n  5.2 <br /> </td></tr>\r\n\r\n<tr>\r\n<td><strong>Sample Output I</strong> <br />\r\n  10.16</td>\r\n\r\n<td><strong>Sample Output II</strong> <br />\r\n  13.208</td>\r\n\r\n</tr> </table>\r\n</p>\r\n<br />\r\n', 2, 4, 4, 1, 1, 65000, 65000, 65000, 1, 'Convert inches to cm', '2016-01-25 20:55:35', '', 'c', 1, 1, 1),
  (8, 'Car Parking', 'Car parking', '<center>\r\n<h1> <img src="/en/figureimageru?pid=c4b7" border="0" \r\nheight="180px" align="right" />\r\nCar Parking </h1></center>\r\n\r\n<p>\r\nArbeni parking garage charges a 150 Lek minimum fee to\r\npark up to four hours. The garage charges an additional\r\n50 Lek per hour for each hour or part thereof in excess\r\nof four hours. The maximum charge for any given 24-hour\r\nperiod is 800 lek. Assume that no car parks for longer\r\nthan 24 hours at a time.\r\n</p><p><strong>Question: </strong> Write a program\r\nthat will calculate and print the total parking \r\ncharge for three customers.\r\n\r\n<p><strong>Input specification </strong> <br />\r\nYou will be given parking time for three cars; where\r\nthe parking time is an integer between 1 and 24.\r\n</p><p>\r\n<img src="/en/figureimageru?n=2&pid=c4b7" border="0" \r\nheight="200px" align="right" />\r\n<strong>Output specification </strong> <br />\r\nShow the total amount collected from three cars'' parking.\r\n\r\n<table border=1>\r\n<tr valign="top">\r\n<td><strong> Sample Input I   </strong></td>\r\n<td><strong> Sample Input II   </strong></td> </tr>\r\n<tr  valign="top">\r\n<td><code> 5 3 6 <br /> </code></td>\r\n<td><code> 15 3 23 <br /> </code></td>\r\n</tr>\r\n<tr  valign="top">\r\n<td><strong> Sample Output I   </strong></td>\r\n<td><strong> Sample Output II   </strong></td>\r\n</tr>\r\n<tr  valign="top">\r\n<td><code> 600<br /> </code></td>\r\n<td><code> 1650<br /> </code></td>\r\n</tr>\r\n</table>\r\n</p>\r\n\r\n\r\n<div class="datadiv"></div>\r\n', 1, 1, 1, 1, 1, 1000, 65000, 65000, 3, 'Calculate the total parking price for three cars', '2016-01-25 21:15:23', '', 'c', 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `problem_cases`
--

CREATE TABLE IF NOT EXISTS `problem_cases` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `problem_id` int(11) DEFAULT NULL,
  `test_nr` int(11) DEFAULT NULL,
  `input` text,
  `pattern` text,
  `points` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_problem_id_idx` (`problem_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `problem_cases`
--

INSERT INTO `problem_cases` (`id`, `problem_id`, `test_nr`, `input`, `pattern`, `points`) VALUES
  (11, 6, 1, '1 2', '', 1),
  (12, 6, 2, '3 4', '', 1),
  (13, 6, 3, '15 20', '', 1),
  (15, 6, 4, '1000 1000', '', 1),
  (16, 7, 1, '5', '12.7', 1),
  (17, 7, 2, '3000', '7620', 1),
  (18, 7, 3, '12345.3', '31357.062', 1),
  (19, 7, 4, '49802', '126497.08\r\n', 1),
  (20, 8, 1, '9 7 3\r\n', '850\r\n', 1),
  (21, 8, 2, '20 12 3\r\n', '1500\r\n', 1),
  (22, 8, 3, '20 15 14 \r\n', '2150\r\n', 1),
  (23, 6, 5, '3 3', '', 1),
  (24, 6, 6, '6 6', '', 1),
  (25, 6, 7, '20 50', '', 1),
  (26, 6, 8, '100 30', '', 1),
  (27, 7, 5, '5', '12.7', 1),
  (28, 6, 9, '5 5', '10', 10),
  (29, 6, 10, '5 7', '', 5);

-- --------------------------------------------------------

--
-- Table structure for table `sections`
--

CREATE TABLE IF NOT EXISTS `sections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `sections`
--

INSERT INTO `sections` (`id`, `name`, `description`, `timestamp`) VALUES
  (1, 'No Section', 'Problems that do not appear in any section', '2016-01-19 00:00:00');
-- --------------------------------------------------------

--
-- Table structure for table `submissions`
--

CREATE TABLE IF NOT EXISTS `submissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `problem_id` int(11) DEFAULT NULL,
  `code` text,
  `language` varchar(45) DEFAULT NULL,
  `verdict` text,
  `message` text,
  `timestamp` datetime DEFAULT NULL,
  `report_path` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_problem_id_idx` (`problem_id`),
  KEY `fk_user_id_idx` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=62;

--
-- Dumping data for table `submissions`
--

INSERT INTO `submissions` (`id`, `user_id`, `problem_id`, `code`, `language`, `verdict`, `message`, `timestamp`, `report_path`) VALUES
  (1, 1, 6, 'import java.util.*;\n\nclass Solver {\n  	public static void main(String[] args) {\n	Scanner sc = new Scanner(System.in);\n	int a = sc.nextInt();\n	int b = sc.nextInt();\n	System.out.print(a+b);  \n    }\n}                                                                             ', 'java', 'Case 0: Accepted(1 points)\nCase 1: Accepted(1 points)\nCase 2: Accepted(1 points)\nCase 4: Accepted(1 points)\n', '4/4', '2016-01-25 19:39:27', '2016-01-25-19-39-27_12_6_510351966.txt'),
  (2, 1, 6, 'import java.util.*;\r\n\r\nclass Solver {\r\n  	public static void main(String[] args) {\r\n	Scanner sc = new Scanner(System.in);\r\n	int a = sc.nextInt();\r\n	int b = sc.nextInt();\r\n	System.out.print(a+b);  \r\n    }\r\n}                                                                             ', 'java', 'Case 0: Accepted(1 points)\nCase 1: Accepted(1 points)\nCase 2: Accepted(1 points)\nCase 4: Accepted(1 points)\n', '4/4', '2016-01-25 19:40:15', '/home/adrita13/public_html/web16_adrita13/reports/2016-01-25-19-40-15_12_6_132554387.txt'),
  (3, 1, 6, 'import java.util.*;\r\n\r\nclass Solver {\r\n  	public static void main(String[] args) {\r\n	Scanner sc = new Scanner(System.in);\r\n	int a = sc.nextInt();\r\n	int b = sc.nextInt();\r\n	System.out.print(a+b);  \r\n    }\r\n}                                             ', 'java', 'Case 0: Accepted(1 points)\nCase 1: Accepted(1 points)\nCase 2: Accepted(1 points)\nCase 4: Accepted(1 points)\n', '4/4', '2016-01-25 19:44:59', '/home/adrita13/public_html/web16_adrita13/reports/2016-01-25-19-44-59_12_6_918353300.txt'),
  (4, 1, 6, 'import java.util.*;\r\n\r\nclass Solver {\r\n  	public static void main(String[] args) {\r\n	Scanner sc = new Scanner(System.in);\r\n	int a = sc.nextInt();\r\n	int b = sc.nextInt();\r\n	System.out.print(a+b);  \r\n    }\r\n}                ', 'java', 'Case 0: Accepted(1 points)\nCase 1: Accepted(1 points)\nCase 2: Accepted(1 points)\nCase 4: Accepted(1 points)\n', '4/4', '2016-01-25 19:49:53', '/home/adrita13/public_html/web16_adrita13/reports/2016-01-25-19-49-53_12_6_141239542.txt'),
  (5, 1, 6, 'import java.util.*;\r\n\r\nclass Solver {\r\n  	public static void main(String[] args) {\r\n	Scanner sc = new Scanner(System.in);\r\n	int a = sc.nextInt();\r\n	int b = sc.nextInt();\r\n	System.out.print(a+b);  \r\n    }\r\n}                    ', 'java', 'Case 0: Accepted(1 points)\nCase 1: Accepted(1 points)\nCase 2: Accepted(1 points)\nCase 4: Accepted(1 points)\n', '4/4', '2016-01-25 19:50:54', '/home/adrita13/public_html/web16_adrita13/reports/2016-01-25-19-50-54_12_6_1592522391.txt'),
  (6, 1, 6, 'import java.util.*;\r\n\r\nclass Solver {\r\n  	public static void main(String[] args) {\r\n	Scanner sc = new Scanner(System.in);\r\n	int a = sc.nextInt();\r\n	int b = sc.nextInt();\r\n	System.out.print(a+b);  \r\n    }\r\n}                    ', 'java', 'Case 0: Accepted(1 points)\nCase 1: Accepted(1 points)\nCase 2: Accepted(1 points)\nCase 4: Accepted(1 points)\n', '4/4', '2016-01-25 19:52:34', '/home/adrita13/public_html/web16_adrita13/reports/2016-01-25-19-52-35_12_6_47961217.txt'),
  (7, 1, 6, 'int i;\r\ncin>>i;', 'java', '', '', '2016-01-25 19:55:57', ''),
  (8, 1, 6, 'int i;\r\ncin??i;', 'java', 'Error: Solver.java:1: error: class, interface, or enum expectedint i;^Solver.java:2: error: class, interface, or enum expectedcin??i;^2 errors', '0/4', '2016-01-25 19:56:33', '/home/adrita13/public_html/web16_adrita13/reports/2016-01-25-19-56-33_12_6_1127379779.txt'),
  (9, 1, 6, 'import java.util.*;\r\n\r\nclass Solver {\r\n  	public static void main(String[] args) {\r\n	Scanner sc = new Scanner(System.in);\r\n	int a = sc.nextInt();\r\n	int b = sc.nextInt();\r\n	System.out.print(a+b);  \r\n    }\r\n}     ', 'java', 'Case 0: Accepted(1 points)\nCase 1: Accepted(1 points)\nCase 2: Accepted(1 points)\nCase 4: Accepted(1 points)\n', '4/4', '2016-01-25 19:57:08', '/home/adrita13/public_html/web16_adrita13/reports/2016-01-25-19-57-08_12_6_91718179.txt'),
  (10, 1, 6, '#include<stdio.h>\r\n\r\nint main(){\r\n int a;\r\n  int b;\r\n  scanf("%d%d", &a, &b);\r\n  printf("%d"a+b);\r\n}', 'c', 'Error: main.c: In function ''main'':main.c:7:14: error: expected '')'' before ''a''   printf("%d"a+b);              ^main.c:7:14: warning: format ''%d'' expects a matching ''int'' argument [-Wformat=]', '0/4', '2016-01-25 20:11:45', '/home/adrita13/public_html/web16_adrita13/reports/2016-01-25-20-11-45_12_6_1668565316.txt'),
  (11, 1, 6, '#include<stdio.h>\r\n\r\n  int main(){\r\n  int a;\r\n  int b;\r\n  scanf("%d %d", &a, &b);\r\n  printf("%d"a+b);\r\n    return 0;\r\n}', 'c', 'Error: main.c: In function ''main'':main.c:7:14: error: expected '')'' before ''a''   printf("%d"a+b);              ^main.c:7:14: warning: format ''%d'' expects a matching ''int'' argument [-Wformat=]', '0/4', '2016-01-25 20:13:10', '/home/adrita13/public_html/web16_adrita13/reports/2016-01-25-20-13-10_12_6_1414301654.txt'),
  (12, 1, 6, '#include <stdio.h>\r\n\r\nint main(){\r\n  int a;\r\n  int b;\r\n  scanf("%d", &a);\r\n  scanf("%d", &b);\r\n  printf("%d"a+b);\r\n    return 0;\r\n}', 'c', 'Error: main.c: In function ''main'':main.c:8:14: error: expected '')'' before ''a''   printf("%d"a+b);              ^main.c:8:14: warning: format ''%d'' expects a matching ''int'' argument [-Wformat=]', '0/4', '2016-01-25 20:15:11', '/home/adrita13/public_html/web16_adrita13/reports/2016-01-25-20-15-11_12_6_2072511656.txt'),
  (13, 1, 6, '#include <stdio.h>\r\n\r\nvoid main(void){\r\n  int a;\r\n  int b;\r\n  scanf("%d", &a);\r\n  scanf("%d", &b);\r\n  printf("%d"a+b);\r\n}', 'c', 'Error: main.c: In function ''main'':main.c:8:14: error: expected '')'' before ''a''   printf("%d"a+b);              ^main.c:8:14: warning: format ''%d'' expects a matching ''int'' argument [-Wformat=]', '0/4', '2016-01-25 20:15:41', '/home/adrita13/public_html/web16_adrita13/reports/2016-01-25-20-15-41_12_6_838941767.txt'),
  (14, 1, 6, '#include <stdio.h>\r\n\r\nint main(void){\r\n  int a;\r\n  int b;\r\n  scanf("%d", &a);\r\n  scanf("%d", &b);\r\n  printf("%d",a+b);\r\n  return 0;\r\n}', 'c', 'Case 1: Accepted(1 points)\nCase 2: Time Limit(0 points)\nCase 3: Time Limit(0 points)\nCase 4: Time Limit(0 points)\n', '1/4', '2016-01-25 20:17:36', '/home/adrita13/public_html/web16_adrita13/reports/2016-01-25-20-17-36_12_6_141884591.txt'),
  (15, 1, 6, '#include <stdio.h>\r\n\r\nint main(void){\r\n  int a;\r\n  int b;\r\n  scanf("%d", &a);\r\n  scanf("%d", &b);\r\n  printf("%d",a+b);\r\n  return 0;\r\n}', 'c', 'Case 1: Accepted(1 points)\nCase 2: Accepted(1 points)\nCase 3: Accepted(1 points)\nCase 4: Accepted(1 points)\n', '4/4', '2016-01-25 20:18:48', '/home/adrita13/public_html/web16_adrita13/reports/2016-01-25-20-18-48_12_6_255815467.txt'),
  (16, 1, 6, 'a = int(raw_input())\r\nb = int(raw_input())\r\nprint a+b', 'python', 'Case 1: Wrong Answer(0 points)\nCase 2: Wrong Answer(0 points)\nCase 3: Wrong Answer(0 points)\nCase 4: Wrong Answer(0 points)\n', '0/4', '2016-01-25 20:22:30', '/home/adrita13/public_html/web16_adrita13/reports/2016-01-25-20-22-30_12_6_1325945422.txt'),
  (17, 1, 6, 'a = int(raw_input())\r\nb = int(raw_input())\r\nprint a+b\r\nexit(0)', 'python', 'Case 1: Wrong Answer(0 points)\nCase 2: Wrong Answer(0 points)\nCase 3: Wrong Answer(0 points)\nCase 4: Wrong Answer(0 points)\n', '0/4', '2016-01-25 20:25:25', '/home/adrita13/public_html/web16_adrita13/reports/2016-01-25-20-25-26_12_6_1886276962.txt'),
  (18, 1, 6, 'a, b = raw_input().split()\r\nprint int(a)+int(b)\r\nexit(0)', 'c', 'Error: main.c:1:1: warning: data definition has no type or storage class [enabled by default] a, b = raw_input().split() ^main.c:1:19: error: request for member ''split'' in something not a structure or union a, b = raw_input().split()                   ^main.c:2:1: error: expected '','' or '';'' before ''print'' print int(a)+int(b) ^', '0/4', '2016-01-25 20:28:08', '/home/adrita13/public_html/web16_adrita13/reports/2016-01-25-20-28-08_12_6_1360955740.txt'),
  (19, 1, 6, 'a, b = raw_input().split()\r\nprint int(a)+int(b)\r\nexit(0)', 'python', 'Case 1: Accepted(1 points)\nCase 2: Accepted(1 points)\nCase 3: Accepted(1 points)\nCase 4: Accepted(1 points)\n', '4/4', '2016-01-25 20:28:18', '/home/adrita13/public_html/web16_adrita13/reports/2016-01-25-20-28-18_12_6_888669363.txt'),
  (20, 1, 6, 'import java.util.Scanner;\r\n\r\npublic class solver {\r\n\r\npublic static void main(String[] args) {\r\nScanner input = new Scanner (System.in);\r\nint a, b, sum=0;\r\n\r\na= input.nextInt();\r\nb= input.nextInt();\r\nsum=a+b;\r\n\r\nSystem.out.println(sum);\r\n}\r\n}', 'java', 'Error: Solver.java:3: error: class solver is public, should be declared in a file named solver.javapublic class solver {       ^1 error', '0/4', '2016-01-25 20:48:16', '/home/adrita13/public_html/web16_adrita13/reports/2016-01-25-20-48-17_15_6_1680749336.txt'),
  (21, 1, 6, 'import java.util.Scanner;\r\n\r\npublic class Solver {\r\n\r\npublic static void main(String[] args) {\r\nScanner input = new Scanner (System.in);\r\nint a, b, sum=0;\r\n\r\na= input.nextInt();\r\nb= input.nextInt();\r\nsum=a+b;\r\n\r\nSystem.out.println(sum);\r\n}\r\n}', 'java', 'Case 1: Accepted(1 points)\nCase 2: Accepted(1 points)\nCase 3: Accepted(1 points)\nCase 4: Accepted(1 points)\n', '4/4', '2016-01-25 20:49:10', '/home/adrita13/public_html/web16_adrita13/reports/2016-01-25-20-49-10_15_6_246718582.txt'),
  (22, 1, 7, 'a = float(input())\r\n\r\nprint (a*2.54)\r\n', 'python', 'Case 0: Accepted(1 points)\nCase 1: Wrong Answer(0 points)\nCase 2: Accepted(1 points)\nCase 3: Wrong Answer(0 points)\n', '2/4', '2016-01-25 20:56:30', '/home/adrita13/public_html/web16_adrita13/reports/2016-01-25-20-56-31_12_7_1255129477.txt'),
  (23, 1, 8, '\r\nimport java.util.Scanner;\r\nimport java.math.*;\r\n\r\npublic class solver \r\n{\r\n  public static void main(String args[])\r\n     {\r\n    \r\n     int total = 0;\r\n      Scanner in = new Scanner(System.in);  \r\n      \r\n      int a;\r\n      \r\n      for (int i=1; i<=3; i++){\r\n        \r\n      int sum = 0;\r\n     \r\n      a =  in.nextInt(); \r\n        \r\n        if (a<4){\r\n          \r\n          total += 150;\r\n          \r\n        }\r\n        \r\n        else if (a>4){\r\n          \r\n          sum = 150 + (a-4)*50;\r\n          \r\n        \r\n          if (sum>800){\r\n            total = total+800;\r\n          }\r\n          else total+=sum;\r\n        }\r\n        \r\n        \r\n        \r\n        \r\n      }\r\n\r\n      System.out.println(total);\r\n        \r\n     }\r\n  }', 'java', 'Error: Solver.java:5: error: class solver is public, should be declared in a file named solver.javapublic class solver       ^1 error', '0/3', '2016-01-25 21:18:19', '/home/adrita13/public_html/web16_adrita13/reports/2016-01-25-21-18-19_15_8_619035403.txt'),
  (24, 1, 8, 'import java.util.Scanner;\r\nimport java.math.*;\r\n\r\npublic class Solver \r\n{\r\n  public static void main(String args[])\r\n     {\r\n    \r\n     int total = 0;\r\n      Scanner in = new Scanner(System.in);  \r\n      \r\n      int a;\r\n      \r\n      for (int i=1; i<=3; i++){\r\n        \r\n      int sum = 0;\r\n     \r\n      a =  in.nextInt(); \r\n        \r\n        if (a<4){\r\n          \r\n          total += 150;\r\n          \r\n        }\r\n        \r\n        else if (a>4){\r\n          \r\n          sum = 150 + (a-4)*50;\r\n          \r\n        \r\n          if (sum>800){\r\n            total = total+800;\r\n          }\r\n          else total+=sum;\r\n        }\r\n        \r\n        \r\n        \r\n        \r\n      }\r\n\r\n      System.out.println(total);\r\n        \r\n     }\r\n  }', 'java', '', '', '2016-01-25 21:18:50', ''),
  (25, 1, 8, 'import java.util.Scanner;\r\nimport java.math.*;\r\n\r\npublic class Solver \r\n{\r\n  public static void main(String args[])\r\n     {\r\n    \r\n     int total = 0;\r\n      Scanner in = new Scanner(System.in);  \r\n      \r\n      int a;\r\n      \r\n      for (int i=1; i<=3; i++){\r\n        \r\n      int sum = 0;\r\n     \r\n      a =  in.nextInt(); \r\n        \r\n        if (a<4){\r\n          \r\n          total += 150;\r\n          \r\n        }\r\n        \r\n        else if (a>4){\r\n          \r\n          sum = 150 + (a-4)*50;\r\n          \r\n        \r\n          if (sum>800){\r\n            total = total+800;\r\n          }\r\n          else total+=sum;\r\n        }\r\n        \r\n        \r\n        \r\n        \r\n      }\r\n\r\n      System.out.println(total);\r\n        \r\n     }\r\n  }', 'java', '', '', '2016-01-25 21:25:30', ''),
  (26, 1, 8, 'import java.util.Scanner;\r\nimport java.math.*;\r\n\r\npublic class Solver \r\n{\r\n  public static void main(String args[])\r\n     {\r\n    \r\n     int total = 0;\r\n      Scanner in = new Scanner(System.in);  \r\n      \r\n      int a;\r\n      \r\n      for (int i=1; i<=3; i++){\r\n        \r\n      int sum = 0;\r\n     \r\n      a =  in.nextInt(); \r\n        \r\n        if (a<4){\r\n          \r\n          total += 150;\r\n          \r\n        }\r\n        \r\n        else if (a>4){\r\n          \r\n          sum = 150 + (a-4)*50;\r\n          \r\n        \r\n          if (sum>800){\r\n            total = total+800;\r\n          }\r\n          else total+=sum;\r\n        }\r\n        \r\n        \r\n        \r\n        \r\n      }\r\n\r\n      System.out.println(total);\r\n        \r\n     }\r\n  }', 'java', 'Case 0: Wrong Answer(0 points)\nCase 1: Wrong Answer(0 points)\nCase 2: Wrong Answer(0 points)\n', '0/3', '2016-01-25 21:26:00', '/home/adrita13/public_html/web16_adrita13/reports/2016-01-25-21-26-00_15_8_1916547470.txt'),
  (27, 1, 8, 'import java.util.Scanner;\r\nimport java.math.*;\r\n\r\npublic class Solver \r\n{\r\n  public static void main(String args[])\r\n     {\r\n    \r\n     int total = 0;\r\n      Scanner in = new Scanner(System.in);  \r\n      \r\n      int a;\r\n      \r\n      for (int i=1; i<=3; i++){\r\n        \r\n      int sum = 0;\r\n     \r\n      a =  in.nextInt(); \r\n        \r\n        if (a<4){\r\n          \r\n          total += 150;\r\n          \r\n        }\r\n        \r\n        else if (a>4){\r\n          \r\n          sum = 150 + (a-4)*50;\r\n          \r\n        \r\n          if (sum>800){\r\n            total = total+800;\r\n          }\r\n          else total+=sum;\r\n        }\r\n        \r\n        \r\n        \r\n        \r\n      }\r\n\r\n      System.out.println(total);\r\n        \r\n     }\r\n  }', 'java', 'Case 0: Accepted(1 points)\nCase 1: Accepted(1 points)\nCase 2: Accepted(1 points)\n', '3/3', '2016-01-25 21:27:30', '/home/adrita13/public_html/web16_adrita13/reports/2016-01-25-21-27-30_15_8_381972669.txt'),
  (28, 1, 6, 'a, b = raw_input().split()\r\nprint int(a)+int(b)\r\nexit(0)', 'python', 'Case 1: Accepted(1 points)\nCase 2: Accepted(1 points)\nCase 3: Accepted(1 points)\nCase 4: Accepted(1 points)\n', '4/4', '2016-01-26 00:31:46', '/home/adrita13/public_html/web16_adrita13/reports/2016-01-26-00-31-46_12_6_1045644229.txt'),
  (29, 1, 6, 'a, b = raw_input().split()\r\nprint int(a)+int(b)\r\nexit(0)', 'python', 'Case 1: Accepted(1 points)\nCase 2: Accepted(1 points)\nCase 3: Accepted(1 points)\nCase 4: Accepted(1 points)\n', '4/8', '2016-01-26 00:33:35', '/home/adrita13/public_html/web16_adrita13/reports/2016-01-26-00-33-35_12_6_1501174485.txt'),
  (30, 1, 6, 'a, b = raw_input().split()\r\nprint int(a)+int(b)\r\nexit(0)', 'java', 'Error: Solver.java:1: error: class, interface, or enum expecteda, b = raw_input().split()^1 error', '0/8', '2016-01-26 00:36:29', '/home/adrita13/public_html/web16_adrita13/reports/2016-01-26-00-36-29_12_6_1107552546.txt'),
  (31, 1, 6, 'a, b = raw_input().split()\r\nprint int(a)+int(b)\r\nexit(0)', 'python', '', '0/8', '2016-01-26 00:36:43', '/home/adrita13/public_html/web16_adrita13/reports/2016-01-26-00-36-43_12_6_1700423052.txt'),
  (32, 1, 6, 'import java.util.Scanner;\r\n\r\npublic class Solver {\r\n\r\npublic static void main(String[] args) {\r\nScanner input = new Scanner (System.in);\r\nint a, b, sum=0;\r\n\r\na= input.nextInt();\r\nb= input.nextInt();\r\nsum=a+b;\r\n\r\nSystem.out.println(sum);\r\n}\r\n}', 'java', 'Case 1: Accepted(1 points)\nCase 2: Accepted(1 points)\nCase 3: Accepted(1 points)\nCase 4: Accepted(1 points)\nCase 0: Accepted(1 points)\nCase 1: Accepted(1 points)\nCase 2: Accepted(1 points)\nCase 3: Accepted(1 points)\n', '8/8', '2016-01-26 00:38:00', '/home/adrita13/public_html/web16_adrita13/reports/2016-01-26-00-38-00_12_6_550422398.txt'),
  (33, 1, 7, '', 'python', 'Case 0: Wrong Answer(0 points)\nCase 1: Wrong Answer(0 points)\nCase 2: Wrong Answer(0 points)\nCase 3: Wrong Answer(0 points)\n', '0/4', '2016-01-26 16:35:09', '/home/adrita13/public_html/web16_adrita13/reports/2016-01-26-16-35-09_15_7_727034532.txt'),
  (34, 1, 7, 'a = float(input())\r\n\r\nprint (a*2.54)', 'python', 'Case 0: Accepted(1 points)\nCase 1: Wrong Answer(0 points)\nCase 2: Accepted(1 points)\nCase 3: Accepted(1 points)\n', '3/4', '2016-01-26 21:00:38', '/home/adrita13/public_html/web16_adrita13/reports/2016-01-26-21-00-38_12_7_88283507.txt'),
  (35, 1, 7, 'a = float(input())\r\n\r\nprint (a*2.54)', 'python', 'Case 0: Accepted(1 points)\nCase 1: Wrong Answer(0 points)\nCase 2: Accepted(1 points)\nCase 3: Accepted(1 points)\n', '3/4', '2016-01-26 21:04:39', '/home/adrita13/public_html/web16_adrita13/reports/2016-01-26-21-04-39_12_7_238700540.txt'),
  (36, 1, 7, 'a = float(input())\r\n\r\nprint (a*2.54)', 'python', 'Case 0: Accepted(1 points)\nCase 1: Wrong Answer(0 points)\nCase 2: Accepted(1 points)\nCase 3: Accepted(1 points)\n', '3/4', '2016-01-26 21:10:13', '/home/adrita13/public_html/web16_adrita13/reports/2016-01-26-21-10-13_12_7_994414830.txt'),
  (37, 1, 7, 'a = float(input())\r\n\r\nprint (a*2.54)', 'python', 'Case 0: Accepted(1 points)\nCase 1: Wrong Answer(0 points)\nCase 2: Accepted(1 points)\nCase 3: Accepted(1 points)\n', '3/4', '2016-01-26 21:12:27', '/home/adrita13/public_html/web16_adrita13/reports/2016-01-26-21-12-28_12_7_1918675220.txt'),
  (38, 1, 6, 'import java.util.Scanner;\r\n\r\npublic class Solver {\r\n\r\npublic static void main(String[] args) {\r\nScanner input = new Scanner (System.in);\r\nint a, b, sum=0;\r\n\r\na= input.nextInt();\r\nb= input.nextInt();\r\nsum=a+b;\r\n\r\nSystem.out.println(sum);\r\n}\r\n}', 'java', 'Case 1: Accepted(1 points)\nCase 2: Accepted(1 points)\nCase 3: Accepted(1 points)\nCase 4: Accepted(1 points)\nCase 0: Accepted(1 points)\nCase 1: Accepted(1 points)\nCase 2: Accepted(1 points)\nCase 3: Accepted(1 points)\n', '8/8', '2016-01-26 21:19:40', '/home/adrita13/public_html/web16_adrita13/reports/2016-01-26-21-19-40_12_6_1509570359.txt'),
  (39, 1, 6, 'import java.util.Scanner;\r\n\r\npublic class Solver {\r\n\r\npublic static void main(String[] args) {\r\nScanner input = new Scanner (System.in);\r\nint a, b, sum=0;\r\n\r\na= input.nextInt();\r\nb= input.nextInt();\r\nsum=a+b;\r\n\r\nSystem.out.println(sum);\r\n}\r\n}', 'java', 'Case 1: Accepted(1 points)\n', '1/8', '2016-01-26 21:20:16', '/home/adrita13/public_html/web16_adrita13/reports/2016-01-26-21-20-16_12_6_1677429204.txt'),
  (40, 1, 6, 'import java.util.Scanner;\r\n\r\npublic class Solver {\r\n\r\npublic static void main(String[] args) {\r\nScanner input = new Scanner (System.in);\r\nint a, b, sum=0;\r\n\r\na= input.nextInt();\r\nb= input.nextInt();\r\nsum=a+b;\r\n\r\nSystem.out.println(sum);\r\n}\r\n}', 'java', 'Case 1: Accepted(1 points)\n', '1/8', '2016-01-26 21:21:33', '/home/adrita13/public_html/web16_adrita13/reports/2016-01-26-21-21-33_12_6_976009445.txt'),
  (41, 1, 6, 'import java.util.Scanner;\r\n\r\npublic class Solver {\r\n\r\npublic static void main(String[] args) {\r\nScanner input = new Scanner (System.in);\r\nint a, b, sum=0;\r\n\r\na= input.nextInt();\r\nb= input.nextInt();\r\nsum=a+b;\r\n\r\nSystem.out.println(sum);\r\n}\r\n}', 'java', 'Case 1: Accepted(1 points)\n', '1/8', '2016-01-26 21:23:54', '/home/adrita13/public_html/web16_adrita13/reports/2016-01-26-21-23-54_12_6_401957883.txt'),
  (42, 1, 6, 'import java.util.Scanner;\r\n\r\npublic class Solver {\r\n\r\npublic static void main(String[] args) {\r\nScanner input = new Scanner (System.in);\r\nint a, b, sum=0;\r\n\r\na= input.nextInt();\r\nb= input.nextInt();\r\nsum=a+b;\r\n\r\nSystem.out.println(sum);\r\n}\r\n}', 'java', 'Case 1: Accepted(1 points)\nCase 2: Accepted(1 points)\nCase 3: Accepted(1 points)\nCase 4: Accepted(1 points)\nCase 0: Accepted(1 points)\nCase 1: Accepted(1 points)\nCase 2: Accepted(1 points)\nCase 3: Accepted(1 points)\n', '8/8', '2016-01-26 21:24:46', '/home/adrita13/public_html/web16_adrita13/reports/2016-01-26-21-24-46_12_6_692604692.txt'),
  (43, 1, 6, 'import java.util.Scanner;\r\n\r\npublic class Solver {\r\n\r\npublic static void main(String[] args) {\r\nScanner input = new Scanner (System.in);\r\nint a, b, sum=0;\r\n\r\na= input.nextInt();\r\nb= input.nextInt();\r\nsum=a+b;\r\n\r\nSystem.out.println(sum);\r\n}\r\n}', 'java', 'Case 1: Wrong Answer(0 points)\nCase 2: Wrong Answer(0 points)\nCase 3: Wrong Answer(0 points)\nCase 4: Wrong Answer(0 points)\nCase 0: Accepted(1 points)\nCase 1: Accepted(1 points)\nCase 2: Accepted(1 points)\nCase 3: Accepted(1 points)\n', '4/8', '2016-01-26 21:25:46', '/home/adrita13/public_html/web16_adrita13/reports/2016-01-26-21-25-46_12_6_972382489.txt'),
  (44, 1, 6, 'import java.util.Scanner;\r\n\r\npublic class Solver {\r\n\r\npublic static void main(String[] args) {\r\nScanner input = new Scanner (System.in);\r\nint a, b, sum=0;\r\n\r\na= input.nextInt();\r\nb= input.nextInt();\r\nsum=a+b;\r\n\r\nSystem.out.println(sum);\r\n}\r\n}', 'java', 'Case 1: Accepted(1 points)\nCase 2: Accepted(1 points)\nCase 3: Accepted(1 points)\nCase 4: Accepted(1 points)\nCase 0: Accepted(1 points)\nCase 1: Accepted(1 points)\nCase 2: Accepted(1 points)\nCase 3: Accepted(1 points)\n', '8/8', '2016-01-26 21:27:14', '/home/adrita13/public_html/web16_adrita13/reports/2016-01-26-21-27-14_12_6_1888636943.txt'),
  (45, 1, 6, 'import java.util.Scanner;\r\n\r\npublic class Solver {\r\n\r\npublic static void main(String[] args) {\r\nScanner input = new Scanner (System.in);\r\nint a, b, sum=0;\r\n\r\na= input.nextInt();\r\nb= input.nextInt();\r\nsum=a+b;\r\n\r\nSystem.out.println(sum);\r\n}\r\n}', 'java', 'Case 1: Accepted(1 points)\nCase 2: Accepted(1 points)\nCase 3: Accepted(1 points)\nCase 4: Accepted(1 points)\nCase 0: Accepted(1 points)\nCase 1: Accepted(1 points)\nCase 2: Accepted(1 points)\nCase 3: Accepted(1 points)\n', '8/8', '2016-01-26 21:28:59', '/home/adrita13/public_html/web16_adrita13/reports/2016-01-26-21-28-59_12_6_1268967460.txt'),
  (46, 1, 6, 'import java.util.Scanner;\r\n\r\npublic class Solver {\r\n\r\npublic static void main(String[] args) {\r\nScanner input = new Scanner (System.in);\r\nint a, b, sum=0;\r\n\r\na= input.nextInt();\r\nb= input.nextInt();\r\nsum=a+b;\r\n\r\nSystem.out.println(sum);\r\n}\r\n}', 'java', 'Case 1: Accepted(1 points)\nCase 2: Accepted(1 points)\nCase 3: Accepted(1 points)\nCase 4: Accepted(1 points)\nCase 5: Accepted(1 points)\nCase 6: Accepted(1 points)\nCase 7: Accepted(1 points)\nCase 8: Accepted(1 points)\n', '8/8', '2016-01-26 21:32:56', '/home/adrita13/public_html/web16_adrita13/reports/2016-01-26-21-32-56_12_6_773292631.txt'),
  (47, 1, 6, 'import java.util.Scanner;\r\n\r\npublic class Solver {\r\n\r\npublic static void main(String[] args) {\r\nScanner input = new Scanner (System.in);\r\nint a, b, sum=0;\r\n\r\na= input.nextInt();\r\nb= input.nextInt();\r\nsum=a+b;\r\n\r\nSystem.out.println(sum);\r\n}\r\n}', 'java', 'Case 1: Accepted(1 points)\nCase 2: Accepted(1 points)\nCase 3: Accepted(1 points)\nCase 4: Accepted(1 points)\nCase 5: Accepted(1 points)\nCase 6: Accepted(1 points)\nCase 7: Accepted(1 points)\nCase 8: Accepted(1 points)\n', '8/8', '2016-01-26 21:34:20', '/home/adrita13/public_html/web16_adrita13/reports/2016-01-26-21-34-20_12_6_120423884.txt'),
  (48, 1, 6, 'import java.util.Scanner;\r\n\r\npublic class Solver {\r\n\r\npublic static void main(String[] args) {\r\nScanner input = new Scanner (System.in);\r\nint a, b, sum=0;\r\n\r\na= input.nextInt();\r\nb= input.nextInt();\r\nsum=a+b;\r\n\r\nSystem.out.println(sum);\r\n}\r\n}', 'java', 'Case 1: Accepted(1 points)\nCase 2: Accepted(1 points)\nCase 3: Accepted(1 points)\nCase 4: Accepted(1 points)\nCase 5: Accepted(1 points)\nCase 6: Accepted(1 points)\nCase 7: Accepted(1 points)\nCase 8: Accepted(1 points)\n', '8/8', '2016-01-26 21:40:19', '/home/adrita13/public_html/web16_adrita13/reports/2016-01-26-21-40-20_12_6_1393225664.txt'),
  (49, 1, 6, 'import java.util.Scanner;\r\n\r\npublic class Solver {\r\n\r\npublic static void main(String[] args) {\r\nScanner input = new Scanner (System.in);\r\nint a, b, sum=0;\r\n\r\na= input.nextInt();\r\nb= input.nextInt();\r\nsum=a+b;\r\n\r\nSystem.out.println(sum);\r\n}\r\n}', 'java', 'Case 1: Accepted(1 points)\nCase 2: Accepted(1 points)\nCase 3: Accepted(1 points)\nCase 4: Accepted(1 points)\nCase 5: Accepted(1 points)\nCase 6: Accepted(1 points)\nCase 7: Accepted(1 points)\nCase 8: Accepted(1 points)\n', '8/8', '2016-01-26 22:38:28', '/home/adrita13/public_html/web16_adrita13/reports/2016-01-26-22-38-29_12_6_1861913804.txt'),
  (50, 1, 6, 'import java.util.Scanner;\r\n\r\npublic class Solver {\r\n\r\npublic static void main(String[] args) {\r\nScanner input = new Scanner (System.in);\r\nint a, b, sum=0;\r\n\r\na= input.nextInt();\r\nb= input.nextInt();\r\nsum=a+b;\r\n\r\nSystem.out.println(sum);\r\n}\r\n}', 'java', 'Error: ', '0/8', '2016-01-26 22:44:22', '/home/adrita13/public_html/web16_adrita13/reports/2016-01-26-22-44-22_12_6_2065083924.txt'),
  (51, 1, 6, 'import java.util.Scanner;\r\n\r\npublic class Solver {\r\n\r\npublic static void main(String[] args) {\r\nScanner input = new Scanner (System.in);\r\nint a, b, sum=0;\r\n\r\na= input.nextInt();\r\nb= input.nextInt();\r\nsum=a+b;\r\n\r\nSystem.out.println(sum);\r\n}\r\n}', 'java', 'Case 1: Accepted(1 points)\nCase 2: Accepted(1 points)\nCase 3: Accepted(1 points)\nCase 4: Accepted(1 points)\nCase 5: Accepted(1 points)\nCase 6: Accepted(1 points)\nCase 7: Accepted(1 points)\nCase 8: Accepted(1 points)\n', '8/8', '2016-01-26 22:46:49', '/home/adrita13/public_html/web16_adrita13/reports/2016-01-26-22-46-49_12_6_690948248.txt'),
  (52, 1, 6, 'a, b = raw_input().split()\r\nprint int(a)+int(b)\r\nexit(0)', 'python', 'Case 1: Accepted(1 points)\nCase 2: Accepted(1 points)\nCase 3: Accepted(1 points)\nCase 4: Accepted(1 points)\nCase 5: Accepted(1 points)\nCase 6: Accepted(1 points)\nCase 7: Accepted(1 points)\nCase 8: Accepted(1 points)\n', '8/8', '2016-01-26 22:48:38', '/home/adrita13/public_html/web16_adrita13/reports/2016-01-26-22-48-38_12_6_1705363555.txt'),
  (59, 1, 6, '&lt;script&gt;alert(&quot;hi&quot;)&lt;/script&gt;', 'c', 'Error: main.c:1:1: error: expected identifier or ''('' before ''<'' token <script>alert("hi")</script> ^', '0/8', '2016-01-27 14:06:23', '/home/adrita13/public_html/web16_adrita13/reports/2016-01-27-14-06-24_15_6_692028514.txt'),
  (60, 1, 6, 'a, b = raw_input().split()\r\nprint int(a)+int(b)\r\nexit(0)', 'python', 'Case 1: Accepted(1 points)\nCase 2: Accepted(1 points)\nCase 3: Accepted(1 points)\nCase 4: Accepted(1 points)\nCase 5: Accepted(1 points)\nCase 6: Accepted(1 points)\nCase 7: Accepted(1 points)\nCase 8: Accepted(1 points)\nCase 9: Accepted(10 points)\n', '18/18', '2016-01-27 14:58:00', '/home/adrita13/public_html/web16_adrita13/reports/2016-01-27-14-58-00_15_6_1895338211.txt'),
  (61, 1, 6, '\r\nimport java.util.Scanner;\r\n\r\npublic class Solver {\r\n\r\npublic static void main(String[] args) {\r\nScanner input = new Scanner (System.in);\r\nint a, b, sum=0;\r\n\r\na= input.nextInt();\r\nb= input.nextInt();\r\n  if(a==1)\r\n	sum = 5;\r\nelse sum=a+b;\r\n\r\nSystem.out.println(sum);\r\n}\r\n}', 'java', 'Case 1: Wrong Answer(0 points)\nCase 2: Accepted(1 points)\nCase 3: Accepted(1 points)\nCase 4: Accepted(1 points)\nCase 5: Accepted(1 points)\nCase 6: Accepted(1 points)\nCase 7: Accepted(1 points)\nCase 8: Accepted(1 points)\nCase 9: Accepted(10 points)\nCase 10: Accepted(5 points)\n', '22/23', '2016-01-28 09:31:17', '/home/adrita13/public_html/web16_adrita13/reports/2016-01-28-09-31-17_15_6_2082201219.txt');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `surname` varchar(45) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `birthdate` datetime DEFAULT NULL,
  `pictureUrl` varchar(200) DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  `attempts_cnt` int(11) DEFAULT NULL,
  `accept_cnt` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_group_id_idx` (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `name`, `surname`, `email`, `birthdate`, `pictureUrl`, `gender`, `group_id`, `type`, `password`, `timestamp`, `attempts_cnt`, `accept_cnt`) VALUES
  (1, 'noldidrita', 'Arnold', 'Drita', 'noldidrita@gmail.com', '2013-03-04 00:00:00', 'placeholder.svg', 'Male', 1, 4, '51035f0c6c585d8734f74c3c9ace4bb382b8fb71', '2016-01-20 23:15:23', 130, 39);
-- --------------------------------------------------------

--
-- Table structure for table `volumes`
--

CREATE TABLE IF NOT EXISTS `volumes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `description` text,
  `timestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `volumes`
--

INSERT INTO `volumes` (`id`, `name`, `description`, `timestamp`) VALUES
  (1, 'No Volume', 'Problems that do not appear in any volume', '2016-01-19 00:00:00');
--
-- Constraints for dumped tables
--

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `fk_comm_problem_id` FOREIGN KEY (`problem_id`) REFERENCES `problems` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_comm_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `problems`
--
ALTER TABLE `problems`
  ADD CONSTRAINT `fk_contest_id` FOREIGN KEY (`contest_id`) REFERENCES `contests` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `fk_homework_id` FOREIGN KEY (`homework_id`) REFERENCES `homeworks` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `fk_section_id` FOREIGN KEY (`section_id`) REFERENCES `sections` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `fk_volume_id` FOREIGN KEY (`volume_id`) REFERENCES `volumes` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Constraints for table `problem_cases`
--
ALTER TABLE `problem_cases`
  ADD CONSTRAINT `fk_problem_id` FOREIGN KEY (`problem_id`) REFERENCES `problems` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `submissions`
--
ALTER TABLE `submissions`
  ADD CONSTRAINT `fk_sub_problem_id` FOREIGN KEY (`problem_id`) REFERENCES `problems` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_sub_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `fk_group_id` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;